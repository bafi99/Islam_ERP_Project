<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Http\Models\Category;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class CategoryController extends Controller {

    private $category;

    public function __construct(){

        $this->category = new Category();

    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $categories = $this->category->paginate(10);

        return view('category.index' ,compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $inputs = array_map('trim' ,Request::all());

        $inputs = $this->category->ClearInputs($inputs);

        $errors = $this->category->ValidateInputs($inputs);

        $view = Redirect::to('category/create');

        if (!count($errors)){

            try{

                $this->category->create(array_only($inputs , ['CatName' ,'CatDesc' ,'disabled' ]));

                $view->with('success' ,[trans('category.success.new_added')] );

            }catch(\Exception $e){

                $view->with('errors' ,[trans('category.errors.database_error')] );

            }

        }else{

            $view->with('errors' ,$errors )->withInput();

        }

        return $view;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $CategoryID = $this->category->IsCategoryExist($id);

        if ($CategoryID){

            $category = $this->category->find($CategoryID);

            return view('category.categorydetails' ,compact('category') );

        }

        return Redirect::to('category')->with('errors', [[trans('category.errors.invaild_id')]] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $CategoryID = $this->category->IsCategoryExist($id);

        if ($CategoryID){

            $category = $this->category->find($CategoryID);

            return view('category.create' ,compact('category') );

        }

        return Redirect::to('/category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // Check Category ID Is Exist And Encrypted Because Its Update request
        $CategoryID = $this->category->IsCategoryExist($id ,true );

        if ($CategoryID){
            // Clear All Inputs From XSS
            $inputs = $this->category->ClearInputs(Request::all()) ;
            // Validate Inputs
            $errors = $this->category->ValidateInputs($inputs ,$CategoryID );

            $view = Redirect::to('category/'.$CategoryID.'/edit');

            if (!count($errors)){
                // Try To Update Selected Category In Database
                try{

                    $this->category->find($CategoryID)->update($inputs);

                    $view->with('success' ,[trans('category.success.category_updated')] );

                }catch(\Exception $e){

                    $view->with('errors' ,[trans('category.errors.database_error')] );

                }

            }else{ $view->with('errors' ,$errors ); }
        }else{
            // If Cannot Find Category Id Return Error Mesage
            $view = Redirect::to('category/')->with('errors' ,[trans('category.errors.invaild_id')] );;
        }

        return $view;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        $CategoryID = $this->category->IsCategoryExist($id ,true );

        $view = Redirect::to('category');

        try
        {

            $this->category->destroy($CategoryID);

            $view->with('success' ,[trans('category.success.category_deleted')] );

        } catch (\Exception $e) {

            $view->with('errors', [ $e->getMessage() ] );

        }
        return $view;
    }

}

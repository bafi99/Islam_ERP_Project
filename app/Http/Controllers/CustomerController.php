<?php namespace App\Http\Controllers;

use App\Http\Models\Customer;
use App\Http\Models\Item;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

class CustomerController extends Controller {

    private $customer;

    public function __construct(){

        $this->customer = new Customer();

    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $Customers = $this->customer->paginate(10);

        return view('customer.index' ,compact('Customers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('customer.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = array_map('trim' ,Request::all());

        $inputs = $this->customer->ClearInputs($inputs);

        $errors = $this->customer->ValidateInputs($inputs);

        $view = Redirect::to('customer/create');

        if (!count($errors)){

            try{

                $this->customer->create(array_only($inputs , ['CustName' ,'CustMobile1', 'CustMobile2','CustAddress', 'CustNote', 'disabled' ]));

                $view->with('success' ,[ trans('customer.success.new_added') ] );

            }catch(\Exception $e){

                $view->with('errors' ,[$e->getMessage()] );

            }

        }else{

            $view->with('errors' ,$errors )->withInput();

        }

        return $view;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $CustomerID = $this->customer->IsCustomerExist($id);

        if ($CustomerID){

            $customer = $this->customer->find($CustomerID);

            return view('customer.details' ,compact('customer') );

        }

        return Redirect::to('customer')->with('errors', [trans('customer.errors.invaild_id')] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $CustomerID = $this->customer->IsCustomerExist($id);

        if ($CustomerID){

            $customer = $this->customer->find($CustomerID);

            return view('customer.create' ,compact('customer'));

        }

        return Redirect::to('/customer');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // Check Category ID Is Exist And Encrypted Because Its Update request
        $CustomerID = $this->customer->IsCustomerExist($id ,true );

        if ($CustomerID){
            // Clear All Inputs From XSS
            $inputs = $this->customer->ClearInputs(Request::all()) ;
            // Validate Inputs
            $errors = $this->customer->ValidateInputs($inputs ,$CustomerID );

            $view = Redirect::to('customer/'.$CustomerID.'/edit');

            if (!count($errors)){
                // Try To Update Selected Category In Database
                try{

                    $this->customer->find($CustomerID)->update($inputs);

                    $view->with('success' ,[trans('customer.success.customer_updated')] );

                }catch(\Exception $e){

                    $view->with('errors' ,[trans('customer.errors.database_error')] );

                }

            }else{ $view->with('errors' ,$errors ); }
        }else{
            // If Cannot Find Category Id Return Error Mesage
            $view = Redirect::to('customer/')->with('errors' ,[trans('customer.errors.invaild_id')] );;
        }

        return $view;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $CustomerID = $this->customer->IsCustomerExist($id ,true );

        $view = Redirect::to('customer');

        try
        {

            $this->customer->destroy($CustomerID);

            $view->with('success' ,[ trans('customer.errors.customer_deleted') ] );

        } catch (\Exception $e) {

            $view->with('errors', [ $e->getMessage() ] );

        }
        return $view;
	}

}

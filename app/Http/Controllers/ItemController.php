<?php namespace App\Http\Controllers;

use App\Http\Models\Category;
use App\Http\Models\Item;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class ItemController extends Controller {

    private $item;

    public function __construct(){

        $this->item = new Item();

    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $items = $this->item->paginate(10);

        return view('item.index' ,compact('items'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        // User Snake Case To get All Enabled Categories
        $Categories = Category::whereDisabled('0')->get();

        return view('item.create' ,compact('Categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = array_map('trim' ,Request::all());

        $inputs = $this->item->ClearInputs($inputs);

        $errors = $this->item->ValidateInputs($inputs);

        $view = Redirect::to('item/create');

        if (!count($errors)){

            try{

                $this->item->create($inputs);

                $view->with('success' ,['تم اضافة صنف جديد'] );

            }catch(\Exception $e){

                $view->with('errors' ,[$e->getMessage()] );

            }

        }else{

            $view->with('errors' ,$errors )->withInput();

        }

        return $view;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $ItemID = $this->item->IsItemExist($id);

        if ($ItemID ){

            $item = $this->item->find($ItemID );

            return view('item.details' ,compact('item') );

        }

        return Redirect::to('item')->with('errors', [['من فضلك قم باختيار الصنف من القائمة']] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $ItemID = $this->item->IsItemExist($id);

        if ($ItemID){

            $item = $this->item->find($ItemID);

            $Categories = Category::whereDisabled('0')->get();

            return view('item.create' ,compact(['item' ,'Categories' ]) );

        }

        return Redirect::to('/item');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // Check Category ID Is Exist And Encrypted Because Its Update request
        $itemID = $this->item->IsItemExist($id ,true );

        if ($itemID){
            // Clear All Inputs From XSS
            $inputs = $this->item->ClearInputs(Request::all()) ;
            // Validate Inputs
            $errors = $this->item->ValidateInputs($inputs ,$itemID );

            $view = Redirect::to('item/'.$itemID.'/edit');

            if (!count($errors)){
                // Try To Update Selected Category In Database
                try{

                    $this->item->find($itemID)->update($inputs);

                    $view->with('success' ,['تم تعديل الصنف بنجاح'] );

                }catch(\Exception $e){

                    $view->with('errors' ,['خطا في قاعدة البيانات لا يمكنك من تعديل الصنف'] );

                }

            }else{ $view->with('errors' ,$errors ); }
        }else{
            // If Cannot Find Category Id Return Error Mesage
            $view = Redirect::to('item/')->with('errors' ,['من فضلك قم باختيار الصنف من القائمة لا يمكنك تعديل الصنف'] );;
        }

        return $view;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $ItemID = $this->item->IsItemExist($id ,true );

        $view = Redirect::to('item');

        try
        {

            $this->item->destroy($ItemID);

            $view->with('success' ,['Item Deleted'] );

        } catch (\Exception $e) {

            $view->with('errors', [ $e->getMessage() ] );

        }
        return $view;
	}

}

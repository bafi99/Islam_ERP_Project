<?php namespace App\Http\Controllers;

use App\Http\Models\Customer;
use App\Http\Models\Item;
use App\Http\Models\PaymentSystem;
use App\Http\Models\POS;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Redirect;
use \Illuminate\Support\Facades\Request;

class POSController extends Controller {

    private $item ,$customer ,$paymentsystem ,$pos ;

    /**
     * Construct
     */
    public function __construct(){
        $this->pos = new POS();
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $POSS = $this->pos->all();

        return view('pos.index' ,compact('POSS') );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $Items = Item::whereDisabled('0')->get();
        $Customers = Customer::whereDisabled('0')->get();
        $PaymentSystems = PaymentSystem::whereDisabled('0')->get();

        return view('pos.create' ,compact(['Items' ,'Customers' ,'PaymentSystems']) );

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = array_map('trim' ,Request::all());

        $inputs = $this->pos->ClearInputs($inputs);

        $errors = $this->pos->ValidateInputs($inputs);


        $view = Redirect::to('pos/create');

        if (!count($errors)){

            try{

                $this->pos->create($inputs);

                $view->with('success' ,[trans('pos.success.new_added')] );

            }catch(\Exception $e){

                $view->with('errors' ,[trans('pos.errors.database_error')] );

            }

        }else{

            $view->with('errors' ,$errors )->withInput($inputs);

        }

        return $view;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $POSID = $this->pos->IsPOSExist($id);

        if ($POSID ){

            $POS = $this->pos->find($POSID);

            return view('pos.details' ,compact('POS') );

        }

        return Redirect::to('pos')->with('errors', [trans('pos.errors.invalid_id')] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $POSID = $this->pos->IsPOSExist($id);

        if ($POSID){

            $POS = $this->pos->find($POSID);

            $Items = Item::whereDisabled('0')->get();
            $Customers = Customer::whereDisabled('0')->get();
            $PaymentSystems = PaymentSystem::whereDisabled('0')->get();

            return view('pos.create' ,compact(['POS','Items' ,'Customers' ,'PaymentSystems']) );

        }

        return Redirect::to('/pos');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // Check POS ID Is Exist And Encrypted Because Its Update request
        $POSID = $this->pos->IsPOSExist($id ,true );

        if ($POSID){
            // Clear All Inputs From XSS
            $inputs = $this->pos->ClearInputs(Request::all()) ;
            // Validate Inputs
            $errors = $this->pos->ValidateInputs($inputs ,$POSID );

            $view = Redirect::to('pos/'.$POSID.'/edit');

            if (!count($errors)){
                // Try To Update Selected Category In Database
                try{

                    $this->pos->find($POSID)->update($inputs);

                    $view->with('success' ,[trans('pos.success.pos_updated')] );

                }catch(\Exception $e){

                    $view->with('errors' ,[trans('pos.errors.database_error')] );

                }

            }else{ $view->with('errors' ,$errors ); }
        }else{
            // If Cannot Find Category Id Return Error Mesage
            $view = Redirect::to('pos/')->with('errors' ,[trans('pos.errors.invalid_id')] );
        }

        return $view;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $POSID = $this->pos->IsPOSExist($id ,true );

        $view = Redirect::to('pos');

        try
        {

            $this->pos->destroy($POSID);

            $view->with('success' ,[trans('pos.success.pos_deleted')] );

        } catch (\Exception $e) {

            $view->with('errors', [ $e->getMessage() ] );

        }
        return $view;
	}

    /**
     * Confirm POS To Avaiable To add Payment dates , and to review all data
     * @param $id
     * @return mixed
     */
    public function Confirm($id){

        $POSID = $this->pos->IsPOSExist($id ,true );

        if ($POSID){

            $view = Redirect::to('pos/'.$POSID);

            try{

                $this->pos->find($POSID)->update(['Confirm' => '1']);

                $view->with('success' ,[trans('pos.success.confirmed')] );

            }catch(\Exception $e){

                $view->with('errors' ,[trans('pos.errors.database_error')] );

            }

        }else{
            // If Cannot Find POS Id Return Error Mesage
            $view = Redirect::to('pos/')->with('errors' ,[trans('pos.errors.confirm')] );;
        }

        return $view;
    }

    public function ToggleFinish(\Illuminate\Http\Request $request ,$id ){

        $POSID = $this->pos->IsPOSExist($id ,true );

        $view = Redirect::to('pos/'.$POSID) ;

        if ($POSID){

            $POS = $this->pos->find($POSID);

            try{

                if ($POS->Finished){

                    $POS->update(['Finished' => '0']);
                    $view->with('success' ,['POS Closed'] );

                }else{
                    if ( $this->pos->CheckAmountPaidIsEqualDebit($POSID) ){

                        $POS->update(['Finished' => '1']);
                        $view->with('success' ,['POS Open'] );

                    }else{
                        $view->with('errors' ,[trans('pos.errors.cannot_close_pos')] );
                    }
                }

            }catch(\Exception $e){
                $view->with('errors' ,['Cannot Close/Open POS'] );
            }

        }else{
            $view->with('error' ,[trans('pos.errors.invaild_id')] );
        }

        return $view;

    }


}

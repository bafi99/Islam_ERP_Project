<?php namespace App\Http\Controllers;

use App\Http\Models\PaymentDates;
use App\Http\Models\PaymentSystem;
use App\Http\Requests;

use App\Services\CustomValidators;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Request;

class PaymentDatesController extends Controller {

    private $paymentdates ,$paymentsystem ;

    public function __construct(){

        $this->paymentdates = new PaymentDates();
        $this->paymentsystem = new PaymentSystem();

    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {

        $PaymentSystems = $PaymentSystem = null ;
        // Check If Payment System Is Selected
        if ($PaymentSystemID = $request->get('payment_system')){

            $PaymentSystemID = $this->paymentsystem->IsPaymentSystemExist($PaymentSystemID ,true ) ;

            // Check Payment System id Is Exist
            if ($PaymentSystemID){

                $PaymentSystem = PaymentSystem::find($PaymentSystemID);

                $PaymentDates = $this->paymentdates->where(['PaymentSystem_ID' => $PaymentSystemID])->paginate();

            }

        }

        isset($PaymentDates) ?: $PaymentDates = $this->paymentdates->paginate(20);

        // Use Snake Case To get All Enabled Payment Systems
        $PaymentSystems = PaymentSystem::whereDisabled('0')->get();

        return view('paymentdates.index' ,compact(['PaymentSystems' ,'PaymentSystem' ,'PaymentDates' ]));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(\Illuminate\Http\Request $request)
    {
        $PaymentSystems = $PaymentSystem = null ;
        // Check If Payment System Is Selected
        if ($PaymentSystemID = $request->get('payment_system')){

            $PaymentSystemID = $this->paymentsystem->IsPaymentSystemExist($PaymentSystemID ,true ) ;

            // Check Payment System id Is Exist
            if ($PaymentSystemID){

                $PaymentSystem = PaymentSystem::find($PaymentSystemID);

                $PaymentSystemDates = $this->paymentdates->where(['PaymentSystem_ID' => $PaymentSystemID])->get()->sortBy('PDate_Date');

            }

        }

        // Use Snake Case To get All Enabled Payment Systems
        $PaymentSystems = PaymentSystem::whereDisabled('0')->get();

        return view('paymentdates.create' ,compact(['PaymentSystems' ,'PaymentSystem' ,'PaymentSystemDates' ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $inputs = array_map('trim' ,Request::all());

        $inputs = $this->paymentdates->ClearInputs($inputs);

        $errors = $this->paymentdates->ValidateInputs($inputs);

        $URLtoRedirect = (Input::get('payment_system') ? 'payment_system=' . Input::get('payment_system') : '' ) ;

        $view = Redirect::to('paymentdates/create?' . $URLtoRedirect);

        if (!count($errors)){

            try{

                $this->paymentdates->create($inputs);

                $view->with('success' ,[trans('psystemdates.success.new_added')] );

            }catch(\Exception $e){

                $view->with('errors' ,[trans('psystemdates.errors.database_error')] );

            }

        }else{

            $view->with('errors' ,$errors )->withInput();

        }

        return $view;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $PaymentDateID = $this->paymentdates->IsPaymentDateExist($id);

        if ($PaymentDateID ){

            $PaymentDate = $this->paymentdates->find($PaymentDateID );

            return view('paymentdates.details' ,compact('PaymentDate') );

        }

        return Redirect::to('paymentdates')->with('errors', [[trans('psystemdates.errors.invaild_id')]] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $PaymenrDateID = $this->paymentdates->IsPaymentDateExist($id);

        if ($PaymenrDateID){

            $PaymentDate = $this->paymentdates->find($PaymenrDateID);

            // Use Snake Case To get All Enabled Payment Systems
            $PaymentSystems = PaymentSystem::whereDisabled('0')->get();

            return view('paymentdates.create' ,compact(['PaymentDate' ,'PaymentSystems']) );

        }

        return Redirect::to('/paymentdates');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // Check Payment Date ID Is Exist And Encrypted Because Its Update request
        $PaymentDateID = $this->paymentdates->IsPaymentDateExist($id ,true );

        if ($PaymentDateID){
            // Clear All Inputs From XSS
            $inputs = $this->paymentdates->ClearInputs(Request::all()) ;
            // Validate Inputs
            $errors = $this->paymentdates->ValidateInputs($inputs ,$PaymentDateID );

            $view = Redirect::to('paymentdates/'.$PaymentDateID.'/edit');

            if (!count($errors)){
                // Try To Update Selected Category In Database
                try{

                    $this->paymentdates->find($PaymentDateID)->update($inputs);

                    $view->with('success' ,[trans('psystemdates.success.psystemdate_updated')] );

                }catch(\Exception $e){

                    $view->with('errors' ,[trans('psystemdates.errors.database_error')] );

                }

            }else{ $view->with('errors' ,$errors ); }
        }else{
            // If Cannot Find Pamynt Date Id Return Error Mesage
            $view = Redirect::to('paymentdates/')->with('errors' ,[trans('psystemdates.errors.invaild_id')] );
        }

        return $view;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $PaymentDateID = $this->paymentdates->IsPaymentDateExist($id ,true );

        $view = Redirect::to('paymentdates');

        try
        {
            $this->paymentdates->destroy($PaymentDateID);

            $view->with('success' ,[trans('psystemdates.success.psystemdate_deleted')] );

        } catch (\Exception $e) {

            $view->with('errors', [ $e->getMessage() ] );

        }
        return $view;
    }
}

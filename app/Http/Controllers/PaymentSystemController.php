<?php namespace App\Http\Controllers;

use App\Http\Models\PaymentSystem;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class PaymentSystemController extends Controller {

    private $paymentsystem ;

    public function __construct(){

        $this->paymentsystem = new PaymentSystem();

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $PaymentSystems = $this->paymentsystem->paginate(10);

        return view('paymentsystem.index' ,compact('PaymentSystems'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        return view('paymentsystem.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        $inputs = array_map('trim' ,Request::all());

        $inputs = $this->paymentsystem->ClearInputs($inputs);

        $errors = $this->paymentsystem->ValidateInputs($inputs);

        $view = Redirect::to('paymentsystem/create');

        if (!count($errors)){

            try{

                $this->paymentsystem->create(array_only($inputs , ['PaymentID' ,'PaymentAlias' ,'PaymentDescription' ,'disabled' ]));

                $view->with('success' ,[trans('psystem.success.new_added')] );

            }catch(\Exception $e){

                $view->with('errors' ,[trans('psystem.errors.database_error')] );

            }

        }else{

            $view->with('errors' ,$errors )->withInput();

        }

        return $view;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {

        $PaymentSystemID = $this->paymentsystem->IsPaymentSystemExist($id);

        if ($PaymentSystemID){

            $paymentsystem = $this->paymentsystem->find($PaymentSystemID);

            return view('paymentsystem.details' ,compact('paymentsystem') );

        }

        return Redirect::to('paymentsystem')->with('errors', [trans('psystem.errors.invaild_id')] );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {

        $PaymentSystemID = $this->paymentsystem->IsPaymentSystemExist($id);

        if ($PaymentSystemID){

            $paymentsystem = $this->paymentsystem->find($PaymentSystemID);

            return view('paymentsystem.create' ,compact('paymentsystem'));

        }

        return Redirect::to('/paymentsystem');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {

        // Check Payment System ID Is Exist And Encrypted Because Its Update request
        $PaymentSystemID = $this->paymentsystem->IsPaymentSystemExist($id ,true );

        if ($PaymentSystemID){
            // Clear All Inputs From XSS
            $inputs = $this->paymentsystem->ClearInputs(Request::all()) ;
            // Validate Inputs
            $errors = $this->paymentsystem->ValidateInputs($inputs ,$PaymentSystemID );

            $view = Redirect::to('paymentsystem/'.$PaymentSystemID.'/edit');

            if (!count($errors)){
                // Try To Update Selected Category In Database
                try{

                    $this->paymentsystem->find($PaymentSystemID)->update($inputs);

                    $view->with('success' ,[trans('psystem.success.psystem_updated')] );

                }catch(\Exception $e){

                    $view->with('errors' ,[trans('psystem.errors.database_error')] );

                }

            }else{ $view->with('errors' ,$errors ); }
        }else{
            // If Cannot Find Payment System Id Return Error Mesage
            $view = Redirect::to('paymentsystem/')->with('errors' ,[trans('psystem.errors.invaild_id')] );;
        }

        return $view;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {

        $PaymentSystemID = $this->paymentsystem->IsPaymentSystemExist($id ,true );

        $view = Redirect::to('paymentsystem');

        try
        {

            $this->paymentsystem->destroy($PaymentSystemID);

            $view->with('success' ,[trans('psystem.success.psystem_deleted')] );

        } catch (\Exception $e) {

            $view->with('errors', [ trans('psystem.errors.database_error') ] );

        }
        return $view;

    }

}

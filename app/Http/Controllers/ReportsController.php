<?php namespace App\Http\Controllers;

use App\Http\Models\Customer;
use App\Http\Models\PaymentSystem;
use App\Http\Models\POS;
use App\Http\Models\Transaction;
use App\Http\Repository\Reports;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportsController extends Controller
{

    private $pos, $transactions, $carbon, $RepoReports;

    public function __construct(Request $request)
    {
        $this->pos = new POS();
        $this->transactions = new Transaction();
        $this->carbon = new Carbon();
        $this->RepoReports = new Reports($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $PaymentSystems = PaymentSystem::where(['disabled' => '0'])->get();

        $Customers = Customer::all();

        $CustomerBills = Customer::find($request->get('customer_id'));

        return view('report.index', compact(['PaymentSystems', 'Customers', 'CustomerBills']));
    }

    /**
     * Get Next Payment Dates
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function NextPaymentDates(Request $request)
    {
        $Transactions = $this->RepoReports->getNextPaymentDates();

        $TotalTransactions = ['AmountPaid' => $Transactions->sum('AmountPaid'), 'ExpectedAmount' => $Transactions->sum('ExpectedAmount')];

        if ($request->get('print')) {
            $Transactions = $Transactions->get();
            $view = view('report.pdf.nextpaymentdate', compact(['Transactions', 'TotalTransactions']));
            return $this->RepoReports->PDFReport($view->render(), 'NextPaymentDates');
        }

        $Transactions = $Transactions->paginate(10);
        return view('report.nextpaymentdate', compact(['Transactions', 'TotalTransactions']));
    }

    /**
     * @param int $POSID
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function CustomerBill(Request $request, $POSID = 0)
    {
        $POS = POS::wherePosid($POSID)->get();

        if ($POS->count()) {

            $Transactions = Transaction::wherePosId($POSID)->get()->sortBy('PaymentDate');

            $POS = $POS->first();

            if ($request->get('print')) {
                $view = view('report.pdf.customerbill', compact(['Transactions', 'POS']));
                return $this->RepoReports->PDFReport($view->render(), $POS->Customer()->first()->CustName, 'P', 10);
            }

            return view('report.customerbill', compact(['Transactions', 'POS']));

        }

        return redirect('/reports');
    }

    /**
     * Finicial Report Get all Admin finicial reports
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function FinicialPostion(Request $request)
    {
        $POS = new POS();
        $POS = $POS->whereDate('StartDate', '>=', $request->get('start_date'))
            ->whereDate('EndDate', '<=', $request->get('end_date'));

        if ($request->get('print')) {
            $view = view('report.pdf.finicialpostion', compact(['POS']));
            return $this->RepoReports->PDFReport($view->render(), 'Finicial Report', 'P', 12);
        }

        return view('report.finicialpostion', compact(['POS']));
    }

    /**
     * Get Remaining Transactions
     * @param Request $request
     * @return \Illuminate\View\View|void
     */
    public function RemainingTransactions(Request $request)
    {
        $POSS = $this->RepoReports->getRemainingTransaction();

        if ($request->get('print')) {
            $view = view('report.pdf.remaining_transactions', compact(['POSS']));
            return $this->RepoReports->PDFReport($view->render(), 'Remainging_Transactions', 'L', 9);
        }
        return view('report.remaining_transactions', compact(['POSS']));
    }

    private function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}

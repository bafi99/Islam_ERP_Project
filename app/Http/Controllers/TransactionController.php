<?php namespace App\Http\Controllers;

use App\Http\Models\PaymentDates;
use App\Http\Models\POS;
use App\Http\Models\Transaction;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class TransactionController extends Controller {

    private $pos ,$paymentdates ,$transaction ;

    public function __construct(){
        $this->pos = new POS();
        $this->paymentdates = new PaymentDates();
        $this->transaction = new Transaction();
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		Redirect::to('/pos');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(\Illuminate\Http\Request $request)
	{
        $POS = null ;
        $errors = null ;
        // Check If POS Is Selected
        if ($POSID = $request->get('pos')){

            $POSID = $this->pos->IsPOSExist($POSID ,true ) ;

            // Check POS id Is Exist
            if ($POSID){

                $POS = $this->pos->where(['POSID' => $POSID ,'Confirm' => '1','disabled' => '0'])->get()->first();

                if ($POS){

                    $PaymentSystemDates = $POS->PaymentSystem()->first()->PaymentDates()->get();

                    $check_transactions = $this->transaction->where(['POS_ID' => $POS->POSID])->get()->count();

                    $GenerateTransactionsDates = [];

                    $GenerateDates = $this->transaction->GenerateTransactionsDates($POS);

                    if (!$check_transactions){
                        $GenerateTransactionsDates = $GenerateDates ;
                    }
                    
                    $Transactions = $this->transaction->where(['POS_ID' => $POSID])->get()->sortBy('PaymentDate');

                    $Compact = ['POS' ,'PaymentSystemDates' ,'GenerateTransactionsDates' ,'Transactions' ,'GenerateDates' ];
                    return view('transaction.create' ,compact( $Compact ) );
                }else{

                    $errors = ['لا يمكن تحميل عملية البيع من الممكن ان تكون مغلقة او غير مفعلة'];

                }
            }

        }else{
            $errors = ['من فضلك قم باختيار عملية البيع من القائمة'];
        }
        return Redirect::to('/pos')->with('errors' ,$errors );
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(\Illuminate\Http\Request $request)
	{

        $POSID = $this->pos->IsPOSExist($request->get('_pos') ,true ); // Get POS ID
        if ($request->get('one_row') == 'one_row_' . csrf_token()){
            $is_transactions_inserted = 0 ;
        }else{
            $is_transactions_inserted = $this->transaction->where(['POS_ID' => $POSID])->get()->count(); // Check If No Transcation inserted
        }
        $view = Redirect::to('transactions/create?pos=' . Crypt::encrypt($POSID)) ; // redirect to transactions

        // check if pos is exist
        if ($POSID && $is_transactions_inserted == 0 ){

            $TotalExpectedAmount = 0 ; $all_transactions = null ; $debit = POS::find($POSID)->Debit ;
            // validate all transactions
            $redirect_with_error = $this->transaction->ValidateAllTransactions($request ,$POSID ,$TotalExpectedAmount ,$all_transactions );

            // check if found error after validate all transaction
            if ($redirect_with_error){
                // stop working and return all with error
                return $redirect_with_error;
            }else if ($request->get('one_row') == 'one_row_' . csrf_token()  && $TotalExpectedAmount > $debit){

                $view->with('errors' ,[ trans('transactions.total_expected') . " ($TotalExpectedAmount) " . trans('transactions.msg_parts.part_1') . " ($debit)"] )->withInput() ;

            // check if total expected amount = debit
            }else if ($request->get('one_row') != 'one_row_' . csrf_token() && $TotalExpectedAmount != $debit){

                $view->with('errors' ,[trans('transactions.total_expected') ." ($TotalExpectedAmount) ".trans('transactions.msg_parts.part_2')." ($debit)"] )->withInput() ;

            }else{
                // Insert block of transactions
                $this->transaction->InsertBlockOfTransactions($view ,$all_transactions );
            }

        }else{
            $view->with('errors' ,['من فضلك قم باختيار عملية البيع من القائمة , او بالفعل تم ادخال معاملات لهذه العملية']);
        }

        return $view;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $TransactionID = $this->transaction->IsTransactionExist($id);

        if ($TransactionID ){

            $Transaction = $this->transaction->find($TransactionID );

            $POS = $Transaction->POS()->first();

            return view('transaction.details' ,compact(['Transaction' ,'POS' ]) );

        }
        return Redirect::to('transaction')->with('errors', [trans('transactions.errors.database_error')] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $TranscationID = $this->transaction->IsTransactionExist($id);

        if ($TranscationID){

            $Transaction = $this->transaction->find($TranscationID);

            $GeneratedPaymentDate = $this->transaction->GenerateTransactionsDates($Transaction->POS()->first());
            $AvaiableTransactionPaymentDates = $this->transaction->where(['POS_ID' => $Transaction->POS_ID ,'Done' => '0'])->get();

            $POS = $Transaction->POS()->first();

            $Transactions = $this->transaction->where(['POS_ID' => $Transaction->POS_ID ,'Done' => '0'])->get();

            return view('transaction.edit' ,compact(['Transaction' ,'Transactions' ,'POS' ,'GeneratedPaymentDate' ,'AvaiableTransactionPaymentDates' ]) );

        }

        return Redirect::to('/transactions');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(\Illuminate\Http\Request $request , $id)
	{

        // Check Category ID Is Exist And Encrypted Because Its Update request
        $TransactionID = $this->transaction->IsTransactionExist($id ,true );
        if ($TransactionID){

            // Clear All Inputs From XSS after triming all spaces
            $inputs = $this->transaction->ClearInputs(array_map('trim' ,$request->all() ) ,$TransactionID ) ;

            $errors = $this->transaction->ValidateInputs($inputs ,$TransactionID );

            $view = Redirect::to('transactions/'.$TransactionID.'/edit');

            if (!count($errors)){
                // Try To Update Selected Category In Database
                try{

                    $this->transaction->find($TransactionID)->update($inputs);

                    $view->with('success' ,['Transaction Updated'] );

                }catch(\Exception $e){

                    $view->with('errors' ,[$e->getMessage()] );

                }

            }else{ $view->with('errors' ,$errors ); }
        }else{
            // If Cannot Find Category Id Return Error Mesage
            $view = Redirect::to('transactions/')->with('errors' ,[trans('transactions.errors.invalid_id')] );
        }

        return $view;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $TransactionID = $this->transaction->IsTransactionExist($id ,true );
        $view = Redirect::to('transactions');
        // Check Transaction is exist
        if ($TransactionID){
            try
            {
                // check if Point OF sale Is Close Or Still Open Till now
                if ( '0' == Transaction::find($TransactionID)->POS()->first()->Finished ){

                    $url = url('transactions/create?pos=' . Crypt::encrypt(Transaction::find($TransactionID)->POS()->first()->POSID));
                    $this->transaction->destroy($TransactionID);
                    $view = Redirect::to($url)->with('success' ,[trans('transactions.success.trans_deleted')] );

                }else{
                    // Redirect with error if POS Is closed
                    $view = Redirect::to('transactions/' . $TransactionID)
                        ->with('errors', [ trans('transactions.if_pos_finished') ] );
                }

            } catch (\Exception $e) {

                $view->with('errors', [ $e->getMessage() ] );

            }
        }
        return $view;
	}

    public function Postpone(\Illuminate\Http\Request $request  ,$id){

        $TransactionID = $this->transaction->IsTransactionExist($id ,true );

        if ($TransactionID){

            $Transaction = $this->transaction->find($TransactionID);
            $inputs = $this->transaction->ClearPostpone($request->get('postpone') );
            $errors = $this->transaction->ValidatePostpone($inputs ,$Transaction->POS_ID );
            $view = Redirect::to('transactions/'.$TransactionID.'/edit');
            if (!count($errors)){
                try{

                    $inputs['ExpectedAmount'] = $Transaction->AmountPaid;
                    $postpone_value = $Transaction->ExpectedAmount - $Transaction->AmountPaid ;
                    if ($postpone_value > 0){

                        $this->transaction->find($TransactionID)->update($inputs);
                        $postpone_transaction = $this->transaction->GetPostponeTransactionByDate($Transaction->POS_ID ,$request->get('postpone') );

                        $postpone_transaction->increment('ExpectedAmount' ,$postpone_value );
                        $postpone_transaction->update(['Notes' => $postpone_transaction->Notes . ' , ' . trans('transactions.postpone_notification' ,['price' => $postpone_value , 'date' => $Transaction->PaymentDate ])] );

                        $view->with('success' ,['تم تاجيل باقي المبلغ الغير مسدد'] );

                    }else{
                        $view->with('errors' ,['المبلغ المدفوع يساوي المبلغ المتوقع دفعه , لا حاجه الي تاجيل'] );
                    }
                }catch(\Exception $e){
                    $view->with('errors' ,[trans('transactions.errors.database_error')] );
                }
            }else{ $view->with('errors' ,$errors ); }

        }else{
            // If Cannot Find Category Id Return Error Mesage
            $view = Redirect::to('transactions/')->with('errors' ,[trans('transactions.invaild_id')] );
        }
        return $view;
    }
}
/**
 * I Stopped On destory
 */
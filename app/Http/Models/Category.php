<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Validator;

class Category extends Model {


    protected $primaryKey = "CategoryID";
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tblCategories';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['CategoryID' ,'CatName' ,'CatDesc','disabled' ];


    /*
     * @param inputs
     * */
    public function ClearInputs($inputs){

        $inputs = array_only($inputs ,[
          'CatName' ,'CatDesc' ,'enable'
        ]);

        $inputs['CatName'] = isset($inputs['CatName']) ? trim(strip_tags($inputs['CatName'])) : '' ;

        $inputs['CatDesc'] = isset($inputs['CatDesc']) ? trim(strip_tags($inputs['CatDesc'])) : '' ;

        $inputs['disabled'] = isset($inputs['enable']) && $inputs['enable']== 'on' ? 0 : 1 ;

        return $inputs;
    }

    /**
     * @param $CategoryID
     * @param bool $Encrypted
     * @return bool
     */
    public function IsCategoryExist($CategoryID ,$Encrypted = false )
    {
        $CategoryID = $Encrypted ? Crypt::decrypt($CategoryID) : $CategoryID ;

        try {
              if (count($this->find($CategoryID))){

                  return $CategoryID;

              }else{
                 return false;
              }

        } catch (\Exception $e) {

            return false;

        }
    }

    /**
     * @param null $inputs
     * @return mixed
     */
    public function ValidateInputs($inputs = null, $CategoryID = null ){

        $rules = [
            'CatName' => 'required|min:2|max:160|unique:tblCategories,CatName,'.$CategoryID.',CategoryID' ,
            'CatDesc' => 'max:255' ,
            'disabled' => 'boolean'
        ];

        $errors = Validator::make($inputs ,$rules ,$this->_ValidationMessage() )->messages()->toArray();

        return $errors;
    }

    /**
     * Validation Message
     * @return array
     */
    private function _ValidationMessage(){
        return array(
            'CatName.required' => trans('category.validation.catname.required'),
            'CatName.max' => trans('category.validation.catname.max'),
            'CatName.min' => trans('category.validation.catname.min'),
            'CatName.unique' => trans('category.validation.catname.unique'),
            'CatDesc.max' => trans('category.validation.catdesc.max'),
            'disabled.boolean' => trans('category.validation.disabled.boolean'),
        );
    }

}

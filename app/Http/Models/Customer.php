<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Validator;

class Customer extends Model {


    protected $primaryKey = "CustomerID";
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tblCustomers';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['CustomerID' ,'CustName' ,'CustMobile1', 'CustMobile2','CustAddress', 'CustNote', 'disabled' ];


    /*
     * @param inputs
     * */
    public function ClearInputs($inputs){

        $inputs = array_only($inputs ,[
          'cust_name' ,'cust_mobile_1' ,'cust_mobile_2' ,'cust_address' ,'cust_note' ,'enable'
        ]);

        $Customer['CustName'] = isset($inputs['cust_name']) ? trim(strip_tags($inputs['cust_name'])) : '' ;

        $Customer['CustMobile1'] = isset($inputs['cust_name']) ? trim(strip_tags($inputs['cust_mobile_1'])) : '' ;

        $Customer['CustMobile2'] = isset($inputs['cust_name']) ? trim(strip_tags($inputs['cust_mobile_2'])) : '' ;

        $Customer['CustAddress'] = isset($inputs['cust_name']) ? trim(strip_tags($inputs['cust_address'])) : '' ;

        $Customer['CustNote'] = isset($inputs['cust_name']) ?  trim(strip_tags($inputs['cust_note'])) : '' ;

        $Customer['disabled'] = isset($inputs['enable']) && $inputs['enable']== 'on' ? 0 : 1 ;

        return $Customer;
    }

    /**
     * @param $CustomerID
     * @param bool $Encrypted
     * @return bool
     */
    public function IsCustomerExist($CustomerID ,$Encrypted = false )
    {
        try {

            $CustomerID = $Encrypted ? Crypt::decrypt($CustomerID) : $CustomerID ;

            if (count($this->find($CustomerID))){

                return $CustomerID;

            }else{
                return false;
            }

        } catch (\Exception $e) {

            return false;

        }
    }

    /**
     * @param null $inputs
     * @return mixed
     */
    public function ValidateInputs($inputs = null, $CustomerID = null ){
        $rules = [
            'CustName' => 'min:2|max:160|required|unique:tblCustomers,CustName,'.$CustomerID.',CustomerID' ,
            'CustMobile1' => 'digits_between:9,11' ,
            'CustMobile2' => 'digits_between:9,11' ,
            'CustAddress' => 'max:255' ,
            'CustNote' => 'max:1000' ,
            'disabled' => 'boolean'
        ];

        $errors = Validator::make($inputs ,$rules ,$this->_ValidationMessage() )->messages()->toArray();

        return $errors;
    }

    /**
     * Validation Message
     * @return array
     */
    private function _ValidationMessage(){
        return array(
            'CustName.required' => trans('customer.validation.customername.required') ,
            'CustName.max' => trans('customer.validation.customername.max') ,
            'CustName.min' => trans('customer.validation.customername.min') ,
            'CustName.unique' => trans('customer.validation.customername.unique') ,

            'CustMobile1.digits_between' => trans('customer.validation.customermobile.digits_between') ,

            'CustMobile2.digits_between' => trans('customer.validation.customermobile.digits_between') ,

            'CustNote.max' => trans('customer.validation.customernote.max') ,

            'CustAddress.max' => trans('customer.validation.customeraddress.max') ,

            'disabled.boolean' => trans('customer.validation.disabled.max')
        );
    }

    /**
     * Customer Has many bills (pos)
     * @return mixed
     */
    public function POS(){
        return $this->hasMany('App\Http\Models\POS' ,'Customer_ID' ,'CustomerID')->select();
    }

}

<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Validator;

class Item extends Model {


    protected $primaryKey = "ItemID";
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tblItems';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ItemID' ,'ItemName' ,'ItemPrice' ,'ItemDesc','cat_id','disabled' ];


    /*
     * @param inputs
     * */
    public function ClearInputs($inputs){


        $inputs = array_only($inputs ,[
            'CategoryID' ,'Item_name' ,'Item_desc','enable'
        ]);

        $item['cat_id'] = isset($inputs['CategoryID']) ? trim(strip_tags($inputs['CategoryID'])) : '0';

        $item['ItemName'] = isset($inputs['Item_name']) ? trim(strip_tags($inputs['Item_name'])) : '';

        $item['ItemDesc'] = isset($inputs['Item_desc']) ? trim(strip_tags($inputs['Item_desc'])) : '';

        $item['disabled'] = isset($inputs['enable']) && $inputs['enable']== 'on' ? 0 : 1 ;

        return $item;
    }

    /**
     * @param $CategoryID
     * @param bool $Encrypted
     * @return bool
     */
    public function IsItemExist($ItemID ,$Encrypted = false )
    {
        try {

            $ItemID = $Encrypted ? Crypt::decrypt($ItemID) : $ItemID ;

            if (count($this->find($ItemID))){

                return $ItemID;

            }else{
                return false;
            }

        } catch (\Exception $e) {

            return false;

        }
    }

    /**
     * @param null $inputs
     * @return mixed
     */
    public function ValidateInputs($inputs = null, $ItemID = null ){

        $rules = [
            'ItemName' => 'min:2|max:160|required|unique:tblItems,ItemName,'.$ItemID.',ItemID' ,
            'ItemDesc' => 'max:1000' ,
            'cat_id' => 'integer|required|exists:tblCategories,CategoryID',
            'disabled' => 'boolean'
        ];

        $errors = Validator::make($inputs ,$rules ,$this->_ValidationMessage() )->messages()->toArray();

        return $errors;
    }

    /**
     * Item belong to Category
     *
     * @relationship
     * */
    public function Category()
    {

        return $this->belongsTo('App\Http\Models\Category' ,'cat_id' ,'CategoryID')->select();

    }

    /**
     * Validation Message
     * @return array
     */
    private function _ValidationMessage(){
        return array(
            'ItemName.required' => trans('item.validation.itemname.required') ,
            'ItemName.max' => trans('item.validation.itemname.max') ,
            'ItemName.min' => trans('item.validation.itemname.min') ,
            'ItemName.unique' => trans('item.validation.itemname.unique') ,

            'ItemDesc.max' => trans('item.validation.itemdesc.unique') ,

            'cat_id.integer' => trans('item.validation.cat_id.exists') ,
            'cat_id.required' => trans('item.validation.cat_id.exists') ,
            'cat_id.exists' => trans('item.validation.cat_id.exists') ,

            'disabled.boolean' => trans('item.validation.disabled.boolean') ,
        );
    }

}

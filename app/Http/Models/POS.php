<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Validator;

class POS extends Model {


    protected $primaryKey = "POSID";
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tblPOS';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'POSID' ,'Customer_ID' ,'PaymentSystem_ID' ,'Item_ID'
        ,'Deposit' ,'Interest' ,'PurchasingPrice' ,'SellingPrice'
        ,'StartDate' ,'EndDate' ,'Notes' ,'Discount' ,'Debit' ,'Finished' ,'Confirm' ,'disabled' ,
    ];


    /*
     * @param inputs
     * */
    public function ClearInputs($inputs){

        $inputs = array_only($inputs ,[
            'customer_name' ,'item_name' ,'payment_system','purchasing_price','selling_price' ,
            'deposit' ,'interest' ,'discount','start_date','end_date' , 'pos_note', 'debit'
        ]);

        $CustomerID = (new Customer())->IsCustomerExist($inputs['customer_name'],true );
        $PaymentSystemID = (new PaymentSystem())->IsPaymentSystemExist($inputs['payment_system'] ,true );
        $ItemID = (new Item())->IsItemExist($inputs['item_name'] ,true );

        $data['Customer_ID'] = isset($inputs['customer_name']) ? $CustomerID : '0';
        $data['PaymentSystem_ID'] = isset($inputs['payment_system']) ? $PaymentSystemID : '0';
        $data['Item_ID'] = isset($inputs['item_name']) ? $ItemID : '0';

        $data['PurchasingPrice'] = isset($inputs['purchasing_price']) ? trim(strip_tags($inputs['purchasing_price'])) : '0';
        $data['SellingPrice'] = isset($inputs['selling_price']) ? trim(strip_tags($inputs['selling_price'])) : '0';

        $data['Deposit'] = isset($inputs['deposit']) ? trim(strip_tags($inputs['deposit'])) : '0';
        $data['Interest'] = isset($inputs['interest']) ? trim(strip_tags($inputs['interest'])) : '0';
        $data['Discount'] = isset($inputs['discount']) ? trim(strip_tags($inputs['discount'])) : '0';

        $data['Debit'] = isset($inputs['debit']) ? trim(strip_tags($inputs['debit'])) : '0';

        $data['StartDate'] = isset($inputs['start_date']) ? trim(strip_tags($inputs['start_date'])) : '';
        $data['EndDate'] = isset($inputs['end_date']) ? trim(strip_tags($inputs['end_date'])) : '';

        $data['Notes'] = isset($inputs['pos_note']) ? trim(strip_tags($inputs['pos_note'])) : '';

        return $data;
    }

    /**
     * @param $CategoryID
     * @param bool $Encrypted
     * @return bool
     */
    public function IsPOSExist($POSID ,$Encrypted = false )
    {
        try {

            $POSID = $Encrypted ? Crypt::decrypt($POSID) : $POSID ;

            if (count($this->find($POSID))){

                return $POSID;

            }else{
                return false;
            }

        } catch (\Exception $e) {

            return false;

        }
    }

    /**
     * @param null $inputs
     * @return mixed
     */
    public function ValidateInputs($inputs = null, $ItemID = null ){

        $rules = [
            'Customer_ID' => 'integer|required|exists:tblCustomers,CustomerID',
            'Item_ID' => 'integer|required|exists:tblItems,ItemID',
            'PaymentSystem_ID' => 'integer|required|exists:tblPaymentSystem,PaymentID',

            'StartDate' => 'required|date|date_format:"Y-m-d"' ,
            'EndDate' => 'required|date|date_format:"Y-m-d"|after:StartDate' ,

            'PurchasingPrice' => 'required|numeric|digits_between:1,5|min:0|max:65000' ,
            'SellingPrice' => 'required|numeric|digits_between:1,5|max:65000|min:' . ($inputs['PurchasingPrice'] + 1) ,
            'Deposit' => 'required|numeric|digits_between:1,5|min:0|max:' . ($inputs['SellingPrice']) ,
            'Discount' => 'required|numeric|digits_between:1,5|min:0|max:' . ($inputs['SellingPrice']) ,
            'Interest' => 'required|numeric|digits_between:1,5|min:0|max:1000'  ,
            'Debit' => 'required|numeric|digits_between:1,5||min:0' , // |max:' . ($inputs['SellingPrice'])    cannot set max for debit

            'Notes' => 'max:1000' ,
        ];

        $errors = Validator::make($inputs ,$rules ,$this->_ValidationMessage() )->messages()->toArray();

        return $errors;
    }

    /**
     * Item belong to Customer
     *
     * @relationship
     * */
    public function Customer()
    {
        return $this->belongsTo('App\Http\Models\Customer' ,'Customer_ID' ,'CustomerID')->select();
    }

    /**
     * Item belong to Item
     *
     * @relationship
     * */
    public function Item()
    {
        return $this->belongsTo('App\Http\Models\Item' ,'Item_ID' ,'ItemID')->select();
    }

    /**
     * Item belong to Payment System
     *
     * @relationship
     * */
    public function PaymentSystem()
    {
        return $this->belongsTo('App\Http\Models\PaymentSystem' ,'PaymentSystem_ID' ,'PaymentID')->select();
    }

    /**
     * Validation Message
     * @return array
     */
    private function _ValidationMessage(){

        return array(
            'Customer_ID.required' => trans('pos.validation.Customer_ID.required') ,
            'Customer_ID.integer' => trans('pos.validation.Customer_ID.exists') ,
            'Customer_ID.exists' => trans('pos.validation.Customer_ID.exists') ,

            'Item_ID.required' => trans('pos.validation.Item_ID.required') ,
            'Item_ID.integer' => trans('pos.validation.Item_ID.exists') ,
            'Item_ID.exists' => trans('pos.validation.Item_ID.exists') ,


            'StartDate.required' => trans('pos.validation.StartDate.required') ,
            'StartDate.date' => trans('pos.validation.StartDate.v') ,
            'StartDate.date_format' => trans('pos.validation.StartDate.date_format') ,

            'EndDate.required' => trans('pos.validation.EndDate.required') ,
            'EndDate.date' => trans('pos.validation.EndDate.date') ,
            'EndDate.date_format' => trans('pos.validation.EndDate.date_format') ,
            'EndDate.after' => trans('pos.validation.EndDate.after') ,

            'SellingPrice.required' => trans('pos.validation.SellingPrice.required') ,
            'SellingPrice.numeric' => trans('pos.validation.SellingPrice.numeric') ,
            'SellingPrice.digits_between' => trans('pos.validation.SellingPrice.digits_between') ,
            'SellingPrice.min' => trans('pos.validation.SellingPrice.min') ,
            'SellingPrice.max' => trans('pos.validation.SellingPrice.max') ,

            'PurchasingPrice.required' => trans('pos.validation.PurchasingPrice.required') ,
            'PurchasingPrice.numeric' => trans('pos.validation.PurchasingPrice.numeric') ,
            'PurchasingPrice.digits_between' => trans('pos.validation.PurchasingPrice.digits_between') ,
            'PurchasingPrice.min' => trans('pos.validation.PurchasingPrice.min') ,
            'PurchasingPrice.max' => trans('pos.validation.PurchasingPrice.max') ,

            'Deposit.required' => trans('pos.validation.Deposit.required') ,
            'Deposit.numeric' => trans('pos.validation.Deposit.numeric') ,
            'Deposit.digits_between' => trans('pos.validation.Deposit.digits_between') ,
            'Deposit.min' => trans('pos.validation.Deposit.min') ,
            'Deposit.max' => trans('pos.validation.Deposit.max') ,

            'Discount.required' => trans('pos.validation.Discount.required') ,
            'Discount.numeric' => trans('pos.validation.Discount.numeric') ,
            'Discount.digits_between' => trans('pos.validation.Discount.digits_between') ,
            'Discount.min' => trans('pos.validation.Discount.min') ,
            'Discount.max' => trans('pos.validation.Discount.max') ,

            'Interest.required' => trans('pos.validation.Interest.required') ,
            'Interest.numeric' => trans('pos.validation.Interest.numeric') ,
            'Interest.digits_between' => trans('pos.validation.Interest.digits_between') ,
            'Interest.min' => trans('pos.validation.Interest.min') ,
            'Interest.max' => trans('pos.validation.Interest.max') ,

            'Debit.required' => trans('pos.validation.Debit.required') ,
            'Debit.numeric' => trans('pos.validation.Debit.numeric') ,
            'Debit.digits_between' => trans('pos.validation.Debit.digits_between') ,
            'Debit.min' => trans('pos.validation.Debit.min') ,
            'Debit.max' => trans('pos.validation.Debit.max') ,

            'Notes.max' => trans('pos.validation.Notes.max') ,
        );
    }

    /**
     * Validate POS Payment Before Close
     * @param $POSID
     * @return bool
     */
    public function CheckAmountPaidIsEqualDebit($POSID){
        $Transaction = new Transaction();
        $Debit = $this->find($POSID)->Debit;
        $TotalAmountPaid = $Transaction->GetTotalOfAmountPaid($POSID) ;
        if ($Debit == $TotalAmountPaid){
            return true;
        }
        return false;
    }
}

<?php

namespace App\Http\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use \Illuminate\Support\Facades\Validator;

class PaymentDates extends Model {


    protected $primaryKey = "PDateID";
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tblPaymentDates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['PDateID' ,'PDate_Date' ,'ISDate' ,'PaymentSystem_ID' ];


    /*
     * @param inputs
     * */
    public function ClearInputs($inputs){

        $inputs = array_only($inputs , [ 'payment_system' ,'payment_date' ,'payment_is_date' ] );

        $PaymentDate['PaymentSystem_ID'] = isset($inputs['payment_system']) ? trim(strip_tags($inputs['payment_system'])) : '0';

        $PaymentDate['PaymentSystem_ID'] = (new PaymentSystem())->IsPaymentSystemExist($PaymentDate['PaymentSystem_ID'] ,true );

        $PaymentDate['PDate_Date'] = isset($inputs['payment_date']) ? trim(strip_tags($inputs['payment_date'])) : null ;

        $PaymentDate['ISDate'] = isset($inputs['payment_is_date']) ? trim(strip_tags($inputs['payment_is_date'])) : 1 ;

        return $PaymentDate;
    }

    /**
     * @param $PaymentDateID
     * @param bool $Encrypted
     * @return bool
     */
    public function IsPaymentDateExist($PaymentDateID ,$Encrypted = false )
    {
        $PaymentDateID = $Encrypted ? Crypt::decrypt($PaymentDateID) : $PaymentDateID ;

        try {
            if (count($this->find($PaymentDateID))){

                return $PaymentDateID;

            }else{
                return false;
            }

        } catch (\Exception $e) {

            return false;

        }
    }

    /**
     * @param null $inputs
     * @return mixed
     */
    public function ValidateInputs($inputs = null, $ItemID = null ){

        $rules = [

            'PDate_Date' => 'required|date|date_format:"Y-m-d"' ,
            'PaymentSystem_ID' => 'required|integer|exists:tblPaymentSystem,PaymentID' ,
            'ISDate' => 'boolean'
        ];

        $errors = Validator::make($inputs ,$rules ,$this->_ValidationMessage() )->messages()->toArray();

        if (!$errors){

            $this->PaymentDateIsUnique($inputs ,$errors );
            $this->CheckDateInLeapYear($inputs ,$errors );
            $this->CheckProfitIsOnlyOneTime($inputs ,$errors );

        }

        return $errors;
    }

    /**
     * Item belong to Category
     *
     * @relationship
     * */
    public function PaymentSystem()
    {

        return $this->belongsTo('App\Http\Models\PaymentSystem' ,'PaymentSystem_ID' ,'PaymentID')->select();

    }

    /**
     * Validation Message
     * @return array
     */
    private function _ValidationMessage(){
        return array(

            'PDate_Date.required' => trans('psystemdates.validation.PDate_Date.required'),
            'PDate_Date.date' => trans('psystemdates.validation.PDate_Date.date'),
            'PDate_Date.date_format' => trans('psystemdates.validation.PDate_Date.date_format'),

            'PaymentSystem_ID.required' => trans('psystemdates.validation.PaymentSystem_ID.required'),
            'PaymentSystem_ID.integer' => trans('psystemdates.validation.PaymentSystem_ID.exists'),
            'PaymentSystem_ID.exists' => trans('psystemdates.validation.PaymentSystem_ID.exists'),

            'ISDate.boolean' => trans('psystemdates.validation.ISDate.boolean'),
        );
    }

    /**
     * Check payment Date Is Unique And Not repeated
     * Get Date Format as Month and day
     * check if this month and day already exist with select payment system
     * @param $inputs
     * @param null $errors
     * @return bool
     */
    private function PaymentDateIsUnique($inputs ,&$errors = []){

        $date = Carbon::parse($inputs['PDate_Date'])->format('m-d') ;

        $PaymentDate = $this
                ->whereRaw("DATE_FORMAT(tblPaymentDates.PDate_Date, '%m-%d') = '$date'")
                ->where(array_only($inputs , ['PaymentSystem_ID' ,'ISDate' ]))
                ->get()->count();

        if ($PaymentDate){

            $errors = array_merge($errors ,['التاريخ الذي قمت بادخاله بالفعل مسجل من قبل']) ;

            return false;
        }

        return true;
    }

    /**
     * Check If Date In Leap Year
     * @param $date
     * @return bool
     */
    private function CheckDateInLeapYear($inputs ,&$errors = []){

        $date = Carbon::parse($inputs['PDate_Date'])->format('m-d') ;

        if ( $date == '2-29' || $date == '02-29' ){

            $errors = array_merge($errors , ['لا يمكنك اختيار يوم 29 - 02 لانه يعتبر سنة كبيسة']) ;

            return false;
        }

        return true;
    }

    /**
     * Check if date is Profit date
     * becuase profit date only add one time per payment system
     * @param $inputs
     * @param array $errors
     * @return bool
     */
    private function CheckProfitIsOnlyOneTime($inputs ,&$errors = [] ){
        // Check If Is Date select (Profit) Then Check is Already exist or not
        if ($inputs['ISDate'] == 0){

            //Is Profit
            $inputs['ISDate'] = '0' ;

            $PaymentDates = $this
                ->where(array_only($inputs , ['PaymentSystem_ID' ,'ISDate' ]))
                ->get()->count();

            if ($PaymentDates){

                $errors = array_merge($errors ,['من فضلك قم بتغير الحالة بالتاريخ لان الارباح يتم ادخالها مرة واحدة فقط']) ;

                return false;
            }
        }

        return true;

    }

    /**
     * Check If Payment Date Is Date
     * @param $date
     * @return string
     */
    public static function CheckIsPaymentDateISDate($date){

        $carbon_date = \DateTime::createFromFormat('Y-m-d' ,$date ) ? Carbon::parse($date)->format('m-d') : null ;

        if ($carbon_date){

            return PaymentDates::whereRaw("DATE_FORMAT(tblPaymentDates.PDate_Date, '%m-%d') = '$carbon_date'")->get()->first()->ISDate ;

        }
        return false;
    }
}

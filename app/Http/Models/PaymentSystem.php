<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Validator;

class PaymentSystem extends Model {


  protected $primaryKey = "PaymentID";
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'tblPaymentSystem';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['PaymentID' ,'PaymentAlias' ,'PaymentDescription' ,'disabled' ];


  /*
   * @param inputs
   * */
  public function ClearInputs($inputs){


    $inputs = array_only($inputs ,
      ['payment_id' ,'payment_alias' ,'payment_desc' ,'enable']
    );

    $paymentsystem['PaymentAlias'] = isset($inputs['payment_alias']) ? trim(strip_tags($inputs['payment_alias'])) : '';

    $paymentsystem['PaymentDescription'] = isset($inputs['payment_desc']) ? trim(strip_tags($inputs['payment_desc'])) : '';

    $paymentsystem['disabled'] = isset($inputs['enable']) && $inputs['enable']== 'on' ? 0 : 1 ;

    return $paymentsystem;
  }

  /**
   * @param $CategoryID
   * @param bool $Encrypted
   * @return bool
   */
  public function IsPaymentSystemExist($PaymentSystemID ,$Encrypted = false )
  {
      try {

          $PaymentSystemID = $Encrypted ? Crypt::decrypt($PaymentSystemID) : $PaymentSystemID ;

          if (count($this->find($PaymentSystemID))){

              return $PaymentSystemID;

          }else{
              return false;
          }

      } catch (\Exception $e) {

          return false;

      }
  }

  /**
   * @param null $inputs
   * @return mixed
   */
  public function ValidateInputs($inputs = null, $PaymentSystemID = null ){

    $rules = [
      'PaymentAlias' => 'min:2|max:160|required|unique:tblPaymentSystem,PaymentAlias,'.$PaymentSystemID.',PaymentID' ,
      'PaymentDesc' => 'max:1000' ,
      'disabled' => 'boolean'
    ];

    $errors = Validator::make($inputs ,$rules ,$this->_ValidationMessage() )->messages()->toArray();

    return $errors;
  }

  /**
   * Validation Message
   * @return array
   */
  private function _ValidationMessage(){
    return array(
      'PaymentAlias.required' => trans('psystem.validation.psystemname.required'),
      'PaymentAlias.max' => trans('psystem.validation.psystemname.max'),
      'PaymentAlias.min' => trans('psystem.validation.psystemname.min'),
      'PaymentAlias.unique' => trans('psystem.validation.psystemname.unique'),

      'PaymentDesc.max' => trans('psystem.validation.paymentdesc.max'),

      'disabled.boolean' => trans('psystem.validation.disabled.boolean'),
    );
  }

    /**
     * Get Sub Category
     * @return mixed
     */
    public function PaymentDates(){

        return $this->hasMany('App\Http\Models\PaymentDates' ,'PaymentSystem_ID' ,$this->primaryKey)->select();

    }
}

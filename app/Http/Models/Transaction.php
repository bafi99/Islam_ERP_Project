<?php namespace App\Http\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Validator;

class Transaction extends Model {


    protected $primaryKey = "TransactionID";

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbltransactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'POS_ID' ,'PaymentDate' ,'DateOfPaid' ,'ExpectedAmount' ,'AmountPaid' ,'Notes' ,'Done','Postpone_to'
    ];


    /**
     * @param $CategoryID
     * @param bool $Encrypted
     * @return bool
     */
    public function IsTransactionExist($TransactionID ,$Encrypted = false )
    {
        try {

            $TransactionID = $Encrypted ? Crypt::decrypt($TransactionID) : $TransactionID ;

            if (count($this->find($TransactionID))){

                return $TransactionID;

            }else{
                return false;
            }

        } catch (\Exception $e) {

            return false;

        }
    }

    /**
     * Generate Transactions Dates
     * Check if this the first time to generate transactions
     * get start | end date {year}
     * loop on each payment date in selected system
     * get payment date as variable
     * loop from start year to end year
     * Check Date/{y} if before end date && after start date
     * @param $POS
     * @return array of generated dates sort by ASC
     */
    public function GenerateTransactionsDates($POS){

        // Set Default as Null
        $GeneratedDates = [];

        $PaymentSystemDates = $POS->PaymentSystem()->first()->PaymentDates()->get();

        $start_date_year = \Carbon\Carbon::parse($POS->StartDate)->year;
        $end_date_year = \Carbon\Carbon::parse($POS->EndDate)->year;

        // Get all Payment dates
        foreach($PaymentSystemDates as $PaymentSystemDate ){
            // get payment date
            $PaymentDate = \Carbon\Carbon::parse($PaymentSystemDate->PDate_Date);
            // loop on years and check date every year
            for ($y = $start_date_year ; $y <= $end_date_year ; $y++ ){
                // temp variable to set fummy date
                $tmp_date_timestamp = \Carbon\Carbon::create($y ,$PaymentDate->month ,$PaymentDate->day )->getTimestamp();
                // Check payment date is greater than start date and less than end date
                if ($tmp_date_timestamp > (strtotime($POS->StartDate) + (60*60*24) ) && $tmp_date_timestamp < (strtotime($POS->EndDate) + (60*60*24))  ){
                    $GeneratedDates[\Carbon\Carbon::createFromTimestamp($tmp_date_timestamp)->format('Y-m-d')] = ['is_date' => $PaymentSystemDate->ISDate] ;
                }
            }
        }
        // Sort Date ASC
        ksort($GeneratedDates);
        return $GeneratedDates;
    }

    /*
     * @param inputs
     * */
    public function ClearInputs($inputs ,$TransactionID = null ){

        $inputs = array_only($inputs ,[
            'payment_date' ,'expected_paid' ,'amount_paid' ,'date_of_paid' ,'is_paid','notes' ,'expected_paid_status'
        ]);
        $CurrentTransaction = Transaction::find($TransactionID);

        $Transaction['POS_ID'] = $CurrentTransaction->POS_ID;

        $Transaction['PaymentDate'] = isset($inputs['payment_date']) ? trim(strip_tags($inputs['payment_date'])) : null ;

        $Transaction['DateOfPaid'] = isset($inputs['date_of_paid']) && strlen($inputs['date_of_paid']) ? trim(strip_tags($inputs['date_of_paid'])) : null ;

        $Transaction['AmountPaid'] = isset($inputs['amount_paid']) ? trim(strip_tags($inputs['amount_paid'])) : '0';

        $Transaction['Notes'] = isset($inputs['notes']) ? trim(strip_tags($inputs['notes'])) : '';

        $Transaction['Done'] = isset($inputs['is_paid']) ? trim(strip_tags($inputs['is_paid'])) : 0;

        // check if user want to update expected paid status or get original expected amount from database
        if (isset($inputs['expected_paid_status']) && $inputs['expected_paid_status'] == '1' ){
            $Transaction['ExpectedAmount'] = isset($inputs['expected_paid']) ? trim(strip_tags($inputs['expected_paid'])) : '0';
        }else{
            $Transaction['ExpectedAmount'] = $CurrentTransaction->ExpectedAmount;
        }

        return $Transaction;
    }

    /**
     * @param null $inputs
     * @return mixed
     */
    public function ValidateInputs($inputs = null, $TransactionID = null ){

        $POS = POS::find($inputs['POS_ID']);
        $CurrentAmountPaidTransaction = $CurrentExpectedTransaction = 0 ;
        if ($TransactionID){
            $Transaction = Transaction::find($TransactionID) ;
            $CurrentExpectedTransaction = $Transaction->ExpectedAmount ;
            $CurrentAmountPaidTransaction = $Transaction->AmountPaid ;
        }

        $rules = [
            'POS_ID' => 'integer|required|exists:tblPOS,POSID',
            'PaymentDate' => 'required|date|date_format:"Y-m-d"|unique:tbltransactions,PaymentDate,'.$TransactionID.',TransactionID,POS_ID,'.$POS->POSID.'|after:'. $POS->StartDate .'|before:'.$POS->EndDate ,
            'DateOfPaid' => 'date|date_format:"Y-m-d"' ,
            'ExpectedAmount' => 'required|numeric|digits_between:1,5|min:0|max:' . ( $POS->Debit - $this->GetTotalOfInsertedExpectedAmount($POS->POSID) + $CurrentExpectedTransaction )  ,
            'AmountPaid' => 'required|numeric|digits_between:1,5|min:0|max:' . ( $POS->Debit - $this->GetTotalOfAmountPaid($POS->POSID) + $CurrentAmountPaidTransaction) ,
            'Notes' => 'max:1000' ,
            'Done' => 'boolean',
        ];

        $errors = Validator::make($inputs ,$rules ,$this->_ValidationMessage() )->messages()->toArray();

        return $errors;
    }

    /**
     * Custom Validate Message
     * @return array
     */
    private function _ValidationMessage(){
        $message = [
            'x' => 'y'
        ];

        return $message;
    }

    /**
     * Validate All Transactions
     * @param \Illuminate\Http\Request $request
     * @param $POSID
     * @param int $TotalExpectedAmount
     * @param null $all_transactions
     * @return null
     */
    public function ValidateAllTransactions(\Illuminate\Http\Request $request ,$POSID ,&$TotalExpectedAmount = 0 ,&$all_transactions = null){

        $PaymentSystemID = POS::find($POSID)->PaymentSystem_ID;
        $PaymentDates = new PaymentDates();

        foreach ($request->get('expected_paid') as $date => $expected_paid) {

            $all_transactions[$date] =
            $transaction = [
                'POS_ID' => $POSID ,
                'PaymentDate' => isset($date) ? $date : null ,
                'ExpectedAmount' => isset($request->get('expected_paid')[$date]) ? $request->get('expected_paid')[$date] : 0 ,
                'AmountPaid' => isset($request->get('actually_paid')[$date]) ? $request->get('actually_paid')[$date] : 0 ,
                'Notes' => isset($request->get('note')[$date]) ? $request->get('note')[$date] : null ,
                'Done' => isset($request->get('has_paid')[$date]) ? $request->get('has_paid')[$date] : 0 ,
                'ISDate' => PaymentDates::CheckIsPaymentDateISDate($date) ,
                'DateOfPaid' => (isset($request->get('has_paid')[$date]) ? $request->get('has_paid')[$date] : 0 ) ? Carbon::today()->format('Y-m-d') : null
            ];
            // get error
            $errors = $this->ValidateInputs($transaction);
            if ($errors){
                return Redirect::to('transactions/create?pos=' . Crypt::encrypt($POSID))
                    ->with('errors' ,$errors )
                    ->with('error_date' ,$date )
                    ->withInput();
            }

            $TotalExpectedAmount += $transaction['ExpectedAmount'];
        }

        $TotalExpectedAmount += $this->GetTotalOfInsertedExpectedAmount($POSID);

        return null;
    }

    /**
     * Insert Block of Transaction
     * @param $view
     */
    public function InsertBlockOfTransactions(&$view ,$all_transactions ){
        // insert in database
        try{

            DB::beginTransaction();

            DB::table($this->getTable())->insert($all_transactions);

            DB::commit();

            $view->with('success' ,['Done'] );

        }catch(\Exception $e){

            DB::rollback();

            $view->with('errors' ,[$e->getMessage()] );
        }
    }

    /**
     * Get Total on inserted Expected Amount in transaction table
     * @param $POSID
     * @return mixed
     */
    private function GetTotalOfInsertedExpectedAmount($POSID){

        return $this->where(['POS_ID' => $POSID])->sum('ExpectedAmount');

    }
    /**
     * Get Total on inserted Expected Amount in transaction table
     * @param $POSID
     * @return mixed
     */
    public static function GetTotalOfAmountPaid($POSID){

        return Transaction::where(['POS_ID' => $POSID ,'Done' => '1'])->sum('AmountPaid');

    }

    /**
     * Transaction belong to POS
     *
     * @relationship
     * */
    public function POS()
    {

        return $this->belongsTo('App\Http\Models\POS' ,'POS_ID' ,'POSID')->select();

    }

    /**
     * Clear Postpone data before validate
     * @param $postpone_date
     * @return mixed
     */
    public function ClearPostpone($postpone_date){

        $input['Postpone_to'] = isset($postpone_date) ? trim(strip_tags($postpone_date)) : null ;
        return $input ;
    }

    /**
     * Validate Postpone date and all info
     * @param $inputs
     * @param $POS_ID
     * @return mixed
     */
    public function ValidatePostpone($inputs ,$POS_ID){
        $rules = [
            'Postpone_to' => 'required|date|date_format:"Y-m-d"|exists:'.$this->getTable().',PaymentDate,POS_ID,' . $POS_ID .',Done,0' ,
        ];

        return Validator::make($inputs ,$rules ,$this->_ValidationMessage() )->messages()->toArray();
    }

    /**
     * Get Postpone Transaction By Date
     * @param $POS_ID
     * @param $PostponeDate
     * @return mixed
     */
    public function GetPostponeTransactionByDate($POS_ID ,$PostponeDate){
        return $this->where(['POS_ID' => $POS_ID ,'PaymentDate' => $PostponeDate,'Done' => '0'])->first();
    }
}
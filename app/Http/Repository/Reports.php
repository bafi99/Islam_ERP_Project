<?php namespace App\Http\Repository;


use App\Http\Models\PaymentSystem;
use App\Http\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Reports
{

    private $request;

    /**
     * Init Request
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * get Next payment dates
     * @return mixed
     */
    public function getNextPaymentDates()
    {
        $StartDate = $this->request->get('from_date');
        $EndDate = $this->request->get('to_date');
        $PaymentStatus = $this->request->get('payment_status');

        $Query = Transaction::where('PaymentDate', '>=', $StartDate)->where('PaymentDate', '<=', $EndDate);

        $Query = $this->getPaymentStatus($Query, $PaymentStatus);

        $Query = $Query->orderBy('PaymentDate');

        return $Query;
    }

    /**
     * Get payment query status
     * is done or not done
     * @param $Query
     * @param $PaymentStatus
     * @return mixed
     */
    private function getPaymentStatus($Query, $PaymentStatus)
    {
        if (in_array($PaymentStatus, ['0', '1'])) {
            $Query = $Query->Where('Done', '=', $PaymentStatus);
        }
        return $Query;
    }

    /**
     * Print Report As PDF
     * @param $html
     * @param string $filesavename Name of output file
     * @param string $Orientation Portrait Or Landscape
     * @param int $FontSize
     */
    public function PDFReport($html ,$filesavename = 'output' ,$Orientation = 'L' ,$FontSize = 8  )
    {
        $pdf = new \TCPDF($Orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator('www.mgouda.net');
        $pdf->setRTL(true);
        $pdf->SetAuthor('Islam Alaa Expenses Project');
        $pdf->SetTitle('Expenses Mobile Report');
        $pdf->SetSubject('Report');
        $pdf->SetKeywords('Report, Expenses, Mobile');
        $pdf->SetHeaderData(null, 0, trans('report.report_signature_2') . '', trans('report.report_signature'), array(0, 0, 0), array(0, 64, 128));
        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));
        $pdf->setHeaderFont(Array('dejavusans', '', '8'));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(4, 17, 4);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', $FontSize, '', true);
        $pdf->AddPage();
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $pdf->Output("$filesavename.pdf", 'I');
    }

    /**
     * Get Remaining Transactions
     * @return array
     */
    public function getRemainingTransaction()
    {
        $POSS = [];
        $POS = Transaction::all()->groupBy('POS_ID');
        foreach ($POS as $POSID => $Transactions) {
            $POSS[$POSID]['all'] = count($Transactions);
            $POSS[$POSID]['done'] = 0;
            $POSS[$POSID]['undone'] = 0;
            foreach ($Transactions as $Transaction) {
                if (1 == $Transaction->Done) {
                    $POSS[$POSID]['done'] += 1;
                } else {
                    $POSS[$POSID]['undone'] += 1;
                }
            }
        }
        return $POSS;
    }

}

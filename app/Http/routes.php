<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => 'auth'], function () {
    Route::resources([
        'home' => 'HomeController',
        'category' => 'CategoryController',
        'item' => 'ItemController',
        'customer' => 'CustomerController',
        'paymentsystem' => 'PaymentSystemController',
        'paymentdates' => 'PaymentDatesController',
        'pos' => 'POSController',
        'transactions' => 'TransactionController',
    ]);
});
Route::group(['middleware' => 'auth'], function () {
    Route::get('/pos/confirm/{id}', 'POSController@Confirm');
    Route::patch('/pos/togglefinished/{id}', 'POSController@ToggleFinish');
    Route::patch('/transactions/postpone/{id}', 'TransactionController@Postpone');
});

Route::group(['prefix' => 'reports', 'middleware' => 'auth'], function () {
    Route::get('/', 'ReportsController@index');
    Route::get('/next_payment_dates', 'ReportsController@NextPaymentDates');
    Route::get('/customer_bill/{POSID}', 'ReportsController@CustomerBill');
    Route::get('/finicial_postion', 'ReportsController@FinicialPostion');
    Route::get('/remaining_transactions', 'ReportsController@RemainingTransactions');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/database/backup', function () {
        try {
            Artisan::call('db:backup',
                [
                    '--database' => 'mysql',
                    '--destination' => 'dropbox',
                    '--destinationPath' => date("d-m-Y H-i-s"),
                    '--compression' => 'gzip',
                ]
            );
            $redirect = Redirect::to('/category')->with('success', ['Please Check Your Dropbox and make sure Is Backup']);
        } catch (\Dropbox\Exception $ex) {
            $redirect = Redirect::to('/category')->with('errors', [$ex->getMessage()]);
        }
        return $redirect;
    });
});

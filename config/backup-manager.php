<?php

return [
    'local' => [
        'type' => 'Local',
        'root' => storage_path('app'),
    ],
    'dropbox' => [
        'type' => 'Dropbox',
        'token' => env('Dropbox_token', ''),
        'key' => env('Dropbox_key', ''),
        'secret' => env('Dropbox_secret', ''),
        'app' => env('Dropbox_app', ''),
        'root' => '',
    ],
];

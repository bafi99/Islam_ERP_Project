-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 05, 2015 at 01:59 AM
-- Server version: 5.6.27
-- PHP Version: 5.5.30-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `islam_erp-mobile`
--

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblCategories`
--

CREATE TABLE IF NOT EXISTS `tblCategories` (
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `CatName` varchar(160) NOT NULL,
  `CatDesc` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `tblCategories`
--

INSERT INTO `tblCategories` (`CategoryID`, `CatName`, `CatDesc`, `created_at`, `updated_at`, `disabled`) VALUES
(21, 'Sony', 'All Sony Mobile', '2015-11-13 12:19:30', '2015-11-13 12:19:30', 0),
(22, 'HTC', 'HTC Mobiles', '2015-11-13 12:19:45', '2015-11-13 12:19:45', 0),
(23, 'Samsung', '', '2015-11-13 12:19:57', '2015-11-13 12:19:57', 0),
(24, 'مجموعة اجهزة', '', '2015-11-27 11:45:38', '2015-11-27 11:46:09', 0),
(25, 'asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdv', '', '2015-12-02 19:02:59', '2015-12-02 19:02:59', 1),
(26, 'asdasd', '', '2015-12-02 19:06:48', '2015-12-02 19:06:48', 1),
(27, 'asdasgfg', '', '2015-12-02 19:07:15', '2015-12-02 19:07:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblCustomers`
--

CREATE TABLE IF NOT EXISTS `tblCustomers` (
  `CustomerID` int(11) NOT NULL AUTO_INCREMENT,
  `CustName` varchar(160) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CustMobile1` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustMobile2` varchar(11) DEFAULT NULL,
  `CustAddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustNote` text,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`CustomerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tblCustomers`
--

INSERT INTO `tblCustomers` (`CustomerID`, `CustName`, `CustMobile1`, `CustMobile2`, `CustAddress`, `CustNote`, `disabled`, `created_at`, `updated_at`) VALUES
(4, 'Ahmed Elsayed', '01282054848', '', 'mami 45,', 'Nice Customer', 0, '2015-11-13 12:29:41', '2015-11-13 12:29:41'),
(5, 'Osama Alaa', '01229945960', '', 'Toson', 'Good Customer', 0, '2015-11-13 12:30:21', '2015-11-13 12:30:21');

-- --------------------------------------------------------

--
-- Table structure for table `tblItems`
--

CREATE TABLE IF NOT EXISTS `tblItems` (
  `ItemID` int(11) NOT NULL AUTO_INCREMENT,
  `ItemName` varchar(160) NOT NULL,
  `ItemPrice` float DEFAULT '0',
  `ItemDesc` text,
  `ItemExpectedPrice` float DEFAULT '0',
  `cat_id` int(11) DEFAULT '0',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ItemID`),
  KEY `fk_tblItems_catid_FROM_catgories_categoryid_idx` (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tblItems`
--

INSERT INTO `tblItems` (`ItemID`, `ItemName`, `ItemPrice`, `ItemDesc`, `ItemExpectedPrice`, `cat_id`, `disabled`, `created_at`, `updated_at`) VALUES
(4, 'Z2 Quad Core 1.7', 1000, 'Iten Description', 0, 21, 0, '2015-11-13 12:23:17', '2015-11-13 12:23:17'),
(5, '820 Ocata 1.2', 2000, '', 2500, 22, 0, '2015-11-13 12:25:26', '2015-11-13 12:25:44'),
(7, 'Item', 0, '', 3100, 21, 0, '2015-11-14 21:06:42', '2015-11-14 21:06:42'),
(8, 'زد تو + ان تي سي', 0, '', 2500, 24, 0, '2015-11-27 11:45:18', '2015-11-27 11:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `tblPaymentDates`
--

CREATE TABLE IF NOT EXISTS `tblPaymentDates` (
  `PDateID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PDate_Date` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ISDate` tinyint(4) NOT NULL DEFAULT '1',
  `PaymentSystem_ID` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PDateID`),
  KEY `fk_tblPaymentDates_paymentsystemid_FROM_tblpaymentdates_PDa_idx` (`PaymentSystem_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `tblPaymentDates`
--

INSERT INTO `tblPaymentDates` (`PDateID`, `PDate_Date`, `ISDate`, `PaymentSystem_ID`, `created_at`, `updated_at`) VALUES
(50, '2015-01-01', 1, 5, '2015-11-27 11:20:29', '2015-11-27 11:20:29'),
(51, '2015-02-01', 1, 5, '2015-11-27 11:20:47', '2015-11-27 11:20:47'),
(52, '2015-03-01', 1, 5, '2015-11-27 11:20:53', '2015-11-27 11:20:53'),
(53, '2015-04-01', 1, 5, '2015-11-27 11:21:03', '2015-11-27 11:21:03'),
(54, '2015-05-01', 1, 5, '2015-11-27 11:21:23', '2015-11-27 11:21:23'),
(55, '2015-06-01', 1, 5, '2015-11-27 11:21:39', '2015-11-27 11:21:39'),
(56, '2015-07-01', 1, 5, '2015-11-27 11:21:45', '2015-11-27 11:21:45'),
(57, '2015-08-01', 1, 5, '2015-11-27 11:21:52', '2015-11-27 11:21:52'),
(58, '2015-09-01', 0, 5, '2015-11-27 11:22:00', '2015-11-27 11:25:23'),
(59, '2015-10-01', 1, 5, '2015-11-27 11:22:28', '2015-11-27 11:22:28'),
(60, '2015-11-01', 1, 5, '2015-11-27 11:22:35', '2015-11-27 11:22:35'),
(61, '2015-12-01', 1, 5, '2015-11-27 11:22:42', '2015-11-27 11:22:42'),
(62, '2015-09-20', 1, 5, '2015-11-27 11:25:51', '2015-11-27 11:25:51'),
(63, '2015-01-10', 1, 6, '2015-11-27 11:57:22', '2015-11-27 11:57:22'),
(64, '2015-01-24', 1, 6, '2015-11-27 11:57:35', '2015-11-27 11:57:35'),
(65, '2015-02-24', 1, 6, '2015-11-27 11:57:40', '2015-11-27 11:57:40'),
(66, '2015-03-24', 1, 6, '2015-11-27 11:57:45', '2015-11-27 11:57:45'),
(67, '2015-04-24', 1, 6, '2015-11-27 11:57:52', '2015-11-27 11:57:52'),
(68, '2015-05-24', 1, 6, '2015-11-27 11:58:00', '2015-11-27 11:58:00'),
(69, '2016-06-24', 1, 6, '2015-11-27 11:59:20', '2015-11-27 11:59:20'),
(70, '2015-07-24', 1, 6, '2015-11-27 11:59:39', '2015-11-27 11:59:39'),
(71, '2015-08-24', 1, 6, '2015-11-27 11:59:45', '2015-11-27 11:59:45'),
(72, '2015-09-24', 1, 6, '2015-11-27 11:59:51', '2015-11-27 11:59:51'),
(73, '2015-10-24', 1, 6, '2015-11-27 11:59:57', '2015-11-27 11:59:57'),
(74, '2015-11-24', 1, 6, '2015-11-27 12:00:09', '2015-11-27 12:00:09'),
(75, '2015-12-24', 1, 6, '2015-11-27 12:03:14', '2015-11-27 12:03:14'),
(76, '2015-02-10', 1, 6, '2015-11-27 12:04:23', '2015-11-27 12:04:23'),
(77, '2015-03-10', 1, 6, '2015-11-27 12:04:45', '2015-11-27 12:04:45'),
(78, '2015-04-10', 1, 6, '2015-11-27 12:04:58', '2015-11-27 12:04:58'),
(79, '2015-05-10', 1, 6, '2015-11-27 12:05:04', '2015-11-27 12:05:04'),
(80, '2015-06-10', 1, 6, '2015-11-27 12:05:13', '2015-11-27 12:05:13'),
(81, '2015-07-10', 1, 6, '2015-11-27 12:05:21', '2015-11-27 12:05:21'),
(82, '2015-08-10', 1, 6, '2015-11-27 12:05:30', '2015-11-27 12:05:30'),
(83, '2015-09-10', 1, 6, '2015-11-27 12:05:38', '2015-11-27 12:05:38'),
(84, '2015-10-10', 1, 6, '2015-11-27 12:05:48', '2015-11-27 12:05:48'),
(85, '2015-11-10', 1, 6, '2015-11-27 12:05:56', '2015-11-27 12:05:56'),
(86, '2015-12-10', 1, 6, '2015-11-27 12:06:14', '2015-11-27 12:06:14'),
(87, '2015-09-20', 0, 6, '2015-11-27 12:07:51', '2015-11-27 12:07:51'),
(88, '2016-12-02', 1, 5, '2015-12-01 20:48:16', '2015-12-01 20:48:16'),
(89, '2015-01-02', 1, 5, '2015-12-01 20:48:34', '2015-12-01 20:48:34'),
(90, '2015-06-30', 1, 5, '2015-12-01 20:49:18', '2015-12-01 20:49:18'),
(91, '2015-12-10', 1, 7, '2015-12-01 21:19:05', '2015-12-01 21:19:05'),
(92, '2015-12-14', 1, 7, '2015-12-01 21:19:11', '2015-12-01 21:19:11'),
(93, '2015-12-24', 1, 7, '2015-12-01 21:19:15', '2015-12-01 21:19:15'),
(94, '2015-11-10', 1, 7, '2015-12-01 21:19:22', '2015-12-01 21:19:22'),
(95, '2015-11-14', 1, 7, '2015-12-01 21:19:56', '2015-12-01 21:19:56'),
(96, '2015-12-7', 1, 7, '2015-12-01 21:20:26', '2015-12-01 21:20:26'),
(97, '2015-12-15', 1, 7, '2015-12-01 21:20:33', '2015-12-01 21:20:33');

-- --------------------------------------------------------

--
-- Table structure for table `tblPaymentSystem`
--

CREATE TABLE IF NOT EXISTS `tblPaymentSystem` (
  `PaymentID` int(11) NOT NULL AUTO_INCREMENT,
  `PaymentAlias` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PaymentDescription` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PaymentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tblPaymentSystem`
--

INSERT INTO `tblPaymentSystem` (`PaymentID`, `PaymentAlias`, `PaymentDescription`, `disabled`, `created_at`, `updated_at`) VALUES
(5, 'النظام الاول', 'System Descrip[tion f', 0, '2015-11-13 12:31:08', '2015-12-02 21:49:29'),
(6, 'النظام الثاني يوم 10 و 24 فقط', 'Any Description', 0, '2015-11-13 12:31:57', '2015-11-27 12:36:24'),
(7, 'النظام الثالث يوم 10 -14-24', 'Descripyion Number Three', 0, '2015-11-13 12:32:11', '2015-11-27 12:35:53');

-- --------------------------------------------------------

--
-- Table structure for table `tblPOS`
--

CREATE TABLE IF NOT EXISTS `tblPOS` (
  `POSID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Customer_ID` int(11) NOT NULL,
  `PaymentSystem_ID` int(11) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Deposit` float NOT NULL DEFAULT '0',
  `Interest` float NOT NULL DEFAULT '0',
  `PurchasingPrice` float NOT NULL DEFAULT '0',
  `SellingPrice` float NOT NULL DEFAULT '0',
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Notes` text,
  `Discount` float NOT NULL DEFAULT '0',
  `Finished` tinyint(4) NOT NULL DEFAULT '0',
  `Debit` float NOT NULL DEFAULT '0',
  `Confirm` tinyint(4) NOT NULL DEFAULT '0',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`POSID`),
  KEY `fk_tblItems_catid_FROM_catgories_categoryid0_idx` (`Item_ID`),
  KEY `fk_tblPOS_customerid_FROM_tblcustomers_Customerid_idx` (`Customer_ID`),
  KEY `fk_tblPOS_paymentsystem_id_FROM_tblpaymentsystems_paymentsy_idx` (`PaymentSystem_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tblPOS`
--

INSERT INTO `tblPOS` (`POSID`, `Customer_ID`, `PaymentSystem_ID`, `Item_ID`, `Deposit`, `Interest`, `PurchasingPrice`, `SellingPrice`, `StartDate`, `EndDate`, `Notes`, `Discount`, `Finished`, `Debit`, `Confirm`, `disabled`, `created_at`, `updated_at`) VALUES
(3, 5, 6, 4, 600, 30, 2800, 3200, '2015-04-01', '2016-04-01', 'الحساب يشمل الجراب و الاسكرين', 100, 0, 3280, 1, 0, '2015-11-27 12:17:25', '2015-12-01 21:17:47'),
(5, 4, 7, 5, 1000, 0, 2000, 5000, '2015-01-01', '2016-01-01', '', 0, 0, 4000, 1, 0, '2015-12-01 21:18:29', '2015-12-01 21:18:36');

-- --------------------------------------------------------

--
-- Table structure for table `tbltransactions`
--

CREATE TABLE IF NOT EXISTS `tbltransactions` (
  `TransactionID` bigint(20) NOT NULL AUTO_INCREMENT,
  `POS_ID` bigint(20) NOT NULL,
  `PaymentDate` date NOT NULL,
  `ISDate` tinyint(4) NOT NULL DEFAULT '1',
  `DateOfPaid` date DEFAULT NULL,
  `AmountPaid` float NOT NULL DEFAULT '0',
  `ExpectedAmount` float NOT NULL DEFAULT '0',
  `Done` tinyint(4) NOT NULL DEFAULT '0',
  `Postpone_to` date DEFAULT NULL,
  `Notes` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TransactionID`),
  KEY `fk_tblPOS_itemid_FROM_tblitems_itemid0_idx` (`POS_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=159 ;

--
-- Dumping data for table `tbltransactions`
--

INSERT INTO `tbltransactions` (`TransactionID`, `POS_ID`, `PaymentDate`, `ISDate`, `DateOfPaid`, `AmountPaid`, `ExpectedAmount`, `Done`, `Postpone_to`, `Notes`, `created_at`, `updated_at`) VALUES
(100, 3, '2015-04-10', 1, '2015-11-29', 100, 141, 1, NULL, '', NULL, '2015-11-30 20:51:34'),
(101, 3, '2015-04-24', 1, '2015-11-29', 100, 131, 1, NULL, '', NULL, NULL),
(102, 3, '2015-05-10', 1, '2015-11-29', 100, 131, 1, NULL, '', NULL, NULL),
(103, 3, '2015-05-24', 1, '2015-11-29', 500, 131, 1, NULL, '', NULL, NULL),
(104, 3, '2015-06-10', 1, NULL, 200, 131, 1, NULL, '', NULL, NULL),
(106, 3, '2015-07-10', 1, NULL, 120, 120, 1, '2016-02-24', 'postpone to 2016-02-24', NULL, '2015-12-01 18:15:12'),
(107, 3, '2015-07-24', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(108, 3, '2015-08-10', 1, NULL, 0, 262, 0, NULL, ',there was (131) postponed from 2015-06-24 , ,there was (131) postponed from 2015-06-24', NULL, '2015-11-30 21:32:09'),
(109, 3, '2015-08-24', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(110, 3, '2015-09-10', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(111, 3, '2015-09-20', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(112, 3, '2015-09-24', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(113, 3, '2015-10-10', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(114, 3, '2015-10-24', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(115, 3, '2015-11-10', 1, NULL, 10, 10, 0, '2016-02-24', '', NULL, '2015-12-01 18:16:20'),
(116, 3, '2015-11-24', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(117, 3, '2015-12-10', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(118, 3, '2015-12-24', 1, NULL, 30, 30, 0, '2016-03-24', '', NULL, '2015-11-30 21:26:54'),
(119, 3, '2016-01-10', 1, NULL, 0, 131, 0, NULL, '', NULL, NULL),
(120, 3, '2016-01-24', 1, NULL, 0, 151, 0, NULL, '', NULL, '2015-11-30 21:16:35'),
(121, 3, '2016-02-10', 1, NULL, 0, 242, 0, NULL, ' ,there was (111) postponed from 2016-03-24', NULL, '2015-11-30 21:35:48'),
(122, 3, '2016-02-24', 1, NULL, 0, 370, 0, NULL, 'there was (11) postponed from 2015-07-10 ,there was (148) postponed from 2015-11-10', NULL, '2015-12-01 18:16:20'),
(152, 5, '2015-11-10', 1, NULL, 0, 571, 0, NULL, '', NULL, NULL),
(153, 5, '2015-11-14', 1, NULL, 0, 571, 0, NULL, '', NULL, NULL),
(154, 5, '2015-12-04', 1, NULL, 0, 571, 0, NULL, '', NULL, NULL),
(155, 5, '2015-12-06', 1, NULL, 0, 571, 0, NULL, '', NULL, NULL),
(156, 5, '2015-12-14', 1, NULL, 0, 571, 0, NULL, '', NULL, NULL),
(157, 5, '2015-12-15', 1, NULL, 0, 571, 0, NULL, '', NULL, NULL),
(158, 5, '2015-12-24', 1, NULL, 0, 574, 0, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `disabled`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Developer Account', 'admin@admin.com', '$2y$10$OfscOIn3G6nZteJLSXS0mO0YlS4NoPStANd4oD2fjdmGAcHPDwGwy', NULL, 0, 0, '2015-11-01 20:43:36', '2015-11-01 20:43:36');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tblItems`
--
ALTER TABLE `tblItems`
  ADD CONSTRAINT `fk_tblItems_catid_FROM_catgories_categoryid` FOREIGN KEY (`cat_id`) REFERENCES `tblCategories` (`CategoryID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tblPaymentDates`
--
ALTER TABLE `tblPaymentDates`
  ADD CONSTRAINT `fk_tblPaymentDates_paymentsystemid_FROM_tblpaymentdates_PDateID` FOREIGN KEY (`PaymentSystem_ID`) REFERENCES `tblPaymentSystem` (`PaymentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblPOS`
--
ALTER TABLE `tblPOS`
  ADD CONSTRAINT `fk_tblPOS_customerid_FROM_tblcustomers_Customerid` FOREIGN KEY (`Customer_ID`) REFERENCES `tblCustomers` (`CustomerID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tblPOS_itemid_FROM_tblitems_itemid` FOREIGN KEY (`Item_ID`) REFERENCES `tblItems` (`ItemID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tblPOS_paymentsys_id_FROM_tblpaymentsys_paymentsysID` FOREIGN KEY (`PaymentSystem_ID`) REFERENCES `tblPaymentSystem` (`PaymentID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tbltransactions`
--
ALTER TABLE `tbltransactions`
  ADD CONSTRAINT `fk_tblPOS_posid_FROM_tblpos_posid` FOREIGN KEY (`POS_ID`) REFERENCES `tblPOS` (`POSID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

errors = [];

var Ctransaction = {
    /**
     * Get Total value in Specific inputs
     * @param $inputs
     * @returns {number}
     */
    total_Value: function ($inputs){
        var amount = 0;
        $inputs.each(function (){
            amount += parseInt($(this).val()) ;
        })
        return amount;
    },
    /**
     * Set debit on each payment date
     * @param $debit
     */
    set_debit_on_each_payment_date : function ($debit){
        var all_rows = $('tbody').find('tr')
        var paymentPerMonth = parseInt($debit) / all_rows.length;
        all_rows.each(function(){
            $(this).find('.expected_paid').val( Math.round(paymentPerMonth) )
        });
    },
    set_total_actullay_paid : function(){
        $('#total_ACValue').html('Total = ' + Ctransaction.total_Value( $('.actullay_paid') ));
    },
    set_total_expected_paid : function(){
        $('#total_EXValue').html('Total = ' + Ctransaction.total_Value( $('.expected_paid') ));
    },
    count_transactions : function(){
        $('#lbl_count_payments').html('Yout have (' + $('tbody').find('tr').length + ') Transaction');
    },
    check_debit_equal_expected :function(){
        var expected = parseInt(Ctransaction.total_Value( $('.expected_paid') ) ) ;
        var debit = parseInt($('#debit_value').val());
        if (expected === debit){
            $('#total_EXValue').attr('class' ,'btn btn-sm btn-success')
            return true;
        }else{

            if (debit > expected){
                alert('الباقي اكبر من المتوقع ب '+( debit - expected )+'')
            }else{
                alert('المتوقع دفعه اكبر من المتبقي ب '+(expected - debit)+'')
            }

        }

        $('#total_EXValue').attr('class' ,'btn btn-sm btn-danger')
        return false;
    },
    set_date_for_name_of_new_transaction : function($this){
        $('#new_expected_paid').attr('name' ,'expected_paid[' + $this.val() + ']')
        $('#new_actually_paid').attr('name' ,'actually_paid[' + $this.val() +']')
        $('#new_has_paid').attr('name' ,'has_paid[' + $this.val() +']')
        $('#new_note').attr('name' ,'note[' + $this.val() + ']')
    },
    total_transactions_expected : function (){
        return parseInt($('#new_expected_paid').val()) + Ctransaction.total_Value( $('.expected_transaction_paid') )
    },
    set_total_transaction_value : function(Selectedlabel){
        var debit = parseInt($('#debit_value').val());
        var total_expected = Ctransaction.total_transactions_expected()
        if (total_expected < debit){
            // red flage
            Selectedlabel.html('Total Expected ('+total_expected+')').attr('class' ,'btn-sm btn-danger')
            alert('Please add (' + (debit - total_expected) + ') LE .' )
        }else if (total_expected > debit){
            // warning flag
            Selectedlabel.html('Total Expected ('+total_expected+')').attr('class' ,'btn-sm btn-warning')
            alert('Please Remove (' + (total_expected - debit) + ') LE .' )
        }else if (total_expected == debit){
            // green flag
            Selectedlabel.html('Total Expected ('+total_expected+')').attr('class' ,'btn-sm btn-success')
        }
    }
};



var events = {
    init : function(){

        Ctransaction.set_debit_on_each_payment_date( $('#debit_value').val() );
        Ctransaction.set_total_actullay_paid();
        Ctransaction.set_total_expected_paid()
        Ctransaction.count_transactions()
        events.on_remove_row();
        events.on_actuall_changed();
        events.on_expected_changed();
        events.on_submit_transcations();
        events.on_get_total_of_expected();
        events.on_change_new_transaction_date();
        events.on_get_total_of_expected_transactionss();

    },
    on_remove_row : function(){
        // remove selected row
        $('body').on('click' ,'.removeRow',function(){
                if (confirm('سوف تقوم بحذف هذا التاريخ من المعاملات')){
                    $(this).closest('tr').remove();
                    Ctransaction.set_total_actullay_paid();
                    Ctransaction.set_total_expected_paid()
                    Ctransaction.count_transactions()
                }
            });
        },
    on_actuall_changed : function(){
        $('.actullay_paid').on('change' ,function(){
            Ctransaction.set_total_actullay_paid();
        });
    },
    on_expected_changed : function(){
        $('.expected_paid').on('change' ,function(){
            Ctransaction.set_total_expected_paid();
        });
    },
    on_submit_transcations : function(){
        $('#generateTransactions').on('submit' ,function(){
            if ( Ctransaction.check_debit_equal_expected() ){
                return true
            }
            return false;
        })
    },
    on_get_total_of_expected : function(){
        $('#total_EXValue').on('click' ,function(){
            Ctransaction.check_debit_equal_expected();
        })
    },
    on_change_new_transaction_date :function() {
        $('#new_transaction_date').on('change' ,function(){
            Ctransaction.set_date_for_name_of_new_transaction($(this));
        });
    },
    on_get_total_of_expected_transactionss : function(){
        $('#total_expected_transactions').on('click' ,function(){
            Ctransaction.set_total_transaction_value($(this))
        })
    }




};

events.init();

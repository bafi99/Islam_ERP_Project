errors = [];

/**
 * Class Of Methods
 * @type {{addError: Function, resetError: Function, valid_date: Function, set_label_color: Function, Check_inputs: Function, validate_facade: Function, validate_inputs: Function, get_debit: Function}}
 */
var pos = {
    addError : function(ErrorMessage){
        errors.push(ErrorMessage);
    },
    resetError : function(){
        errors = [];
    },
    valid_date : function($date){
        if (Date.parse($date) ) {
            return true;
        }
        return false;
    },
    set_label_color: function($element ,$class ){
        $element.parent().find('label')
            .removeClass( 'btn-info btn-success btn-danger btn-error btn-warning btn-primary' )
            .addClass($class);
    },
    Check_inputs: function(){

        if ( !$.isNumeric($('[name=purchasing_price]').val()) ){
            $('[name=purchasing_price]').val('0')
        }
        if ( !$.isNumeric($('[name=selling_price]').val()) ){
            $('[name=selling_price]').val('0')
        }
        if ( !$.isNumeric($('[name=interest]').val()) ){
            $('[name=interest]').val('0')
        }
        if ( !$.isNumeric($('[name=deposit]').val()) ){
            $('[name=deposit]').val('0')
        }
        if ( !$.isNumeric($('[name=discount]').val()) ){
            $('[name=discount]').val('0')
        }
        if ( !pos.valid_date($('[name=start_date]').val()) ){
            $('[name=start_date]').val('2015-01-01')
        }
        if ( !pos.valid_date($('[name=end_date]').val()) ){
            $('[name=end_date]').val('2015-01-01')
        }
    },
    validate_facade: function(){

        pos.resetError();
        pos.Check_inputs();
        pos.validate_select_boxes()
        pos.validate_inputs()


    },
    validate_inputs : function(){

        var SellingPrice = $('[name=selling_price]');
        var PurchasingPrice = $('[name=purchasing_price]');
        var discount = $('[name=discount]');
        var interest = $('[name=interest]');
        var deposit = $('[name=deposit]');
        var start_date = $('[name=start_date]');
        var end_date = $('[name=end_date]');

        // Check selling price is more then PurchasingPrice
        if (parseInt(PurchasingPrice.val()) == parseInt(SellingPrice.val())){
            pos.addError('سعر الشراء وسعر البيع متساويين , لا يوجد ارباح')
            pos.set_label_color(SellingPrice ,'btn-warning' )
        }else if ( parseInt(PurchasingPrice.val()) >= parseInt(SellingPrice.val()) ) {
            pos.addError('سعر الشراء يجب ان يكون اقل من سعر البيه , لا يوجد ربح')
            pos.set_label_color(SellingPrice ,'btn-danger' )
        }else{
            pos.set_label_color(SellingPrice ,'btn-success' )
        }
        // Check interest Value is less than Zero , And its not logic
        if (parseInt(interest.val()) < 0 ){
            pos.addError('الفائدة لا يمكن ان تقل عن صفر بالمائة')
            pos.set_label_color(interest ,'btn-danger' )
        }else{
            pos.set_label_color(interest ,'btn-info' )
        }

        // Check deposit Value is less than Zero , And its not logic
        if (parseInt(deposit.val()) < 0 ){
            pos.addError('المقدم لا يجب ان يكون اقل من صفر')
            pos.set_label_color(deposit ,'btn-danger' )
        }
        // Check If deposit is larger than selling price , its` not logic => dont forget to parse to int
        else if (parseInt(deposit.val()) > parseInt(SellingPrice.val()) ) {
            pos.addError('المقدم لا يجب ان يكون اكبر من سعر البيع')
            pos.set_label_color(deposit, 'btn-danger')
        }else {
            pos.set_label_color(deposit ,'btn-info' )
        }
        // Check Dsicount Value is less than Zero , And its not logic
        if (parseInt(discount.val()) < 0 ){
            pos.addError('التخفيض لا يمكن ان يكون اقل من صفر')
            pos.set_label_color(discount ,'btn-danger' )
        }else{
            pos.set_label_color(discount ,'btn-info' )
        }

        // Check End Date is after than Start Date
        if (new Date(start_date.val()) > new Date(end_date.val())){
            pos.addError('تاريخ الانتهاء يجب ان يلي تاريخ الابتداء')
            pos.set_label_color(start_date ,'btn-danger' )
        }else{
            pos.set_label_color(start_date ,'btn-info' )
        }
    },
    validate_select_boxes : function(){
        var customer_name = $('[name=customer_name]');
        var item_name = $('[name=item_name]');
        var payment_system = $('[name=payment_system]');

        // Check Customer Is selected
        if (parseInt(customer_name.val()) == 0  ){
            pos.addError('من فضلك اختار اسم العميل')
            pos.set_label_color(customer_name ,'btn-danger' )
        }else{
            pos.set_label_color(customer_name ,'btn-success' )
        }
        // Check item name Is selected
        if (parseInt(item_name.val()) == 0  ){
            pos.addError('من فضلك اختار الصنف الذي سيتم بيع للعميل')
            pos.set_label_color(item_name ,'btn-danger' )
        }else{
            pos.set_label_color(item_name ,'btn-success' )
        }
        // Check Customer Is selected
        if (parseInt(payment_system.val()) == 0  ){
            pos.addError('من فضلك اختار النظام الذي سيتبعه العميل')
            pos.set_label_color(payment_system ,'btn-danger' )
        }else{
            pos.set_label_color(payment_system ,'btn-success' )
        }
    },
    get_debit :function(){
        var selling_price = parseInt($('[name=selling_price]').val());
        var purchasing_price = parseInt($('[name=purchasing_price]').val());
        var discount = parseInt($('[name=discount]').val());
        var interest = parseInt($('[name=interest]').val());
        var deposit = parseInt($('[name=deposit]').val());
        var selling_without_deposit = selling_price - deposit ;
        var debit = selling_without_deposit + (selling_without_deposit * interest / 100) - discount
        $('[name=debit]').val(debit);
        if (debit <= 0){
            pos.addError('لا يمكن ان تضع الباقي بالسالب')
            pos.set_label_color($('[name=debit]') ,'btn-danger' )
        }else{
            pos.set_label_color($('[name=debit]') ,'btn-success' )
        }
    },
    Show_notification : function(Messages){

        msg = '' ;
        walker = function (key, value) {
            if (value !== null && typeof value === "object") {
                $.each(value, walker);
            }else{
                msg += '* ' + value + '<br />';
            }
        }
        $.each(Messages, walker);
        if (msg){
            $('#notification-box').show().find('div').html(msg);
        }else{
            $('#notification-box').hide()
        }
    },
    get_submit_status :function($errors){
        if ($errors.length ){
            $('[type=submit]').removeClass('buttonDisabled btn-danger btn-success').addClass('buttonDisabled btn-danger')
        }else{
            $('[type=submit]').removeClass('buttonDisabled btn-danger btn-success').addClass('btn-success')
        }
    }
};
/**
 * Event on actions
 * @type {{init: Function, before_submit: Function, on_debit_btn_click: Function}}
 */
var events = {
    init : function(){
        events.before_submit();
        events.on_debit_btn_click();
        events.on_validate_btn_click();
    },
    // on form submit
    before_submit : function(){
        $('#POS-Step-1').on('submit' ,function(){
            pos.validate_facade();
            pos.Show_notification(errors);
            pos.get_submit_status(errors);
            if (errors.length){
                return false;
            }
            return true;
        });
    },
    on_debit_btn_click : function(){
        $('#btn-debit').on('click' ,function(){
            pos.validate_facade();
            pos.get_debit();
            pos.Show_notification(errors);
            pos.get_submit_status(errors);
        });
    },
    on_validate_btn_click : function(){
        $('#btn-validate').on('click' ,function(){
            pos.validate_facade();
            pos.get_debit();
            pos.Show_notification(errors);
            pos.get_submit_status(errors);
        });
    }
};

events.init();
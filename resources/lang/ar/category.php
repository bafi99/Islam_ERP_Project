<?php

return [

	"categories"             => "جميع الفئات",
	"category"             => "الفئة",
	"create_category"             => "انشاء فئة جديدة",
	"category_details"             => "تفاصيل الفئة",
	"category_info"             => "جميع التفاصيل الخاصة بهذه الفئة",
	"category_name"             => "اسم الفئة",
	"add_new_category"             => "أضافة فئة جديدة",
	"index_page_hint"             => "جميع الفئات التي تستخدمها لادراج الاصناف اليها مثال (سوني , نوكيا , لابتوب وهكذا)",
	"category_name"             => "اسم الفئة",
	"category_desc"             => "ملاظات",
	"no_category"             => "لا يوجد فئات",
    'success' =>
        [
            'new_added' => 'تم اضافة فئة جديدة' ,
            'category_updated' => 'تم تعديل الفئة نجاح' ,
            'category_deleted' => 'تم مسح الفئة نجاح'
        ],
    'errors' =>
        [
            'database_error' => 'حدث خطا في قاعدة البيانات' ,
            'invaild_id' => 'من فضلك قم باختيار الفئة من الثائمة' ,
        ],
	"validation"              =>
        [
		    "catname" =>
                [
                    'required' => "اسم الفئة مطلوب " ,
                    'max' => "اسم الفئة لا يجوز ان يتجاوز 160 حرف",
                    'min' => "اسم الفئة لا يقل عن 2 حرف",
                    'unique' => "اسم الفئة يجب ان يكون غير مقرر" ,
                ] ,
            'catdesc' =>
                [
                    'max' => 'الملاحظات لا يجب ان تزيد عن 255 حرف' ,
                    'boolean' => 'خطا في اثناء اختيار الحالة' ,
                ]
	    ]
];

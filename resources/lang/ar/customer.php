<?php

return [

	"customers"             => "جميع العملاء",
	"customer"             => "العميل",
	"create_customer"             => "انشاء عميل جديد",
	"customer_details"             => "تفاصيل العميل",
	"customer_info"             => "جميع التفاصيل الخاصة بالعميل",
	"customer_name"             => "اسم العميل",
	"customer_mobile"             => "رقم هاتف العميل",
	"customer_mobile_2"             => "رقم هاتف العميل 2",
	"customer_address"             => "عنوان العميل",
	"add_new_customer"             => "اضافة عميل جديد",
	"index_page_hint"             => "جميع المعلومات الخاصة بالعملاء الذي يتم التعامل معهم",
	"customer_desc"             => "ملاحظات",
	"no_category"             => "لا يوجد عملاء",
    'success' =>
        [
            'new_added' => 'تم اضافة عميل جديد' ,
            'customer_updated' => 'تم تعديل العميل' ,
            'customer_deleted' => 'تم الغاء العميل بنجاح'
        ],
    'errors' =>
        [
            'database_error' => 'حدث خطا في قاعدة البيانات' ,
            'invaild_id' => 'من فضلك قم باختيار العميل من القائمة' ,
        ],
	"validation"              =>
        [
		    "customername" =>
                [
                    'required' => "يجب ادخال اسم العميل" ,
                    'max' => "اسم العميل لا يجوز ان يتجاوز 160 حرف",
                    'min' => "اسم العميل لا يقل عن 2 حرف",
                    'unique' => "اسم العميل يجب ان يكون غير مقرر" ,
                ] ,
            'customermobile' =>
                [
                    'digits_between' => 'رقم الهاتف يجب ان يكن ارقام من 9 الي 11 رقم ( ارضي او منزلي )' ,
                ] ,
            'customernote' =>
                [
                    'max' => 'ملاحظات العميل يجب عدد حروفها لا يتجاوز 1000 حرف' ,
                ] ,
            'customeraddress' =>
                [
                    'max' => 'ععنوان العميل لا يجب ان يتجاوز ال 255 حرف' ,
                ] ,
            'disabled' =>
                [
                    'boolean' => 'يجيب اختيار الحالة الخاصة بالعميل' ,
                ] ,
	    ]
];

<?php

return [

    "serial"             => "م",
    "status"             => "الحالة",
    "enable"             => "متاح",
    "disabled"             => "غير متاح",
    "read_more"             => "المزيد",
    "reset"             => "مسح الخانات",
    "submit"             => "حفظ",
    "update"             => "حفظ التعديلات",
    "edit"             => "تعديل",
    "details"             => "تفاصيل",
    "delete"             => "حذف",
    "created_at"             => "انشا في ",
    "updated_at"             => "موعد اخر تعديل",
    "confirm"             => "موافق",
    "cancel"             => "تراجع",
    "action"             => "Action",
    "add"             => "أضافة",
    "not_selected"             => "اختار ...",
    "next"             => "التالي",
    "validate"             => "التاكد",
    "remove"             => "حذف",
];
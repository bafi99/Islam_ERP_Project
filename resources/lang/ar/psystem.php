<?php

return [

	"psystems"             => "جميع الانظمة",
	"psystem"             => "النظام",
	"create_psystem"             => "انشاء نظام جديد",
	"psystem_details"             => "تفاصيل النظام",
	"psystem_info"             => "جميع التفاصيل الخاصة بالنظام",
	"psystem_name"             => "اسم النظام",
	"add_new_psystem"             => "اضافة نظام جديد",
	"index_page_hint"             => 'جميع المعلومات الخاصة بالانظمة',
	"no_psystem"             => 'لا يوجد انظمة',
	"psystem_desc"             => "ملاحظات",
    'success' =>
        [
            'new_added' => 'تم اضافة نظام جديد' ,
            'psystem_updated' => 'تم تعديل النظام' ,
            'psystem_deleted' => 'تم الغاء النظام بنجاح'
        ],
    'errors' =>
        [
            'database_error' => 'حدث خطا في قاعدة البيانات' ,
            'invaild_id' => 'من فضلك قم باختيار النظام من القائمة' ,
        ],
	"validation"              =>
        [
		    "psystemname" =>
                [
                    'required' => "يجب ادخال اسم النظام" ,
                    'max' => "اسم النظام لا يجوز ان يتجاوز 160 حرف",
                    'min' => "اسم النظام لا يقل عن 2 حرف",
                    'unique' => "اسم النظام يجب ان يكون غير مقرر" ,
                ] ,
            'paymentdesc' =>
                [
                    'max' => 'ملاحظات النظام لا يجب ان تتجاوز 1000 حرف' ,
                ] ,
            'disabled' =>
                [
                    'boolean' => 'يجيب اختيار الحالة الخاصة بالنظام' ,
                ] ,
	    ]
];

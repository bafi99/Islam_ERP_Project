<?php

return [

	"psystemsdates_sidebar"             => "مواعيد الدفع الخاصة بالانظمة",
	"psystemsdates_all_sidebar"             => "جميع مواعيد الدفع الخاصة بالانظمة",
	"psystemsdates_add_sidebar"             => "اضافة معاد دفع لنظام",
	"psystemsdates_date"             => "تاريخ الدفع مكرر سنويا",
	"paymentsystem"             => "النظام ",
	"type_date_or_prfoit"             => "تاريخ - ارباح",
	"psystemsdates"             => "جميع المواعيد الخاصة بالانظمة",
	"psystemdates"             => "النظام",
	"date"             => "تاريخ",
	"profit"             => "أرباح",
	"add_psystemdate"             => "اضافة معاد لنظام",
	"psystemdate_info"             => "جميع المعلومات الخاصة بالانظمة",
	"psystemdate_list_by_psystem"             => "عرض بالنظام المختار من القائمة فقط",
	"psystemdate_list_all"             => "عرض الكل",
	"psystemdate_details"             => "المزيد من التفاصيل",
	"psystemdate_name"             => "تاريخ الدفع",
	"psystemdates_details"             => "تفاصيل وملاحظات",

    'success' =>
        [
            'new_added' => 'شتم اضافة المعاد للنظام' ,
            'psystemdate_updated' => 'تم تعديل المعاد' ,
            'psystemdate_deleted' => 'تم الغاء المعاد'
        ],
    'errors' =>
        [
            'database_error' => 'حدث خطا في قاعدة البيانات' ,
            'invaild_id' => 'من فضلك قم باختيار المعاد الذي تريد تعديله' ,
        ],
	"validation"              =>
        [
		    "PDate_Date" =>
                [
                    'required' => 'من فضلك ادخل تاريخ الدفع' ,
                    'date' => 'القيمة التي داخلتها لا تتطابق مع التاريخ',
                    'date_format' => 'تاريخ يجب ان تقوم بادخاله بهذا الترتيب Y-m-d',
                ] ,
            'PaymentSystem_ID' =>
                [
                    'required' => 'من فضلك قم باختيار النظام' ,
                    'exists' => 'النظام الذي قمت باختيار غير متتطابق لدينا في قاعدة البيانات' ,
                ] ,
            'ISDate' =>
                [
                    'boolean' => 'من فضلك قم باختيار تاريخ ام ارباح من القائمة' ,
                ] ,
	    ]
];

<?php

return [

    'all_transactions' => 'جميع عمليات البيع',
    'transaction_info' => 'حميع المعلومات الخاصة بعملية البيع',
    'transaction_details' => 'بيان بالمعاملات الخاصة بعملية البيع',
    'total_paid' => 'اجمالي المدفوع' ,
    'total_expected' => 'الجمالع المتوقع دفعه' ,
    'margin_profit' => 'هامش الربح' ,
    'payment_dates' => 'مواعيد الدفع' ,
    'payment_date' => 'معاد الدفع' ,
    'expected_paid' => 'المتوفع دفعه' ,
    'actually_paid' => 'المدفوع فعلي' ,
    'paid' => 'تم الدفع' ,
    'notes' => 'ملاحظات' ,
    'if_pos_finished' => 'لا يمكنك تعديل او حذف اي معاملات لان عملية البيع تم اغلاقها' ,
    'goto_all_transactions' => 'الذهاب الي جميع المعاملات الخاصة بعملية البيع' ,
    'pos_details' => 'تفاصيل عملية البيع' ,
    'transaction_more_details' => 'تفاصيل المعاملة' ,
    'date_of_paid' => 'يوم تسديد القسط' ,
    'postpone_quest' => 'هل تريد تاجيل المبلغ المتبقي ؟ المبلغ المتوقع - المبلغ المدفوع' ,
    'postpone' => 'تاجيل باقي المبلغ' ,

    'msg_parts' => [
        'part_1' => 'لا يمكن ان يكون اكبر من المتبقي علي العميل' ,
        'part_2' => 'لا يساوي المتبقي علي العميل'
    ] ,

    'postpone_notification' => 'هيوجد مبلغ (:price) مؤجل من يوم (:date) ' ,


    'success' =>
        [
            'new_added' => 'تم اضافة معاملة جديدة',
            'trans_updated' => 'تم تعديل عملية البيع',
            'trans_deleted' => 'تم الغاء عملية المعاملة',
        ],
    'errors' =>
        [
            'database_error' => 'حدث خطا في قاعدة البيانات',
            'invaild_id' => 'من فضلك قم باختيار المعاملة من القائمة , لتجنب حدوث خطا',
        ],
    "validation" =>
        [
            "Customer_ID" =>
                [
                    'required' => 'من فضلك تاكد من اختيار اسم العميل',
                    'exists' => 'العميل الذي قمت باختيار ليس مسجل لدينا',
                ],
        ]
];

@extends('app')

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <a href="index.html"><b>Expenses</b> Project</a>
        </div><!-- /.login-logo -->
        <div class="login-box-body" dir="rtl">
            <p class="login-box-msg">ادخل بيانات الدخول</p>
            <p>
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>حطأ</strong>  في بيانات الدخول<br><br>
                    </div>
                @endif
            </p>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                <input type="hidden" name="_token" value="<?php echo csrf_token();?>">
                <div class="form-group has-feedback">
                    <input type="text" name="email" class="form-control" placeholder="Email"
                           value="<?php echo old('email') ;?>"  />
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">دخول</button>
                    </div><!-- /.col -->
                    <div class="col-xs-5"></div><!-- /.col -->
                    <div class="col-xs-3">
                        <div class="checkbox icheck">
                            <label>
                                <span>تذكرني</span><input type="checkbox" name="remember" >
                            </label>
                        </div>
                    </div><!-- /.col -->
                </div>
            </form>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

@endsection

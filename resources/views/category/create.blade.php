@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/category')}}"><h3>{{trans('category.categories')}}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">

                            @if(isset($category))
                                <h2><b>{{$category->CatName}}</b>
                                    <small>Created At <code>{{$category->created_at}}</code></small>
                                    <small>Last Update At <code>{{$category->updated_at}}</code></small>
                                </h2>
                            @else
                                <h2>{{trans('category.category')}}<small></small></h2>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')

                            @include('layout.partial.errors')

                            <br />
                            <form class="form-horizontal form-label-left" method="post" action="{{url(isset($category) ? '/category/'.Crypt::encrypt($category->CategoryID) : 'category'  )}}">

                                <input name="_token" value="{{csrf_token()}}" type="hidden"/>

                                @if(isset($category))
                                <input type="hidden" name="_method" value="PUT" />
                                @endif

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{trans('category.category_name')}} <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="CatName" class="form-control col-md-7 col-xs-12 " value="@if(isset($category)){{$category->CatName}}@else{{ old('CatName') ? old('CatName') : ''   }}@endif" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{trans('category.category_desc')}} <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="CatDesc" class="form-control col-md-7 col-xs-12" cols="20" rows="5">@if(isset($category)){{$category->CatDesc}}@else{{ old('CatDesc') ? old('CatDesc') : ''   }}@endif</textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">{{trans('global.status')}}</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="checkbox">
                                            <label>
                                                <input name="enable" type="checkbox" class="flat" @if(isset($category) && !$category->disabled) checked="checked" @endif > {{trans('global.enable')}}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        @if(isset($category))
                                            <a href="{{url('/category/create')}}" class="btn btn-default" >{{trans('category.create_category')}}</a>
                                            <button type="submit" class="btn btn-success">{{trans('global.update')}}</button>
                                        @else
                                            <a href="{{url('/category/create')}}" class="btn btn-primary" >{{trans('global.reset')}}</a>
                                            <button type="submit" class="btn btn-success">{{trans('global.submit')}}</button>
                                        @endif
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        @include('layout.partial.footer')

    </div>
    <!-- END page content -->

@endsection
@section('customscript')
    <!-- icheck -->
    <script src="{{asset('dashboard/js/icheck/icheck.min.js')}}"></script>
@endsection


@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{trans('category.categories')}}</h3>
                    <a href="{{url('/category/create')}}"><span><small>{{trans('category.add_new_category')}}</small></span></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>{{trans('category.categories')}} <small><code>{{trans('category.index_page_hint')}}</code></small></h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <?php echo $categories->render(); ?>
                                        <table id="tblResult" class="table table-striped responsive-utilities jambo_table bulk_action">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="column-title">{{trans('global.serial')}}</th>
                                                    <th class="column-title">{{trans('category.category')}}</th>
                                                    <th class="column-title">{{trans('category.category_desc')}}</th>
                                                    <th class="column-title">{{trans('global.status')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($categories))
                                                @foreach($categories as $k => $category)
                                                    <tr class="even pointer">
                                                        <th scope="row"><a class="btn-primary btn-sm  source" href="{{url('/category/'.$category->CategoryID)}}">{{$k + 1}}</a></th>
                                                        <td class="a-center text-info" ><b>{{strlen($category->CatName) > 27 ? mb_strimwidth($category->CatName ,0 ,27 ).'...' : $category->CatName }}</b></td>
                                                        <td class="a-right a-right text-info" >
                                                            @if(strlen($category->CatDesc) > 35)
                                                            {{mb_strimwidth($category->CatDesc ,0 ,35 ). '...'}} <a href="{{url('/category/'.$category->CategoryID)}}"><b>{{trans('global.read_more')}}</b></a>
                                                            @else
                                                            {{$category->CatDesc}}
                                                            @endif
                                                        </td>
                                                        <td class="a-right a-right text-warning" >
                                                            <label class="source btn-sm @if($category->disabled) btn-danger @else btn-success @endif">@if($category->disabled) {{trans('global.disabled')}} @else {{trans('global.enable')}} @endif</label>
                                                            <a class="btn btn-sm btn-primary" href="{{url('category/' . $category->CategoryID)}}">{{trans('global.details')}}</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4"><label>{{trans('category.no_category')}}</label></td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <?php echo $categories->render(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection


@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/customer')}}"><h3>{{ trans('customer.customers') }}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">

                            @if(isset($customer))
                                <h2><b>{{$customer->CustomerName}}</b>
                                    <small>{{ trans('global.created_at') }} <code>{{$customer->created_at}}</code></small>
                                    <small>{{ trans('global.updated_at') }} <code>{{$customer->updated_at}}</code></small>
                                </h2>
                            @else
                                <h2>{{ trans('customer.create_customer') }}<small></small></h2>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')

                            @include('layout.partial.errors')

                            <br />
                            <form class="form-horizontal form-label-left" method="post" action="{{url(isset($customer) ? '/customer/'.Crypt::encrypt($customer->CustomerID) : 'customer'  )}}">

                                <input name="_token" value="{{csrf_token()}}" type="hidden"/>

                                @if(isset($customer))
                                <input type="hidden" name="_method" value="PUT" />
                                @endif


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('customer.customer_name') }} <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="cust_name" class="form-control col-md-7 col-xs-12 " value="@if(isset($customer)){{$customer->CustName}}@else{{ old('cust_name') ? old('cust_name') : ''   }}@endif" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('customer.customer_mobile') }} :
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="cust_mobile_1" class="form-control col-md-7 col-xs-12 " value="@if(isset($customer)){{$customer->CustMobile1}}@else{{ old('cust_mobile_1') ? old('cust_mobile_1') : ''   }}@endif" placeholder="Land Number like (03xxxxxxx) Or Mobile Like (01xxxxxxxxx)" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('customer.customer_mobile_2') }} :
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="cust_mobile_2" class="form-control col-md-7 col-xs-12 " value="@if(isset($customer)){{$customer->CustMobile2}}@else{{ old('cust_mobile_2') ? old('cust_mobile_2') : ''   }}@endif" placeholder="Land Number like (03xxxxxxx) Or Mobile Like (01xxxxxxxxx)" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('customer.customer_address') }} :
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="cust_address" class="form-control col-md-7 col-xs-12 " value="@if(isset($customer)){{$customer->CustAddress}}@else{{ old('cust_address') ? old('cust_address') : ''   }}@endif" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('customer.customer_desc') }} :
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="cust_note" class="form-control col-md-7 col-xs-12" cols="20" rows="5">@if(isset($customer)){{$customer->CustNote}}@else{{ old('cust_note') ? old('cust_note') : ''   }}@endif</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('global.status') }}</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="checkbox">
                                            <label>
                                                <input name="enable" type="checkbox" class="flat" @if(isset($customer) && !$customer->disabled) checked="checked" @elseif(old('enable') == 'on') checked="checked" @endif > {{ trans('global.enable') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        @if(isset($customer))
                                            <a href="{{url('/customer/create')}}" class="btn btn-default" >{{ trans('customer.create_customer') }}</a>
                                            <button type="submit" class="btn btn-success">{{ trans('global.update') }}</button>
                                        @else
                                            <a href="{{url('/customer/create')}}" class="btn btn-primary" >{{ trans('global.reset') }}</a>
                                            <button type="submit" class="btn btn-success">{{ trans('global.submit') }}</button>
                                        @endif
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        @include('layout.partial.footer')

    </div>
    <!-- END page content -->

@endsection
@section('customscript')
    <!-- icheck -->
    <script src="{{asset('dashboard/js/icheck/icheck.min.js')}}"></script>
    <!-- select2 -->
    <script src="{{asset('dashboard/js/select/select2.full.js')}}"></script>
    <script type="text/javascript">
        (function(){
            $(".select2_single").select2();
        })()
    </script>
@endsection


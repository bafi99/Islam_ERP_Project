@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/customer')}}"><h3>{{ trans('customer.customers') }}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('customer.customer_details') }} <strong>{{$customer->CustName}}</strong><small><code>{{ trans('customer.customer_info') }}</code></small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <br />
                            <div id="notification-form" class="alert alert-info row invisiblediv" >
                                <div class="col-md-9">
                                    <strong>اضغط موافق اذا كنت تريد حذف العميل</strong> ولكن لا يمكنك حذف العميل اذا كان لديه اي معاملات خاصة بالعميل
                                </div>
                                <div class="col-md-3">
                                    <form method="post" action="{{url('/customer/'.Crypt::encrypt($customer->CustomerID))}}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}" />

                                        <input type="hidden" name="_method" value="DELETE" />

                                        <input type="button" id="cancel-notification" class="btn btn-round btn-default text-primary " value="{{ trans('global.cancel') }}"/>

                                        <input type="submit" class="btn btn-round btn-warning" value="{{ trans('global.confirm') }}"/>
                                    </form>
                                </div>
                            </div>
                            <div class="form-horizontal form-label-left" novalidate="" >

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('customer.customer_name') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$customer->CustName}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('customer.customer_mobile') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$customer->CustMobile1 ? $customer->CustMobile1 : "N/A"}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('customer.customer_mobile_2') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$customer->CustMobile2 ? $customer->CustMobile2 : "N/A"}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('customer.customer_address') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$customer->CustAddress ? $customer->CustAddress : "N/A"}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('customer.customer_desc') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$customer->CustNote ? $customer->CustNote : "N/A"}}</label>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <a href="{{url('/customer/'.$customer->CustomerID.'/edit')}}" class="btn btn-primary source" >{{ trans('global.edit') }}</a>

                                        <a id="deletecustomer" href="javascript:void(0)" class="btn btn-danger source" >{{ trans('global.delete') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection
@section('customscript')
    <script type="text/javascript">
        (function(){

            $('#deletecustomer').on('click' ,function(){
                $('#notification-form').slideDown()
            });
            $('#cancel-notification').on('click' ,function(){
                $('#notification-form').slideUp()
            })

        })();
    </script>
@endsection


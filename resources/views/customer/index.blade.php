@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('customer.customers') }}</h3>
                    <a href="{{url('/customer/create')}}"><small>{{ trans('customer.create_customer') }}</small></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>{{ trans('customer.customers') }} <small><code>{{ trans('customer.index_page_hint') }}</code></small></h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <?php echo $Customers->render(); ?>
                                        <table id="tblResult" class="table table-striped responsive-utilities jambo_table bulk_action">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="column-title">{{ trans('global.serial') }}</th>
                                                    <th class="column-title">{{ trans('customer.customer_name') }}</th>
                                                    <th class="column-title">{{ trans('customer.customer_mobile') }}</th>
                                                    <th class="column-title">{{ trans('customer.customer_mobile_2') }}</th>
                                                    <th class="column-title">{{ trans('customer.customer_address') }}</th>
                                                    <th class="column-title">{{ trans('customer.customer_desc') }}</th>
                                                    <th class="column-title">{{ trans('global.status') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($Customers))
                                                @foreach($Customers as $k => $customer)
                                                    <tr class="even pointer">
                                                        <th scope="row"><a class="btn-primary btn-sm  source" href="{{url('/customer/'.$customer->CustomerID)}}">{{$k + 1}}</a></th>
                                                        <td class="a-center text-info" ><b>{{$customer->CustName}}</b></td>
                                                        <td class="a-center text-info" ><b>{{$customer->CustMobile1 ? $customer->CustMobile1 : "N/A"}}</b></td>
                                                        <td class="a-center text-info" ><b>{{$customer->CustMobile2 ? $customer->CustMobile2 : "N/A"}}</b></td>
                                                        <td class="a-right a-right text-info" >
                                                            @if(strlen($customer->CustAddress) > 15)
                                                            {{mb_strimwidth($customer->CustAddress ,0 ,15 ). '...'}} <a href="{{url('/customer/'.$customer->CustomerID)}}"><b>{{ trans('global.more') }}</b></a>
                                                            @else
                                                            {{$customer->CustAddress ? $customer->CustAddress : "N/A"}}
                                                            @endif
                                                        </td>
                                                        <td class="a-right a-right text-info" >
                                                            @if(strlen($customer->CustNote) > 15)
                                                            {{mb_strimwidth($customer->CustNote ,0 ,15 ). '...'}} <a href="{{url('/customer/'.$customer->CustomerID)}}"><b>{{ trans('global.more') }}</b></a>
                                                            @else
                                                            {{$customer->CustNote ? $customer->CustNote : "N/A"}}
                                                            @endif
                                                        </td>
                                                        <td class="a-right a-right text-warning" >
                                                            <label class="source btn-sm @if($customer->disabled) btn-danger @else btn-success @endif">@if($customer->disabled) {{ trans('global.disabled') }} @else {{ trans('global.enable') }} @endif</label>
                                                            <a href="{{url('customer/' . $customer->CustomerID)}}" class="btn btn-sm btn-primary" >{{ trans('global.edit') }}</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4"><label>{{'No Customers'}}</label></td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <?php echo $Customers->render(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection



    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="{{asset('dashboard/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('dashboard/js/nicescroll/jquery.nicescroll.min.js')}}"></script>

    <script src="{{asset('dashboard/js/custom.js')}}"></script>

    @yield('customscript')

    </body>

</html>
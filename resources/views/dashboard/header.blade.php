<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{csrf_token()}}">

    <title>برنامج ادارة المبيعات</title>

    <!-- Bootstrap core CSS -->

    <link href="{{asset('dashboard/css/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{asset('dashboard/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{{asset('dashboard/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/icheck/flat/green.css')}}" rel="stylesheet">
    <!-- editor -->
    {{--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">--}}
    <link href="{{asset('dashboard/css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/editor/index.css')}}" rel="stylesheet">
    <!-- select2 -->
    <link href="{{asset('dashboard/css/select/select2.min.css')}}" rel="stylesheet">
    <!-- switchery -->
    <link rel="stylesheet" href="{{asset('dashboard/css/switchery/switchery.min.css')}}" />

    <script src="{{asset('dashboard/js/jquery.min.js')}}"></script>
    <style>
        .invisiblediv{
            display: none;
        }
    </style>
</head>

<body class="nav-md">

<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{url('/')}}" class="site_title"><i class="fa fa-paw"></i> <span>برنامج ادارة المبيعات</span></a>
                </div>
                <div class="clearfix"></div>


                <!-- menu prile quick info -->
                @include('dashboard.menuquickinfo')
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                @include('dashboard.sidebar')
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        @include('dashboard.navigation')
        <!-- /top navigation -->

        @yield('content')
    </div>
</div>
<!-- Start Footer -->
@include('dashboard.footer')
<!-- End Footer -->
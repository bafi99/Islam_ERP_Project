<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-arrow-circle-down"></i> {{trans('category.category')}} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/category')}}">{{trans('category.categories')}}</a></li>

                    <li><a href="{{url('/category/create')}}">{{trans('category.create_category')}}</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> {{ trans('item.item') }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/item')}}">{{ trans('item.items') }}</a></li>

                    <li><a href="{{url('/item/create')}}">{{ trans('item.create_item') }}</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> {{ trans('customer.customer') }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/customer')}}">{{ trans('customer.customers') }} </a></li>

                    <li><a href="{{url('/customer/create')}}">{{ trans('customer.create_customer') }}</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> {{ trans('psystem.psystem') }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/paymentsystem')}}">{{ trans('psystem.psystems') }}</a></li>

                    <li><a href="{{url('/paymentsystem/create')}}">{{ trans('psystem.create_psystem') }}</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i>{{ trans('psystemdates.psystemsdates_sidebar') }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/paymentdates')}}">{{ trans('psystemdates.psystemsdates_all_sidebar') }}</a></li>

                    <li><a href="{{url('/paymentdates/create')}}">{{ trans('psystemdates.psystemsdates_add_sidebar') }}</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> {{ trans('pos.sidebar.pos') }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/pos')}}">{{ trans('pos.all_pos') }}</a></li>

                    <li><a href="{{url('/pos/create')}}">{{ trans('pos.sidebar.new_pos') }}</a></li>
                </ul>
            </li>
            {{--<li><a><i class="fa fa-arrow-circle-down"></i> Transactions <span class="fa fa-chevron-down"></span></a>--}}
                {{--<ul class="nav child_menu" style="display: none">--}}
                    {{--<li><a href="{{url('/transactions')}}">All Transaction</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li><a><i class="fa fa-arrow-circle-down"></i> {{ trans('report.reports') }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/reports')}}">{{ trans('report.reports') }}</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>
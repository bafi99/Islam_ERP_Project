@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <h3>Home Page</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_content">

                        @include('layout.partial.success')

                        @include('layout.partial.errors')

                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection

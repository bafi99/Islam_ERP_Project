@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/item')}}"><h3>{{ trans('item.items') }}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">

                            @if(isset($item))
                                <h2><b>{{$item->ItemName}}</b>
                                    <small>{{ trans('item.created_at') }} <code>{{$item->created_at}}</code></small>
                                    <small>{{ trans('item.updated_at') }} <code>{{$item->updated_at}}</code></small>
                                </h2>
                            @else
                                <h2>{{ trans('item.item') }}<small></small></h2>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')

                            @include('layout.partial.errors')

                            <br />
                            <form class="form-horizontal form-label-left" method="post" action="{{url(isset($item) ? '/item/'.Crypt::encrypt($item->ItemID) : 'item'  )}}">

                                <input name="_token" value="{{csrf_token()}}" type="hidden"/>

                                @if(isset($item))
                                <input type="hidden" name="_method" value="PUT" />
                                @endif

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('category.category_name') }} <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="select2_single form-control" name="CategoryID" >
                                            <option value="0">Not Selected</option>
                                            @foreach($Categories as $category)
                                                <option value="{{$category->CategoryID}}" @if(isset($item) && $item->cat_id == $category->CategoryID ) selected="selected" @elseif(old('CategoryID') == $category->CategoryID) selected="selected" @endif >{{$category->CatName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('item.item_name') }} <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="Item_name" class="form-control col-md-7 col-xs-12 " value="@if(isset($item)){{$item->ItemName}}@else{{ old('Item_name') ? old('Item_name') : ''   }}@endif" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('item.item_desc') }} <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="Item_desc" class="form-control col-md-7 col-xs-12" cols="20" rows="5">@if(isset($item)){{$item->ItemDesc}}@else{{ old('Item_desc') ? old('Item_desc') : ''   }}@endif</textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('global.status') }}</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="checkbox">
                                            <label>
                                                <input name="enable" type="checkbox" class="flat" @if(isset($item) && !$item->disabled) checked="checked" @elseif(old('enable') == 'on') checked="checked" @endif > {{ trans('global.enable') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        @if(isset($item))
                                            <a href="{{url('/item/create')}}" class="btn btn-default" >{{ trans('item.create_item') }}</a>
                                            <button type="submit" class="btn btn-success">{{ trans('global.update') }}</button>
                                        @else
                                            <a href="{{url('/item/create')}}" class="btn btn-primary" >{{ trans('global.reset') }}</a>
                                            <button type="submit" class="btn btn-success">{{ trans('global.submit') }}</button>
                                        @endif
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        @include('layout.partial.footer')

    </div>
    <!-- END page content -->

@endsection
@section('customscript')
    <!-- icheck -->
    <script src="{{asset('dashboard/js/icheck/icheck.min.js')}}"></script>
    <!-- select2 -->
    <script src="{{asset('dashboard/js/select/select2.full.js')}}"></script>
    <script type="text/javascript">
        (function(){
            $(".select2_single").select2();
        })()
    </script>
@endsection


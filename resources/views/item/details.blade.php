@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/item')}}"><h3>{{ trans('item.items') }}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('item.item_info') }} <strong>{{$item->ItemName}}</strong><small><code>{{ trans('item.item_info') }}</code></small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <br />
                            <div id="notification-form" class="alert alert-info row invisiblediv" >
                                <div class="col-md-9">
                                    <strong>اضغط نعم اذا كنت تريد الغاء هذا الصنف</strong> ولاكن يجب العلم انه لا يمكنك الغاء هذا الصنف اذا كان مندرج تحت اي عملية بيع
                                </div>
                                <div class="col-md-3">
                                    <form method="post" action="{{url('/item/'.Crypt::encrypt($item->ItemID))}}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}" />

                                        <input type="hidden" name="_method" value="DELETE" />

                                        <input type="button" id="cancel-notification" class="btn btn-round btn-default text-primary " value="{{ trans('global.cancel') }}"/>

                                        <input type="submit" class="btn btn-round btn-warning" value="{{ trans('global.confirm') }}"/>
                                    </form>
                                </div>
                            </div>
                            <div class="form-horizontal form-label-left" novalidate="" >

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('item.item_category') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" ><a href="{{url('category/' . $item->Category()->first()->CategoryID)}}">{{$item->Category()->first()->CatName}}</a></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('item.item_name') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$item->ItemName}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('item.item_desc') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$item->ItemDesc}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('global.status') }} <span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="btn col-md-12 col-xs-12">
                                            <label class="source btn-sm @if($item->disabled) btn-danger @else btn-success @endif">@if($item->disabled) {{ trans('global.disabled') }} @else {{ trans('global.enable') }} @endif</label>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <a href="{{url('/item/'.$item->ItemID.'/edit')}}" class="btn btn-primary source" >{{ trans('global.edit') }}</a>

                                        <a id="deleteitem" href="javascript:void(0)" class="btn btn-danger source" >{{ trans('global.delete') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection
@section('customscript')
    <script type="text/javascript">
        (function(){

            $('#deleteitem').on('click' ,function(){
                $('#notification-form').slideDown()
            });
            $('#cancel-notification').on('click' ,function(){
                $('#notification-form').slideUp()
            })

        })();
    </script>
@endsection


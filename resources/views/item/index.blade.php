@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('item.all_items') }}</h3>
                    <a href="{{url('/item/create')}}"><small>{{ trans('item.create_item') }}</small></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>{{ trans('item.items') }} <small><code>{{ trans('item.items_info') }}</code></small></h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <?php echo $items->render(); ?>
                                        <table id="tblResult" class="table table-striped responsive-utilities jambo_table bulk_action">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="column-title">{{ trans('global.serial') }}</th>
                                                    <th class="column-title">{{ trans('item.item') }}</th>
                                                    <th class="column-title">{{ trans('category.category') }}</th>
                                                    <th class="column-title">{{ trans('item.item_desc') }}</th>
                                                    <th class="column-title">{{ trans('global.status') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($items))
                                                @foreach($items as $k => $item)
                                                    <tr class="even pointer">
                                                        <th scope="row"><a class="btn-primary btn-sm  source" href="{{url('/item/'.$item->ItemID)}}">{{$k + 1}}</a></th>
                                                        <td class="a-center text-info" ><b>{{$item->ItemName}}</b></td>
                                                        <td class="a-center text-info" ><b>{{$item->Category()->first()->CatName}}</b></td>
                                                        <td class="a-right a-right text-info" >
                                                            @if(strlen($item->ItemDesc) > 20)
                                                            {{mb_strimwidth($item->ItemDesc ,0 ,20 ). '...'}} <a href="{{url('/item/'.$item->ItemID)}}"><b>{{ trans('global.more') }}</b></a>
                                                            @else
                                                            {{$item->ItemDesc}}
                                                            @endif
                                                        </td>
                                                        <td class="a-right a-right text-warning" >
                                                            <label class="source btn-sm @if($item->disabled) btn-danger @else btn-success @endif">@if($item->disabled) {{ trans('global.disabled') }} @else {{trans('global.enable')}} @endif</label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4"><label>{{'No Items'}}</label></td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <?php echo $items->render(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection


@if(isset($errors) && count($errors))
    <!-- start:Errors -->
    <div class="alert alert-danger" id="alertbox">
        <button type="button" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        @foreach($errors as $error)

            @if(is_array($error))

                @foreach($error as $e)

                    <p>* {{$e}}</p>

                @endforeach
            @else

                <p>* {{$error}}</p>

            @endif
        @endforeach
    </div>
    <!-- End:Errors -->
@endif
<!-- footer content -->
<footer>
    <div class="">
        <p class="pull-right">Islam Ismail Project! Developed by <a href="www.mgouda.net">MGouda</a>. |
            <span class="lead"> <i class="fa fa-paw"></i> Expenses Management!</span>
        </p>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
<?php $success = Session::get('success'); ?>
@if(isset($success) && count($success))

    <!-- start:Success -->
    <div class="alert alert-success" id="alertbox">
        <button type="button" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        @foreach($success as $s)
            <p>* {{$s}}</p>
        @endforeach
    </div>
    <!-- End:Success -->

@endif


@extends('dashboard.header')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">

                            @if(isset($paymentsystem))
                                <h2><b>{{$paymentsystem->PaymentAlias}}</b>
                                    <small>{{ trans('global.created_at') }} <code>{{$paymentsystem->created_at}}</code></small>
                                    <small>{{ trans('global.updated_at') }} <code>{{$paymentsystem->updated_at}}</code></small>
                                </h2>
                            @else
                                <h2><a href="{{url('/paymentdates')}}">{{ trans('psystemdates.psystemsdates_add_sidebar') }}</a>
                                    <small><code>ققم باختيارك معاد دفع سنوي , لا تقلق من اختيارك السنة اثناء اضافة المواعيد</code></small>
                                </h2>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')

                            @include('layout.partial.errors')

                                <div class="form-horizontal form-label-left form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        @if(isset($PaymentSystem))
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('psystem.psystem') }}</label>
                                            <a href="{{url('/paymentsystem/' . $PaymentSystem->PaymentID)}}"><h5 class="text-danger">{{$PaymentSystem->PaymentAlias}}</h5></a>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="col-md-6 col-sm-6 col-xs-9">
                                            <select name="payment_system_id" class="form-control">
                                                @foreach($PaymentSystems as $paymentsystem)
                                                    <option value="{{Crypt::Encrypt($paymentsystem->PaymentID)}}">{{$paymentsystem->PaymentAlias}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-3">
                                            <input type="button" id="btn_paymentsystem" class="btn btn-success" value="{{ trans('psystemdates.psystemdate_list_by_psystem') }}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="x_content">
                                    <table class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th class="column-title">{{isset($PaymentDate) ? trans('psystem.psystem') : trans('global.serial')}}</th>
                                                <th class="column-title">{{ trans('psystemdates.type_date_or_prfoit') }} </th>
                                                <th class="column-title">{{ trans('global.status') }} </th>
                                                <th class="column-title">{{ trans('global.action') }} </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                    @if(isset($PaymentSystem))
                                        @foreach ($PaymentSystemDates as $k => $paymentsystemdate)
                                            <tr class="">
                                                <th scope="row"><a class="btn-primary btn-sm  source" href="{{url('/paymentdates/' . $paymentsystemdate->PDateID )}}">{{$k + 1}}</a></th>
                                                <td class="a-center text-info"><b>YYYY-{{\Carbon\Carbon::parse($paymentsystemdate->PDate_Date)->format('m-d')}}</b></td>
                                                <td class="a-center text-info last"><b>{{$paymentsystemdate->ISDate ? trans('psystemdates.date') : trans('psystemdates.profit')}}</b></td>
                                                <td>
                                                    <a href="{{url('/paymentdates/' . $paymentsystemdate->PDateID )}}" class="btn btn-default btn-sm" >{{ trans('global.details') }}</a>
                                                    <a href="{{url('/paymentdates/' . $paymentsystemdate->PDateID . '/edit' )}}" class="btn btn-primary btn-sm" >{{ trans('global.edit') }}</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    @if(isset($PaymentDate) || isset($PaymentSystem))
                                        <tr class="">
                                            <form method="post" action="{{url(isset($PaymentDate) ? '/paymentdates/'.Crypt::encrypt($PaymentDate->PDateID) : 'paymentdates'  )}}" >

                                                <input name="_token" value="{{csrf_token()}}" type="hidden"/>

                                                @if(isset($PaymentDate))
                                                    <input type="hidden" name="_method" value="PUT" />
                                                @endif

                                                <input name="payment_system" value="{{Input::get('payment_system')}}" type="hidden"/>
                                                <td class=" ">
                                                    @if(isset($PaymentDate))
                                                        <select name="payment_system" class="form-control">
                                                            @foreach($PaymentSystems as $paymentsystem)
                                                                <option value="{{Crypt::Encrypt($paymentsystem->PaymentID)}}" @if(isset($PaymentDate) && $PaymentDate->PaymentSystem_ID == $paymentsystem->PaymentID) selected="selected" @endif  >{{$paymentsystem->PaymentAlias}}</option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </td>
                                                <td class=" ">
                                                    <input type="text"  class="form-control date-picker" name="payment_date" value="@if(isset($PaymentDate)){{$PaymentDate->PDate_Date}}@else{{ old('payment_date') ? old('payment_date') : \Carbon\Carbon::today()->format('Y-m-d') }} @endif" />
                                                </td>
                                                <td class="last">
                                                    <select class="form-control" name="payment_is_date">
                                                        <option value="0" @if(isset($PaymentDate) && $PaymentDate->ISDate == '0') selected="selected" @elseif(old('payment_is_date') == '0') selected="selected" @endif >{{ trans('psystemdates.profit') }}</option>
                                                        <option value="1" @if(isset($PaymentDate) && $PaymentDate->ISDate == '1') selected="selected" @elseif(old('payment_is_date') == '1') selected="selected" @elseif(old('payment_is_date') != '0') selected="selected" @endif >{{ trans('psystemdates.date') }}</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    @if(isset($PaymentDate))
                                                        <input type="submit"  class="btn btn-success btn-sm" value="{{ trans('global.update') }}" />
                                                        <a href="{{url('/paymentdates/create')}}" class="btn btn-dark btn-sm" >{{ trans('psystemdates.psystemsdates_add_sidebar') }}</a>
                                                    @else
                                                        <input type="submit"  class="btn btn-success btn-sm" value="{{ trans('global.add') }}" />
                                                    @endif
                                                </td>
                                            </form>
                                        </tr>
                                    @endif
                                        </tbody>
                                    </table>
                                </div>
                            <div class="ln_solid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        @include('layout.partial.footer')

    </div>
    <!-- END page content -->

@endsection
@section('customscript')

    <!-- daterangepicker -->
    <script type="text/javascript" src="{{asset('dashboard/js/moment.min2.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/datepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript">
        /**
         * Select System Payment
         * */
        $('#btn_paymentsystem').on('click' ,function(){
           window.location.href = '/paymentdates/create?payment_system=' + $('[name=payment_system_id] :selected').val()
        });
        $(document).ready(function () {
            /**
             * Create Date Picker Selector
             */
            $('.date-picker').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                format: "YYYY-MM-DD"
            });

        });
    </script>

@endsection
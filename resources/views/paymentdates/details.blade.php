@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/paymentdates')}}"><h3>{{ trans('psystemdates.psystemsdates_all_sidebar') }}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('psystemdates.psystemdate_details') }} <strong>{{$PaymentDate->PDate_Date}}</strong><small></small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <br />
                            <div id="notification-form" class="alert alert-info row invisiblediv" >
                                <div class="col-md-9">
                                    <strong>اضغط موافق اذا كنت تريد حذف هذا الميعاد !</strong> ولكن لا يمكنك الغاء هذا التاريخ اذا كان مسجل في اي معاملات
                                </div>
                                <div class="col-md-3">
                                    <form method="post" action="{{url('/paymentdates/'.Crypt::encrypt($PaymentDate->PDateID))}}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}" />

                                        <input type="hidden" name="_method" value="DELETE" />

                                        <input type="button" id="cancel-notification" class="btn btn-round btn-default text-primary " value="{{ trans('global.cancel') }}"/>

                                        <input type="submit" class="btn btn-round btn-warning" value="{{ trans('global.confirm') }}"/>
                                    </form>
                                </div>
                            </div>
                            <div class="form-horizontal form-label-left" novalidate="" >

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('psystem.psystem') }}<span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$PaymentDate->PaymentSystem()->first()->PaymentAlias}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('psystemdates.psystemdate_name') }}<span class="required">:</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$PaymentDate->PDate_Date}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">{{ trans('psystemdates.type_date_or_prfoit') }} :
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class=" btn source col-md-12 col-xs-12" >{{$PaymentDate->ISDate ? trans('psystemdates.date') : trans('psystemdates.profit')}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <a href="{{url('/paymentdates/'.$PaymentDate->PDateID.'/edit')}}" class="btn btn-primary source" >{{ trans('global.edit') }}</a>

                                        <a id="deletepaymentdate" href="javascript:void(0)" class="btn btn-danger source" >{{ trans('global.delete') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection
@section('customscript')
    <script type="text/javascript">
        (function(){

            $('#deletepaymentdate').on('click' ,function(){
                $('#notification-form').slideDown()
            });
            $('#cancel-notification').on('click' ,function(){
                $('#notification-form').slideUp()
            })

        })();
    </script>
@endsection


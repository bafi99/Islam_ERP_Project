@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('psystemdates.psystemsdates') }}</h3>
                    <a href="{{url('/paymentdates/create')}}"><small>{{ trans('psystemdates.add_psystemdate') }}</small></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2> <small><code>{{ trans('psystemdates.psystemdate_info') }}</code></small></h2>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="form-horizontal form-label-left form-group">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            @if(isset($PaymentSystem))
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('psystem.psystem') }}</label>
                                                <a href="{{url('/paymentsystem/' . $PaymentSystem->PaymentID)}}"><h5 class="text-danger">{{$PaymentSystem->PaymentAlias}}</h5></a>
                                            @endif
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-9">
                                                <select name="payment_system_id" class="form-control">
                                                    @foreach($PaymentSystems as $paymentsystem)
                                                        <option value="{{Crypt::Encrypt($paymentsystem->PaymentID)}}">{{$paymentsystem->PaymentAlias}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-3">
                                                <input type="button" id="btn_paymentsystem" class="btn btn-success" value="{{ trans('psystemdates.psystemdate_list_by_psystem') }}" />
                                                <input type="button" id="btn_paymentsystem_all" class="btn btn-primary" value="{{ trans('psystemdates.psystemdate_list_all') }}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="x_content">
                                        <table id="tblResult" class="table table-striped responsive-utilities jambo_table bulk_action">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="column-title">{{ trans('global.serial') }}</th>
                                                    <th class="column-title">{{ trans('psystemdates.psystemsdates_date') }}</th>
                                                    <th class="column-title">{{ trans('psystemdates.paymentsystem') }}</th>
                                                    <th class="column-title">{{ trans('psystemdates.type_date_or_prfoit') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($PaymentDates))
                                                @foreach($PaymentDates as $k => $paymentdate)
                                                    <tr class="even pointer">
                                                        <th scope="row"><a class="btn-primary btn-sm  source" href="{{url('/paymentdates/'.$paymentdate->PDateID)}}">{{$k + 1}}</a></th>
                                                        <td class="a-center text-info" ><b>YYYY-{{\Carbon\Carbon::parse($paymentdate->PDate_Date)->format('m-d')}}</b></td>
                                                        <td class="a-center text-info" ><b>{{$paymentdate->PaymentSystem()->first()->PaymentAlias}}</b></td>
                                                        <td class="a-center text-info" ><b>{{$paymentdate->ISDate ? trans('psystemdates.date') : trans('psystemdates.profit')}}</b></td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4"><label>{{'No Payment Dates'}}</label></td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        {!! $PaymentDates->appends(Input::except('page'))->render() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection

@section('customscript')
    <script type="text/javascript">
        /**
         * Select System Payment
         * */
        $('#btn_paymentsystem').on('click' ,function(){
            window.location.href = '/paymentdates?payment_system=' + $('[name=payment_system_id] :selected').val()
        });
        $('#btn_paymentsystem_all').on('click' ,function(){
            window.location.href = '/paymentdates'
        });
    </script>
@endsection
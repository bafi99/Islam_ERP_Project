@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/paymentsystem')}}"><h3>{{ trans('psystem.psystems') }}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">

                            @if(isset($paymentsystem))
                                <h2><b>{{$paymentsystem->PaymentAlias}}</b>
                                    <small>{{ trans('global.created_at') }} <code>{{$paymentsystem->created_at}}</code></small>
                                    <small>{{ trans('global.updated_at') }} <code>{{$paymentsystem->updated_at}}</code></small>
                                </h2>
                            @else
                                <h2>Payment System<small></small></h2>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')

                            @include('layout.partial.errors')

                            <br />
                            <form class="form-horizontal form-label-left" method="post" action="{{url(isset($paymentsystem) ? '/paymentsystem/'.Crypt::encrypt($paymentsystem->PaymentID) : 'paymentsystem'  )}}">

                                <input name="_token" value="{{csrf_token()}}" type="hidden"/>

                                @if(isset($paymentsystem))
                                <input type="hidden" name="_method" value="PUT" />
                                @endif


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('psystem.psystem_name') }} <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="payment_alias" class="form-control col-md-7 col-xs-12 " value="@if(isset($paymentsystem)){{$paymentsystem->PaymentAlias}}@else{{ old('payment_alias') ? old('payment_alias') : ''   }}@endif" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ trans('psystem.psystem_desc') }} :
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="payment_desc" class="form-control col-md-7 col-xs-12" cols="20" rows="5">@if(isset($paymentsystem)){{$paymentsystem->PaymentDescription}}@else{{ old('payment_desc') ? old('payment_desc') : ''   }}@endif</textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('global.enable') }}</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="checkbox">
                                            <label>
                                                <input name="enable" type="checkbox" class="flat" @if(isset($paymentsystem) && !$paymentsystem->disabled) checked="checked" @elseif(old('enable') == 'on') checked="checked" @endif > {{ trans('global.enable') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        @if(isset($paymentsystem))
                                            <a href="{{url('/paymentsystem/create')}}" class="btn btn-default" >{{ trans('psystem.create_psystem') }}</a>
                                            <button type="submit" class="btn btn-success">{{ trans('global.update') }}</button>
                                        @else
                                            <a href="{{url('/paymentsystem/create')}}" class="btn btn-primary" >{{ trans('global.reset') }}</a>
                                            <button type="submit" class="btn btn-success">{{ trans('global.submit') }}</button>
                                        @endif
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        @include('layout.partial.footer')

    </div>
    <!-- END page content -->

@endsection
@section('customscript')

@endsection


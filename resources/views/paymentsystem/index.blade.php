@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('psystem.psystems') }}</h3>
                    <a href="{{url('/paymentsystem/create')}}"><small>{{ trans('psystem.create_psystem') }}</small></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>{{ trans('psystem.psystems') }} <small><code>{{ trans('psystem.index_page_hint') }} </code></small></h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <?php echo $PaymentSystems->render(); ?>
                                        <table id="tblResult" class="table table-striped responsive-utilities jambo_table bulk_action">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="column-title">{{ trans('global.serial') }}</th>
                                                    <th class="column-title">{{ trans('psystem.psystem_name') }}</th>
                                                    <th class="column-title">{{ trans('psystem.psystem_desc') }}</th>
                                                    <th class="column-title">{{ trans('global.status') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($PaymentSystems))
                                                @foreach($PaymentSystems as $k => $paymentsystem)
                                                    <tr class="even pointer">
                                                        <th scope="row"><a class="btn-primary btn-sm  source" href="{{url('/paymentsystem/'.$paymentsystem->PaymentID)}}">{{$k + 1}}</a></th>
                                                        <td class="a-center text-info" ><b>{{$paymentsystem->PaymentAlias}}</b></td>
                                                        <td class="a-right a-right text-info" >
                                                            @if(strlen($paymentsystem->PaymentDescription) > 30)
                                                            {{mb_strimwidth($paymentsystem->PaymentDescription ,0 ,30 ). '...'}} <a href="{{url('/paymentsystem/'.$paymentsystem->PaymentID)}}"><b>{{ trans('global.more') }}</b></a>
                                                            @else
                                                            {{$paymentsystem->PaymentDescription ? $paymentsystem->PaymentDescription : "N/A"}}
                                                            @endif
                                                        </td>
                                                        <td class="a-right a-right text-warning" >
                                                            <label class="source btn-sm @if($paymentsystem->disabled) btn-danger @else btn-success @endif">@if($paymentsystem->disabled) {{ trans('global.disabled') }} @else {{ trans('global.enable') }} @endif</label>
                                                            <a class="btn btn-sm btn-primary" href="{{url('paymentsystem/' . $paymentsystem->PaymentID)}}">{{ trans('global.edit') }}</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4"><label>{{ trans('psystem.no_psystem') }}</label></td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <?php echo $PaymentSystems->render(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection


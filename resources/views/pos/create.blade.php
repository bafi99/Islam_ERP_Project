@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{isset($POS) ? trans('pos.update_pos')  :  trans('pos.create_pos') }}
                                <small></small>
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')

                            @include('layout.partial.errors')

                            <br/>

                            @if(isset($POS))
                                <p>
                                    <span>
                                        {{ trans('customer.customer_name') }} : <code>{{$POS->Customer()->first()->CustName}}</code> &
                                    </span>
                                    <span>
                                        {{ trans('item.item_name') }} : <code>{{$POS->Item()->first()->ItemName}}</code>.
                                    </span>
                                    <span>
                                        {{ trans('psystem.psystem') }} : <code>{{$POS->PaymentSystem()->first()->PaymentAlias}}</code>.
                                    </span>
                                </p>
                            @endif


                            <div id="wizard" class="form_wizard wizard_horizontal">
                                <ul class="wizard_steps">
                                    <li>
                                        <a href="javascript:void(0)" class="selected">
                                            <span class="step_no">1</span>
                                                    <span class="step_descr">
                                            {{ trans('pos.header_label.step_1') }}<br/>
                                            <small>{{ trans('pos.header_label.step_1_msg') }}</small>
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="@if(isset($POS) && $POS->Confirm){{url('/pos/' . $POS->POSID)}}@elseif(isset($POS)){{url('/pos/' . $POS->POSID)}}@endif" class="@if(isset($POS)){{$POS->Confirm ? 'done' :'selected'}}@else{{'disabled'}}@endif">
                                            <span class="step_no">2</span>
                                                    <span class="step_descr">
                                            {{ trans('pos.header_label.step_2') }}<br/>
                                            <small>{{ trans('pos.header_label.step_2_msg') }}</small>
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="disabled">
                                            <span class="step_no">3</span>
                                                    <span class="step_descr">
                                            {{ trans('pos.header_label.step_3') }}<br/>
                                            <small>{{ trans('pos.header_label.step_3_msg') }}</small>
                                        </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="step-1">
                                    <div id="notification-box" class="alert alert-danger" style="display: none" >
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <div></div>
                                    </div>

                                    <form id="POS-Step-1" class="form-horizontal form-label-left" action="{{url(isset($POS) ? '/pos/'.Crypt::encrypt($POS->POSID) : 'pos'  )}}" method="post">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token"/>
                                        @if(isset($POS))
                                            <input type="hidden" name="_method" value="PUT" />
                                        @endif
                                        {{-- Start Of Customers --}}
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('customer.customer_name') }}
                                                <span class="required">*</span></label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2_single form-control col-md-7 col-xs-12"
                                                        name="customer_name">
                                                    <option value="0">{{ trans('global.not_selected') }}</option>
                                                    @foreach($Customers as $customer)
                                                        <option value="{{Crypt::encrypt($customer->CustomerID)}}" @if(isset($POS) && $POS->Customer_ID == $customer->CustomerID ) selected="selected" @elseif(old('Customer_ID') == $customer->CustomerID) selected="selected" @endif >{{$customer->CustName}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        {{-- End Of Customers --}}

                                        {{-- Start Of Items --}}
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('item.item_name') }}
                                                <span class="required">*</span></label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2_single form-control col-md-7 col-xs-12" name="item_name">
                                                    <option value="0">{{ trans('global.not_selected') }}</option>
                                                    @foreach($Items as $item)
                                                        <option value="{{Crypt::encrypt($item->ItemID)}}" @if(isset($POS) && $POS->Item_ID == $item->ItemID ) selected="selected" @elseif(old('Item_ID') == $item->ItemID) selected="selected" @endif>{{$item->ItemName}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        {{-- End Of Items --}}

                                        {{-- Start Of Payment sSystem --}}
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('psystem.psystem') }}
                                                <span class="required">*</span></label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2_single form-control col-md-7 col-xs-12"
                                                        name="payment_system">
                                                    <option value="0">{{ trans('global.not_selected') }}</option>
                                                    @foreach($PaymentSystems as $paymentsystem)
                                                        <option value="{{Crypt::encrypt($paymentsystem->PaymentID)}}" @if(isset($POS) && $POS->PaymentSystem_ID == $paymentsystem->PaymentID ) selected="selected" @elseif(old('PaymentSystem_ID') == $paymentsystem->PaymentID) selected="selected" @endif >{{$paymentsystem->PaymentAlias}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        {{-- End Of Payment System --}}

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="selling_price" value="@if(isset($POS)){{$POS->SellingPrice}}@else{{ old('SellingPrice') ? old('SellingPrice') : ''   }}@endif" >
                                                      <span class="input-group-btn">
                                                        <label class="btn btn-info">{{ trans('pos.selling_price') }} <span class="required">*</span></label>
                                                      </span>
                                                </div><!-- /input-group -->
                                            </div><!-- /.col-lg-6 -->
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="purchasing_price" value="@if(isset($POS)){{$POS->PurchasingPrice}}@else{{ old('PurchasingPrice') ? old('PurchasingPrice') : ''   }}@endif" >
                                                      <span class="input-group-btn">
                                                        <label class="btn btn-info">{{ trans('pos.purchasing_price') }} <span class="required">*</span></label>
                                                      </span>
                                                </div><!-- /input-group -->
                                            </div><!-- /.col-lg-6 -->
                                        </div><!-- /.row -->

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="deposit" value="@if(isset($POS)){{$POS->Deposit}}@else{{ old('Deposit') ? old('Deposit') : '' }}@endif" >
                                                      <span class="input-group-btn">
                                                        <label class="btn btn-info">{{ trans('pos.deposit') }} <span class="required">*</span></label>
                                                      </span>
                                                </div><!-- /input-group -->
                                            </div><!-- /.col-lg-6 -->
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="interest" value="@if(isset($POS)){{$POS->Interest}}@else{{ old('Interest') ? old('Interest') : '' }}@endif" >
                                                      <span class="input-group-btn">
                                                        <label class="btn btn-info">{{ trans('pos.interest') }} <span class="required">*</span></label>
                                                      </span>
                                                </div><!-- /input-group -->
                                            </div><!-- /.col-lg-6 -->
                                        </div><!-- /.row -->

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="debit" value="@if(isset($POS)){{$POS->Debit}}@else{{ old('Debit') ? old('Debit') : '' }}@endif" >
                                                      <span class="input-group-btn">
                                                        <label id="btn-debit" class="btn btn-primary">{{ trans('pos.debit') }} <span class="required">*</span></label>
                                                      </span>
                                                </div><!-- /input-group -->
                                            </div><!-- /.col-lg-6 -->
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="discount" value="@if(isset($POS)){{$POS->Discount}}@else{{ old('Discount') ? old('Discount') : '' }}@endif">
                                                      <span class="input-group-btn">
                                                        <label class="btn btn-info">{{ trans('pos.discount') }} <span class="required">*</span></label>
                                                      </span>
                                                </div><!-- /input-group -->
                                            </div><!-- /.col-lg-6 -->
                                        </div><!-- /.row -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <div class="col-lg-12">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control date-picker" name="start_date" value="@if(isset($POS)){{$POS->StartDate}}@else{{ old('StartDate') ? old('StartDate') : '' }}@endif" >
                                                              <span class="input-group-btn">
                                                                <label class="btn btn-info">{{ trans('pos.start_date') }} <span class="required">*</span></label>
                                                              </span>
                                                        </div><!-- /input-group -->
                                                    </div><!-- /.col-lg-6 -->
                                                    <br/>
                                                    <div class="col-lg-12">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control date-picker" name="end_date" value="@if(isset($POS)){{$POS->EndDate}}@else{{ old('EndDate') ? old('EndDate') : '' }}@endif" >
                                                              <span class="input-group-btn">
                                                                <label class="btn btn-info">{{ trans('pos.end_date') }} <span class="required">*</span></label>
                                                              </span>
                                                        </div><!-- /input-group -->
                                                    </div><!-- /.col-lg-6 -->
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-group">
                                                        <textarea class="form-control" id="" cols="10" rows="2" name="pos_note">@if(isset($POS)){{$POS->Notes}}@else{{ old('Notes') ? old('Notes') : '' }}@endif</textarea>
                                                          <span class="input-group-btn">
                                                            <label class="btn btn-info">{{ trans('pos.note') }} </label>
                                                          </span>
                                                    </div><!-- /input-group -->
                                                </div>
                                            </div><!-- /.col-lg-6 -->
                                        </div><!-- /.row -->

                                        <div class="actionBar">
                                            <input type="button" class="btn btn-default source" onclick="window.location.href = '/pos/create' " value="{{ trans('global.reset') }}"/>
                                            <input type="button" class="btn btn-primary" id="btn-validate" value="{{ trans('global.validate') }}"/>
                                            <input type="submit" class="btn btn-danger buttonDisabled" value="{{isset($POS) ? trans('global.update')  : trans('global.next') }}"/>
                                        </div>
                                </form>
                                </div>
                            </div>
                        </div>
                        <!-- End SmartWizard Content -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    @include('layout.partial.footer')

    </div>
    <!-- END page content -->

@endsection

@section('customscript')

    <!-- daterangepicker -->
    <script type="text/javascript" src="{{asset('dashboard/js/moment.min2.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/datepicker/daterangepicker.js')}}"></script>
    <!-- Custom js -->
    <script src="{{asset('dashboard/js/custom/pos.js')}}"></script>
    <!-- select2 -->
    <script src="{{asset('dashboard/js/select/select2.full.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".select2_single").select2();
            $('.date-picker').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                format: "YYYY-MM-DD"
            });

        });
    </script>
@endsection

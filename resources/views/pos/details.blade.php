@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('pos.pos_for') }} {{($POS->Customer()->first()->CustName)}}
                                <small></small>
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('layout.partial.success')

                            @include('layout.partial.errors')

                            <br/>

                            @if($POS->Confirm)
                                <p>اضغط التالي لمعرفة مواعيد الدفع <code>ااذا قمت بادخال معاملات لا يمكنك تغيير النظام</code>.</p>
                            @else
                                <p class="text-danger">هذه العملية ليست مراجعه من فضلك قم بمراجعتها حتي يتثني لك ادخال و تعديل المعاملات.</p>
                            @endif
                            <div id="wizard" class="form_wizard wizard_horizontal">
                                <ul class="wizard_steps">
                                    <li>
                                        <a href="{{url('/pos/' . $POS->POSID . '/edit')}}" class="done">
                                            <span class="step_no">1</span>
                                                    <span class="step_descr">
                                            {{ trans('pos.header_label.step_1') }}<br/>
                                            <small>{{ trans('pos.header_label.step_1_msg') }}</small>
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="selected">
                                            <span class="step_no">2</span>
                                                    <span class="step_descr">
                                            {{ trans('pos.header_label.step_2') }}<br/>
                                            <small>{{ trans('pos.header_label.step_2_msg') }}</small>
                                        </span>
                                        </a>
                                    </li>
                                    <?php $transcations = \App\Http\Models\Transaction::where('POS_ID',$POS->POSID)->get() ;?>
                                    <li>
                                        <a href="{{url('transactions/create?pos=' . Crypt::encrypt($POS->POSID))}}" class="{{$transcations->count() ? 'done' : 'disabled'}}">
                                            <span class="step_no">3</span>
                                                    <span class="step_descr">
                                            {{ trans('pos.header_label.step_3') }}<br/>
                                            <small>{{ trans('pos.header_label.step_3_msg') }}</small>
                                        </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="step-1">
                                    <div id="notification-delete-form" class="alert alert-info row invisiblediv" >
                                        <div class="col-md-9">
                                            <strong>اذغط موافق اذا كنت ترد الغاء هذه العملية</strong> عند الغاءم العملية سيتم الغاء جميع المعاملات الخاصة بها ولا يمكنك استرجعها
                                        </div>
                                        <div class="col-md-3">
                                            <form method="post" action="{{url('/pos/'.Crypt::encrypt($POS->POSID))}}">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}" />

                                                <input type="hidden" name="_method" value="DELETE" />

                                                <input type="button" id="cancel-notification" class="btn btn-round btn-default text-primary " value="{{ trans('global.cancel') }}"/>

                                                <input type="submit" class="btn btn-round btn-warning" value="{{ trans('global.confirm') }}"/>
                                            </form>
                                        </div>
                                    </div>
                                    <div id="notification-box" class="alert alert-danger" style="display: none" >
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <div></div>
                                    </div>
                                    <div class="form-horizontal form-label-left">
                                        {{-- Start Of Customers --}}
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('customer.customer_name') }}
                                                <span class="required">*</span></label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label for="middle-name" class="control-label">{{$POS->Customer()->first()->CustName}}</label>
                                            </div>
                                        </div>
                                        {{-- End Of Customers --}}

                                        {{-- Start Of Items --}}
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('item.item_name') }}
                                                <span class="required">*</span></label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label for="middle-name" class="control-label">{{$POS->Item()->first()->ItemName}}</label>
                                            </div>
                                        </div>
                                        {{-- End Of Items --}}

                                        {{-- Start Of Payment sSystem --}}
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('psystem.psystem') }}
                                                <span class="required">*</span></label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label for="middle-name" class="control-label">{{$POS->PaymentSystem()->first()->PaymentAlias}}</label>
                                            </div>
                                        </div>
                                        {{-- End Of Payment System --}}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.selling_price') }} LE <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <input type="text" id="first-name2" required="required" class="form-control col-md-7 col-xs-12" disabled
                                                       value="{{$POS->SellingPrice}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.purchasing_price') }} LE <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <input type="text" id="first-name2" required="required" class="form-control col-md-7 col-xs-12" disabled
                                                       value="{{$POS->PurchasingPrice}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.deposit') }} LE <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <input type="text" id="first-name2" required="required" class="form-control col-md-7 col-xs-12" disabled
                                                       value="{{$POS->Deposit}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.interest') }} % <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <input type="text" id="first-name2" required="required" class="form-control col-md-7 col-xs-12" disabled
                                                       value="{{$POS->Interest}}%">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.discount') }} LE <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <input type="text" id="first-name2" required="required" class="form-control col-md-7 col-xs-12" disabled
                                                       value="{{$POS->Discount}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.debit') }} LE <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <input type="text" id="first-name2" required="required" class="form-control col-md-7 col-xs-12" disabled
                                                       value="{{$POS->Debit}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.start_date') }} <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <input type="text" id="first-name2" required="required" class="form-control col-md-7 col-xs-12" disabled
                                                       value="{{$POS->StartDate}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.end_date') }} <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <input type="text" id="first-name2" required="required" class="form-control col-md-7 col-xs-12" disabled
                                                       value="{{$POS->EndDate}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.note') }} <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <label >{{$POS->Notes}}</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.confirm') }}  <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="col-md-3 col-sm-3">
                                                    <i class="{{$POS->Confirm ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                                </div>
                                                @if(!$POS->Confirm)
                                                    <div class="col-md-9 col-sm-9">
                                                        <a href="{{url('pos/confirm/' . Crypt::encrypt($POS->POSID) )}}" class="btn btn-default btn-sm text-success " >{{ trans('pos.click_to_confirm') }}</a>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3" for="first-name">{{ trans('pos.finished') }}  <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6">

                                                <form method="post" action="{{url('/pos/togglefinished/'. Crypt::encrypt($POS->POSID ))}}">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <input type="hidden" name="_method" value="patch"/>
                                                    <div class="col-md-3 col-sm-3">
                                                        @if($POS->Finished)
                                                            <i class="fa-check-circle text-success fa-2x fa"></i>
                                                        @else
                                                            <i class="fa-times-circle text-danger fa-2x fa"></i>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <input type="submit" class="btn btn-dark" value="{{$POS->Finished ? 'فتح' : 'غلق'}} العملية"/>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="POS-Step-1" class="form-horizontal form-label-left" action="{{url('/pos')}}" method="post">
                                        <div class="actionBar">
                                            <a href="#notification-delete-form" id="deletepos" class="btn btn-danger source" >{{ trans('global.delete') }}</a>
                                            <a href="{{url('/pos/' . $POS->POSID)}}/edit" class="btn btn-primary source" >{{ trans('global.edit') }}</a>
                                            @if($POS->Confirm)
                                                <a href="{{url('transactions/create?pos=' . Crypt::encrypt($POS->POSID))}}" class="btn btn-success">{{ trans('global.next') }}</a>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End SmartWizard Content -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    @include('layout.partial.footer')

    </div>
    <!-- END page content -->

@endsection

@section('customscript')
    <script type="text/javascript">
        (function(){
            $('#deletepos').on('click' ,function(){
                $('#notification-delete-form').slideDown()
            });
            $('#cancel-notification').on('click' ,function(){
                $('#notification-delete-form').slideUp()
            })
        })();
    </script>
@endsection
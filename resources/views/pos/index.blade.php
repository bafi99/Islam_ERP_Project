@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">

                            @include('layout.partial.success')
                            @include('layout.partial.errors')

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>{{ trans('pos.all_pos') }} <small><code>{{ trans('pos.pos_info') }}</code></small></h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                                            <thead>
                                            <tr class="headings">
                                                <th>{{ trans('global.serial') }} </th>
                                                <th>{{ trans('pos.name') }} </th>
                                                <th>{{ trans('item.item_name') }} </th>
                                                <th>{{ trans('psystem.psystem') }} </th>
                                                <th>{{ trans('pos.start_date') }} </th>
                                                <th>{{ trans('pos.end_date') }} </th>
                                                <th>{{ trans('pos.confirm') }} </th>
                                                <th>{{ trans('pos.finish') }} </th>
                                                <th class=" no-link last"><span class="nobr">{{ trans('global.action') }}</span>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($POSS as $k => $pos )
                                            <tr class="pointer">
                                                <td>{{$k + 1 }}</td>
                                                <td>{{$pos->Customer()->first()->CustName}}</td>
                                                <td>{{$pos->Item()->first()->ItemName}}</td>
                                                <td>{{$pos->PaymentSystem()->first()->PaymentAlias}}</td>
                                                <td>{{$pos->StartDate}}</td>
                                                <td>{{$pos->EndDate}}</td>
                                                <td><i class="{{$pos->Confirm ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i></td>
                                                <td><i class="{{$pos->Finished ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i></td>
                                                <td>
                                                    <a href="{{url('/pos/' . $pos->POSID )}}" class="btn btn-sm btn-primary" >{{ trans('global.edit') }}</a>
                                                    @if($pos->Confirm)
                                                        <a href="{{url('/transactions/create?pos=' . Crypt::encrypt($pos->POSID) )}}" class="btn btn-sm btn-primary" >{{ trans('pos.transactions') }}</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection

@section('customscript')

    <!-- Custom styling plus plugins -->
    <link href="{{asset('dashboard/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('/dashboard/css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
    <!-- Datatables -->
    <script src="{{asset('/dashboard/js/datatables/js/jquery.dataTables.js')}}"></script>
    <script>
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip'
            });

        });
    </script>
@endsection

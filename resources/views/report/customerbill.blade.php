@extends('report.header')

@section('content')
    <!-- page content -->
    <div class="right_col " role="main" >
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>{{ trans('transactions.transaction_details') }}<small></small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('customer.customer_name') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->Customer()->first()->CustName}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.deposit') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->Deposit}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('psystem.psystem') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->PaymentSystem()->first()->PaymentAlias}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.interest') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->Interest}}%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('item.item_name') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->Item()->first()->ItemName}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.discount') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->Discount}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.start_date') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->StartDate}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.selling_price') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->SellingPrice}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.end_date') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{$POS->EndDate}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.debit') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" id="debit_value" class="btn-info form-control" disabled="disabled" value="{{$POS->Debit}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <?php $TotalAmountPaid = \App\Http\Models\Transaction::GetTotalOfAmountPaid($POS->POSID) ; ?>
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('transactions.total_paid') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="{{$TotalAmountPaid < $POS->Debit ? 'btn-danger' : 'btn-success'}}  form-control" disabled="disabled"
                                                           value="{{ $TotalAmountPaid }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.finished') }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <p>
                                                        <i class="{{$POS->Finished ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ 'المتبقي من الاقساط' }} : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" id="debit_value" class="btn-primary form-control" disabled="disabled" value="{{$POS->Debit - $TotalAmountPaid}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>{{ trans('transactions.payment_dates') }}<small></small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table class="table table-striped responsive-utilities jambo_table" dir="rtl">
                                        <thead>
                                        <tr class="headings">
                                            <th class="column-title text-center">{{ trans('global.serial') }}</th>
                                            <th class="column-title text-center">{{ trans('global.status') }}</th>
                                            <th class="column-title text-center">{{ trans('transactions.payment_date') }}</th>
                                            <th class="column-title text-center">{{ trans('transactions.expected_paid') }} </th>
                                            <th class="column-title text-center">{{ trans('transactions.actually_paid') }} </th>
                                            <th class="column-title text-center">{{ trans('transactions.paid') }} </th>
                                            <th class="column-title text-center" style="width: 30%">{{ trans('transactions.notes') }} </th>
                                            <th class="column-title text-center">Actions </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php $paid_transaction = $unpaid_transaction = 0 ?>
                                            @foreach($Transactions as $k => $transaction)
                                                <?php $transaction->Done ? $paid_transaction++ : $unpaid_transaction++ ?>
                                                <tr class="even pointer">
                                                    <td class="text-center text-info" >
                                                        <label class="btn btn-primary btn-sm">{{$k + 1}}</label>
                                                    </td>
                                                    <td class="text-center text-info" >{{ $transaction->ISDate ? trans('psystemdates.date') : trans('psystemdates.profit') }}</td>
                                                    <td class="text-center text-info" ><label>{{ $transaction->PaymentDate }}</label></td>
                                                    <td class="text-center text-info" >
                                                        <input type="text" class="form-control expected_transaction_paid" disabled="disabled" value="{{$transaction->ExpectedAmount}}">
                                                    </td>
                                                    <td class="text-center text-info" >
                                                        <input type="text" class="form-control" disabled="disabled" value="{{$transaction->AmountPaid}}">
                                                    </td>
                                                    <td class="text-center text-info" >
                                                        <i class="{{$transaction->Done ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                                    </td>
                                                    <td class="text-center text-info" >
                                                        @if($transaction->Notes)
                                                            <label>
                                                                <a class="source" onclick="show_notification('{{$transaction->Notes}}' ,'Notes about ({{$transaction->PaymentDate}})')">
                                                                    {{strlen($transaction->Notes) > 60 ? mb_strimwidth($transaction->Notes ,0 ,60 ) . trans('global.read_more').'...' : $transaction->Notes }}
                                                                </a>
                                                            </label>
                                                        @else
                                                            <label class="source">N/A</label>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{url('/transactions/'. $transaction->TransactionID)}}" class="btn btn-sm btn-primary">{{ trans('global.edit') }}</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
@include('layout.partial.footer')
    <!-- END page content -->
@endsection
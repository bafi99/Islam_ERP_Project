@extends('report.header')

@section('content')
    <!-- page content -->
    <div class="right_col " role="main" dir="rtl" >
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-6 col-sm-12 col-xs-12">

                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>
                                        <label class="btn btn-primary">{{ trans('report.start_from_day') }} : {{Input::get('start_date')}}</label>
                                        <label class="btn btn-success">{{ trans('report.end_in_day') }} : {{Input::get('end_date')}}</label>
                                    </h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-lg-12">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{--*/ $_POS = clone $POS /*--}}{{$_POS->count()}}">
                                                </div>
                                                <label class="control-label col-md-5 col-sm-5 col-xs-12">{{ trans('report.total_pos') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{--*/ $_POS = clone $POS /*--}}{{$_POS->whereFinished('0')->count()}}">
                                                </div>
                                                <label class="control-label col-md-5 col-sm-5 col-xs-12">{{ trans('report.total_open_pos') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{--*/ $_POS = clone $POS /*--}}{{$_POS->whereFinished('1')->count()}}">
                                                </div>
                                                <label class="control-label col-md-5 col-sm-5 col-xs-12">{{ trans('report.total_close_pos') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{--*/ $_POS = clone $POS /*--}}{{ $SellingPrice = $_POS->sum('SellingPrice') }}">
                                                </div>
                                                <label class="control-label col-md-5 col-sm-5 col-xs-12">{{ trans('report.total_selling') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{--*/ $_POS = clone $POS /*--}}{{ $_POS->sum('PurchasingPrice')}}">
                                                </div>
                                                <label class="control-label col-md-5 col-sm-5 col-xs-12">{{ trans('report.total_purchasing') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{ $_POS->select(DB::raw('SUM( (SellingPrice - PurchasingPrice)) as total'))->get()->first()->total }}">
                                                </div>
                                                <label class="control-label col-md-5 col-sm-5 col-xs-12">{{ trans('report.total_margin_profit') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{--*/ $_POS = clone $POS /*--}}{{ $SellingPrice = $_POS->select(DB::raw('SUM( (Debit + Deposit)) as total'))->get()->first()->total }}">
                                                </div>
                                                <label class="control-label col-md-5 col-sm-5 col-xs-12">{{ trans('report.total_selling_with_profit') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-horizontal form-label-left input_mask">
                                            <div class="form-group">
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" class="form-control" disabled="disabled" value="{{--*/ $_POS = clone $POS /*--}}{{ $_POS->select(DB::raw('SUM( (Debit + Deposit) - PurchasingPrice ) as total'))->get()->first()->total }}">
                                                </div>
                                                <label class="control-label col-md-5 col-sm-5 col-xs-12">{{ trans('report.total_profit_with_interests') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
    @include('layout.partial.footer')
    <!-- END page content -->
@endsection

    <script src="{{asset('dashboard/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('dashboard/js/nicescroll/jquery.nicescroll.min.js')}}"></script>

    <script src="{{asset('dashboard/js/custom.js')}}"></script>

    @yield('customscript')

    </body>

</html>
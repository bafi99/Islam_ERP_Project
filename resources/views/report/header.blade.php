<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{csrf_token()}}">

    <title>Expenses Project</title>

    <!-- Bootstrap core CSS -->

    <link href="{{asset('dashboard/css/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{asset('dashboard/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{{asset('dashboard/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/editor/index.css')}}" rel="stylesheet">
    <!-- switchery -->
    <link rel="stylesheet" href="{{asset('dashboard/css/switchery/switchery.min.css')}}" />

    <script src="{{asset('dashboard/js/jquery.min.js')}}"></script>
    <style>
        .invisiblediv{
            display: none;
        }
    </style>
</head>

<body class="nav-sm">

<div class="container body">

    <div class="main_container">

        @yield('content')

    </div>
</div>
<!-- Start Footer -->
@include('report.footer')
<!-- End Footer -->
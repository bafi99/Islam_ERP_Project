@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="text-right">

            <div class="page-title text-right">
                <div class="title_left">
                    <h1>{{ trans('report.reports') }}</h1>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('report.generate_next_payment') }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div class="form-horizontal form-label-left">
                                <form method="get" action="{{url('reports/next_payment_dates')}}" target="_blank">
                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <input type="text"  class="form-control date-picker" name="from_date" value="{{\Carbon\Carbon::today()->format('Y-m-d')}}" />
                                        </div>
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12">{{ trans('report.start_date') }}</label>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <input type="text"  class="form-control date-picker" name="to_date" value="{{\Carbon\Carbon::today()->format('Y-m-d')}}" />
                                        </div>
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12">{{ trans('report.end_date') }}</label>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <select name="payment_status" class="form-control">
                                                <option value="all">{{ trans('report.all_transaction') }}</option>
                                                <option value="1">{{ trans('report.paid_transaction') }}</option>
                                                <option value="0">{{ trans('report.unpaid_transaction') }}</option>
                                            </select>
                                        </div>
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12">{{ trans('report.paid_status') }}</label>
                                    </div>
                                    <div class="actionBar">
                                        <input type="submit" class="btn btn-success" name="print" value="{{ trans('report.print') }}" />
                                        <input type="submit" class="btn btn-primary" value="{{ trans('report.view_report') }}" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('report.customer_bill') }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div class="form-horizontal form-label-left">
                                <form method="get" action="{{url('reports')}}">
                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <select class="form-control select2_singled" name="customer_id">
                                                <option value="0">Not Selected</option>
                                                @foreach($Customers as $Customer)
                                                <option value="{{$Customer->CustomerID}}">{{$Customer->CustName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12">{{ trans('report.customer_name') }}</label>
                                    </div>
                                    <div class="form-group">
                                        @if( isset($CustomerBills) )
                                        <div class="col-xs-12">
                                            <label class="btn-sm btn-dark">{{ $CustomerBills->CustName }}</label>
                                            <table class="table table-striped table-bordered " >
                                                <thead style="background-color: #e3e3e3">
                                                    <td>{{ trans('report.item') }}</td>
                                                    <td style="width: 20%">{{ trans('report.start_date') }}</td>
                                                    <td style="width: 20%">{{ trans('report.end_date') }}</td>
                                                    <td style="width: 5%">{{ trans('report.closed_pos') }}</td>
                                                    <td style="width: 30%">Action</td>
                                                </thead>
                                                <tbody>
                                                    @foreach($CustomerBills->POS()->get() as $Bill )
                                                    <tr>
                                                        <td>{{$Bill->Item()->first()->ItemName}}</td>
                                                        <td>{{$Bill->StartDate}}</td>
                                                        <td>{{$Bill->EndDate}}</td>
                                                        <td><i class="{{$Bill->Finished ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i></td>
                                                        <td>
                                                            <a class="btn-sm btn-primary" target="_blank" href="{{'reports/customer_bill/' . $Bill->POSID}}">{{ trans('report.view') }}</a>
                                                            <a class="btn-sm btn-success" target="_blank" href="{{'reports/customer_bill/' . $Bill->POSID . '?print=print'}}">{{ trans('report.print') }}</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @if( 0 == $CustomerBills->POS()->count() )
                                                        <tr>
                                                            <td colspan="5" class="text-center text-danger"><strong>Not Bill For This Selected Customer</strong></td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="actionBar">
                                        <input type="submit" class="btn btn-primary" value="{{ trans('report.search_customer_pos') }}" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('report.remaining_transactions') }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div class="form-horizontal form-label-left">
                                <form method="get" action="{{url('reports/remaining_transactions')}}" target="_blank">
                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <select name="remaining_transaction" class="form-control">
                                                {{--*/ $index = 1 /*--}}
                                                @for($index ; $index <= 10 ; $index++)
                                                <option value="{{$index}}">{{$index}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12">{{ trans('report.num_of_remaining') }}</label>
                                    </div>
                                    <div class="actionBar">
                                        <input type="submit" class="btn btn-success" name="print" value="{{ trans('report.print') }}" />
                                        <input type="submit" class="btn btn-primary" value="{{ trans('report.view_report') }}" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('report.finicial_report') }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div class="form-horizontal form-label-left">
                                <form method="get" action="{{url('reports/finicial_postion')}}" target="_blank">
                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <input type="text"  class="form-control date-picker" name="start_date" value="{{\Carbon\Carbon::today()->format('Y-m-d')}}" />
                                        </div>
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12">{{ trans('report.start_from_day') }}</label>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <input type="text"  class="form-control date-picker" name="end_date" value="{{\Carbon\Carbon::today()->format('Y-m-d')}}" />
                                        </div>
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12">{{ trans('report.end_in_day') }}</label>
                                    </div>
                                    <div class="actionBar">
                                        <input type="submit" class="btn btn-success" name="print" value="{{ trans('report.print') }}" />
                                        <input type="submit" class="btn btn-primary" value="{{ trans('report.view_report') }}" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection
@section('customscript')

    <!-- daterangepicker -->
    <script type="text/javascript" src="{{asset('dashboard/js/moment.min2.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/datepicker/daterangepicker.js')}}"></script>
    <!-- select2 -->
    <script src="{{asset('dashboard/js/select/select2.full.js')}}"></script>
    <script type="text/javascript">
        (function(){

            $('.date-picker').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                format: "YYYY-MM-DD"
            });
            $('.select2_singled').select2();
        })();
    </script>
@endsection


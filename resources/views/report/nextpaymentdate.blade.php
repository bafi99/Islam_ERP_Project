@extends('report.header')

@section('content')
    <!-- page content -->
    <div class="right_col " role="main" dir="rtl">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>تقرير عن عمليات البيع</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                        <tr class="headings">
                                            <th class="column-title text-center">{{ trans('global.serial') }}</th>
                                            <th class="column-title text-center">{{ trans('customer.customer_name') }}</th>
                                            <th class="column-title text-center">{{ trans('item.item_name') }}</th>
                                            <th class="column-title text-center">{{ trans('psystem.psystem') }}</th>
                                            <th class="column-title text-center">{{ trans('transactions.payment_date') }}</th>
                                            <th class="column-title text-center">{{ trans('transactions.expected_paid') }} </th>
                                            <th class="column-title text-center">{{ trans('transactions.actually_paid') }} </th>
                                            <th class="column-title text-center">{{ trans('transactions.paid') }} </th>
                                            <th class="column-title text-center">{{ trans('report.remaining_balance') }}</th>
                                            <th style="width: 20%" class="no-link last text-center"><span class="nobr">{{ trans('transactions.notes') }}</span>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($Transactions as $k => $transaction)
                                                <tr>
                                                    <td>
                                                        <a class="btn-sm btn-dark" href="{{url('/transactions/' . $transaction->TransactionID)}}">
                                                            {{$k + 1}}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {{$transaction->POS()->first()->Customer()->first()->CustName}}
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {{$transaction->POS()->first()->Item()->first()->ItemName}}
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {{$transaction->POS()->first()->PaymentSystem()->first()->PaymentAlias}}
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {{$transaction->PaymentDate}}
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {{$transaction->ExpectedAmount}}
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {{$transaction->AmountPaid}}
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            <i class="{{$transaction->Done ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="text-danger">
                                                            {{'(' . $transaction->GetTotalOfAmountPaid($transaction->POS_ID).' /'}}
                                                        </label>
                                                        <label class="text-success">
                                                            {{$transaction->POS()->first()->Debit . ')'}}
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {{$transaction->Notes}}
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">
                                                    <label class="btn btn-primary">المتوقع دفعه</label>
                                                    <label class="btn btn-primary">
                                                        {{$TotalTransactions['ExpectedAmount']}}
                                                    </label>
                                                </td>
                                                <td colspan="2">
                                                    <label class="btn btn-dark">المدفوغ</label>
                                                    <label class="btn btn-dark">
                                                        {{$TotalTransactions['AmountPaid']}}
                                                    </label>
                                                </td>
                                                <td colspan="4">
                                                    <label class="btn btn-success">المتبقي</label>
                                                    <label class="btn btn-success">
                                                        {{$TotalTransactions['ExpectedAmount'] - $TotalTransactions['AmountPaid']}}
                                                    </label>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                {!! $Transactions->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
@include('layout.partial.footer')
    <!-- END page content -->
@endsection
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Customer Bill</title>
</head>
<body>
<?php $TotalAmountPaid = \App\Http\Models\Transaction::GetTotalOfAmountPaid($POS->POSID) ; ?>
    <h3>بيانات العميل</h3>
    <table>
        <tbody>
            <tr>
                <td><strong>{{ trans('customer.customer_name') }}</strong></td>
                <td>{{ $POS->Customer()->first()->CustName }}</td>
                <td><strong>{{ trans('pos.deposit') }}</strong></td>
                <td>{{ $POS->Deposit }}</td>
            </tr>
            <tr>
                <td><strong>{{ trans('pos.interest') }}</strong></td>
                <td>{{ $POS->Interest }}%</td>
                <td><strong>{{ trans('psystem.psystem') }}</strong></td>
                <td>{{ $POS->PaymentSystem()->first()->PaymentAlias }}</td>
            </tr>
            <tr>
                <td><strong>{{ trans('pos.discount') }}</strong></td>
                <td>{{ $POS->Discount }}</td>
                <td><strong>{{ trans('item.item_name') }}</strong></td>
                <td>{{ $POS->Item()->first()->ItemName }}</td>
            </tr>
            <tr>
                <td><strong>{{ trans('pos.selling_price') }}</strong></td>
                <td>{{ $POS->SellingPrice }}</td>
                <td><strong>{{ trans('pos.start_date') }}</strong></td>
                <td>{{ $POS->StartDate }}</td>
            </tr>
            <tr>
                <td><strong>{{ trans('pos.debit') }}</strong></td>
                <td>{{ $POS->Debit }}</td>
                <td><strong>{{ trans('pos.end_date') }}</strong></td>
                <td>{{ $POS->EndDate }}</td>
            </tr>
            <tr>
                <td><strong>{{ trans('pos.finished') }}</strong></td>
                <td>{{ $POS->Finished ? 'نعم' : 'لا' }}</td>
                <td><strong>{{ trans('transactions.total_paid') }}</strong></td>
                <td>{{ $TotalAmountPaid }}</td>
            </tr>
            <tr>
                <td><strong>{{ trans('المتبقي من الاقساط') }}</strong></td>
                <td>{{ $POS->Debit - $TotalAmountPaid }}</td>
            </tr>
        </tbody>
    </table>
    <br/>
    <br/>
        <center>
            <h3>مواعيد الدفع</h3>
        </center>
    <br/>
    <br/>
    <table border="1">
        <thead>
        <tr>
            <th style="width: 5%"><strong>{{ trans('global.serial') }}</strong></th>
            <th><strong>{{ trans('global.status') }}</strong></th>
            <th><strong>{{ trans('transactions.payment_date') }}</strong></th>
            <th><strong>{{ trans('transactions.expected_paid') }}</strong> </th>
            <th><strong>{{ trans('transactions.actually_paid') }}</strong> </th>
            <th><strong>{{ trans('transactions.paid') }}</strong> </th>
            <th><strong>{{ trans('transactions.notes') }}</strong> </th>
        </tr>
        </thead>
        <tbody>
        <?php $paid_transaction = $unpaid_transaction = 0 ?>
        @foreach($Transactions as $k => $transaction)
            <?php $transaction->Done ? $paid_transaction++ : $unpaid_transaction++ ?>
            <tr>
                <td style="width: 5%">
                    <label>{{$k + 1}}</label>
                </td>
                <td>{{ $transaction->ISDate ? trans('psystemdates.date') : trans('psystemdates.profit') }}</td>
                <td><label>{{ $transaction->PaymentDate }}</label></td>
                <td>
                    <label>{{$transaction->ExpectedAmount}}</label>
                </td>
                <td>
                    <label>{{$transaction->AmountPaid}}</label>
                </td>
                <td>
                    <label>{{$transaction->Done ? 'نعم' : 'لا'}}</label>
                </td>
                <td>
                    @if($transaction->Notes)
                        {{$transaction->Notes}}
                    @else
                        <label class="source">N/A</label>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
        <br/>
        <br/>
        <h3>التوقيع</h3>
        ........................
    </table>
</body>
</html>
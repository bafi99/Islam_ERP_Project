<!doctype html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <title>Finicial Report</title>
</head>
<body>

<h2 style="text-align: center">{{ trans('report.finicial_report') }}</h2>
<span>{{ trans('report.start_from_day') }} : </span> <label>{{Input::get('start_date')}}</label>
<br/>
<span>{{ trans('report.end_in_day') }} : </span> <label>{{Input::get('end_date')}}</label>
<br/>
<br/>
<br/>
    <table>
        <tbody>
            <tr>
                <td>{{ trans('report.total_pos') }}</td>
                <td>{{--*/ $_POS = clone $POS /*--}}{{$_POS->count()}}</td>
            </tr>
            <tr>
                <td>{{ trans('report.total_open_pos') }}</td>
                <td>{{--*/ $_POS = clone $POS /*--}}{{$_POS->whereFinished('0')->count()}}</td>
            </tr>
            <tr>
                <td>{{ trans('report.total_close_pos') }}</td>
                <td>{{--*/ $_POS = clone $POS /*--}}{{$_POS->whereFinished('1')->count()}}</td>
            </tr>
            <tr>
                <td>{{ trans('report.total_selling') }}</td>
                <td>{{--*/ $_POS = clone $POS /*--}}{{ $SellingPrice = $_POS->sum('SellingPrice') }}</td>
            </tr>
            <tr>
                <td>{{ trans('report.total_purchasing') }}</td>
                <td>{{--*/ $_POS = clone $POS /*--}}{{ $_POS->sum('PurchasingPrice')}}</td>
            </tr>
            <tr>
                <td>{{ trans('report.total_margin_profit') }}</td>
                <td>{{ $_POS->select(DB::raw('SUM( (SellingPrice - PurchasingPrice)) as total'))->get()->first()->total }}</td>
            </tr>
            <tr>
                <td>{{ trans('report.total_selling_with_profit') }}</td>
                <td>{{--*/ $_POS = clone $POS /*--}}{{ $SellingPrice = $_POS->select(DB::raw('SUM( (Debit + Deposit)) as total'))->get()->first()->total }}</td>
            </tr>
            <tr>
                <td>{{ trans('report.total_profit_with_interests') }}</td>
                <td>{{--*/ $_POS = clone $POS /*--}}{{ $_POS->select(DB::raw('SUM( (Debit + Deposit) - PurchasingPrice ) as total'))->get()->first()->total }}</td>
            </tr>
        </tbody>
    </table>

</body>
</html>
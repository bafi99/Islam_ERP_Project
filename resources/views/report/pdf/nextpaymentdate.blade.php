<!doctype html>
<html lang="en" dir="rtl" >
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
</head>
<body>
<table cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th style="width: 4%"><strong>{{ trans('global.serial') }}</strong></th>
        <th style="width: 10%"><strong>{{ trans('customer.customer_name') }}</strong></th>
        <th style="width: 10%"><strong>{{ trans('item.item_name') }}</strong></th>
        <th style="width: 15%"><strong>{{ trans('psystem.psystem') }}</strong></th>
        <th style="width: 10%"><strong>{{ trans('transactions.payment_date') }}</strong></th>
        <th style="width: 5%"><strong>{{ trans('transactions.expected_paid') }}</strong></th>
        <th style="width: 5%"><strong>{{ trans('transactions.actually_paid') }}</strong></th>
        <th style="width: 5%"><strong>{{ trans('transactions.paid') }}</strong></th>
        <th style="width: 10%"><strong>{{ trans('report.remaining_balance') }}</strong></th>
        <th style="width: 25%"><span>{{ trans('transactions.notes') }}</span>
        </th>
    </tr>
    <tr>
        <td colspan="7"></td>
    </tr>
    </thead>
    <tbody>
    @foreach($Transactions as $k => $transaction)
        <tr>
            <td style="width: 4%">
                <strong><label>{{$k + 1}}</label></strong>
            </td>
            <td style="width: 10%">
                <label>
                    {{$transaction->POS()->first()->Customer()->first()->CustName}}
                </label>
            </td>
            <td style="width: 10%">
                <label>
                    {{$transaction->POS()->first()->Item()->first()->ItemName}}
                </label>
            </td>
            <td style="width: 15%">
                <label>
                    {{$transaction->POS()->first()->PaymentSystem()->first()->PaymentAlias}}
                </label>
            </td>
            <td style="width: 10%">
                <label>
                    {{$transaction->PaymentDate}}
                </label>
            </td>
            <td style="width: 5%">
                <label>
                    {{$transaction->ExpectedAmount}}
                </label>
            </td>
            <td style="width: 5%">
                <label>
                    {{$transaction->AmountPaid}}
                </label>
            </td>
            <td style="width: 5%">
                <label>
                    <label>{{$transaction->Done ? 'نعم' : 'لا'}}</label>
                </label>
            </td>
            <td style="width: 10%">
                <label>
                    <strong>{{$transaction->POS()->first()->Debit . ')'}}</strong>
                </label>
                <label>
                    {{'(' . $transaction->GetTotalOfAmountPaid($transaction->POS_ID).' /'}}
                </label>
            </td>
            <td style="width: 25%">
                <label>
                    {{$transaction->Notes}}
                </label>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<h3><span>    المتوقع دفعه</span><label>{{$TotalTransactions['ExpectedAmount']}} = </label></h3>

<h3><span>المدفوغ</span><label>{{$TotalTransactions['AmountPaid']}} = </label></h3>

<h3>المتبقي<label>{{$TotalTransactions['ExpectedAmount'] - $TotalTransactions['AmountPaid']}}=</label></h3>
</body>
</html>
<!doctype html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
</head>
<body>
<table cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th style="width: 4%"><strong>{{ trans('global.serial') }}</strong></th>
        <th style="width: 20%"><strong>{{ trans('customer.customer_name') }}</strong></th>
        <th style="width: 20%"><strong>{{ trans('item.item_name') }}</strong></th>
        <th style="width: 20%"><strong>{{ trans('psystem.psystem') }}</strong></th>
        <th style="width: 15%"><strong>{{ trans('report.done_undone') }}</strong></th>
        <th style="width: 10%"><strong>{{ trans('report.paid_debit') }}</strong></th>
        <th style="width: 16%"><strong>{{ trans('report.remainging') }}</strong></th>
    </tr>
    </thead>
    <tbody>
    {{--*/ $index = 1 /*--}}
    @foreach($POSS as $key => $POS)
        @if($POS['undone'] <= Input::get('remaining_transaction') )
            {{--*/ $POS = \App\Http\Models\POS::find($key) /*--}}
            <tr>
                <td style="width: 4%">
                    <strong><label>{{$index++}}</label></strong>
                </td>
                <td style="width: 20%">
                    <label>
                        {{$POS->Customer()->first()->CustName}}
                    </label>
                </td>
                <td style="width: 20%">
                    <label>
                        {{$POS->Item()->first()->ItemName}}
                    </label>
                </td>
                <td style="width: 20%">
                    <label>
                        {{$POS->PaymentSystem()->first()->PaymentAlias}}
                    </label>
                </td>
                <td style="width: 15%">
                    <label>
                        {{' ( ' . $POSS[$key]['done'] .' / ' . $POSS[$key]['undone'] .' ) '}}
                    </label>
                </td>
                <td style="width: 10%">
                    <label>
                        {{'( ' . \App\Http\Models\Transaction::GetTotalOfAmountPaid($key) .' / ' . $POS->Debit . ' )'}}
                    </label>
                </td>
                <td style="width: 16%">
                    <label>
                        {{ $POS->Debit - \App\Http\Models\Transaction::GetTotalOfAmountPaid($key) }}
                    </label>
                </td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>
</body>
</html>
@extends('report.header')

@section('content')
    <!-- page content -->
    <div class="right_col " role="main" dir="rtl">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>{{ trans('report.remaining_transactions') }}</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                        <tr class="headings ">
                                            <th class="column-title text-center">{{ trans('global.serial') }}</th>
                                            <th class="column-title text-center">{{ trans('customer.customer_name') }}</th>
                                            <th class="column-title text-center">{{ trans('item.item_name') }}</th>
                                            <th class="column-title text-center">{{ trans('psystem.psystem') }}</th>
                                            <th class="column-title text-center">{{ trans('report.done_undone') }}</th>
                                            <th class="column-title text-center">{{ trans('report.paid_debit') }}</th>
                                            <th class="column-title text-center">{{ trans('report.remainging') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            {{--*/ $index = 1 /*--}}
                                        @foreach($POSS as $key => $POS)
                                            @if($POS['undone'] <= Input::get('remaining_transaction') )
                                            {{--*/ $POS = \App\Http\Models\POS::find($key) /*--}}
                                            <tr class="text-center">
                                                <td>
                                                    <a class="btn btn-sm btn-primary" href="{{url('/transactions/create?pos=') . \Crypt::encrypt($key)}}">{{$index++}}</a>
                                                </td>
                                                <td>{{$POS->Customer()->first()->CustName}}</td>
                                                <td>{{$POS->Item()->first()->ItemName}}</td>
                                                <td>{{$POS->PaymentSystem()->first()->PaymentAlias}}</td>
                                                <td>{{ ' ( ' . $POSS[$key]['done'] .' / ' . $POSS[$key]['undone'] .' ) '  }}</td>
                                                <td>
                                                    <label class="text-danger">
                                                        {{'( ' . \App\Http\Models\Transaction::GetTotalOfAmountPaid($key) .' / '}}
                                                    </label>
                                                    <label class="text-success">
                                                        {{$POS->Debit . ' )'}}
                                                    </label>
                                                </td>
                                                <td>
                                                    <label class="text-success">{{ $POS->Debit - \App\Http\Models\Transaction::GetTotalOfAmountPaid($key) }}</label>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
@include('layout.partial.footer')
    <!-- END page content -->
@endsection
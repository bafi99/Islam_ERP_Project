<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-arrow-circle-down"></i> Category <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/category')}}">Categories</a></li>

                    <li><a href="{{url('/category/create')}}">Create Category</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> Item <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/item')}}">Items</a></li>

                    <li><a href="{{url('/item/create')}}">Create Item</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> Customer <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/customer')}}">All Customers </a></li>

                    <li><a href="{{url('/customer/create')}}">Create Customer</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> Payment System <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/paymentsystem')}}">All Payment System</a></li>

                    <li><a href="{{url('/paymentsystem/create')}}">New Payment System</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> Payment Dates <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/paymentdates')}}">All Payment Dates</a></li>

                    <li><a href="{{url('/paymentdates/create')}}">New Payment Date</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> Point Of Sale <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/pos')}}">All POS </a></li>

                    <li><a href="{{url('/pos/create')}}">New POS</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/transactions/create')}}">New Transaction</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-arrow-circle-down"></i> Report <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('/reports')}}">Report</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>
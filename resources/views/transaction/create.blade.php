@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main" xmlns="http://www.w3.org/1999/html">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xs-12">

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('transactions.transaction_details') }}<small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('customer.customer_name') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->Customer()->first()->CustName}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.deposit') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->Deposit}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('psystem.psystem') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->PaymentSystem()->first()->PaymentAlias}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.interest') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->Interest}}%">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('item.item_name') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->Item()->first()->ItemName}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.discount') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->Discount}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.purchasing_price') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->PurchasingPrice}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.start_date') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->StartDate}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.selling_price') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->SellingPrice}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.end_date') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" disabled="disabled" value="{{$POS->EndDate}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.debit') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" id="debit_value" class="btn-info form-control" disabled="disabled" value="{{$POS->Debit}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('transactions.margin_profit') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class=" btn-info form-control" disabled="disabled"
                                                   value="{{ $POS->Deposit + $POS->Debit - $POS->PurchasingPrice}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <?php $TotalAmountPaid = \App\Http\Models\Transaction::GetTotalOfAmountPaid($POS->POSID) ; ?>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('transactions.total_paid') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="{{$TotalAmountPaid < $POS->Debit ? 'btn-danger' : 'btn-success'}}  form-control" disabled="disabled"
                                                   value="{{ $TotalAmountPaid }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('pos.finished') }} : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <p>
                                                <i class="{{$POS->Finished ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('transactions.payment_dates') }}<small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        @include('layout.partial.success')

                        @include('layout.partial.errors')

                        <div class="x_content">
                            @if($Transactions)
                            <div id="notification-box"></div>
                            @endif
                            <form id="generateTransactions" action="{{url('/transactions')}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <input type="hidden" name="_pos" value="{{Input::get('pos')}}"/>
                            <table id="tblResult" class="table table-striped responsive-utilities jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">{{ trans('global.serial') }}</th>
                                    <th class="column-title">{{ trans('global.status') }}</th>
                                    <th class="column-title">{{ trans('transactions.payment_date') }}</th>
                                    <th class="column-title">{{ trans('transactions.expected_paid') }} </th>
                                    <th class="column-title">{{ trans('transactions.actually_paid') }} </th>
                                    <th class="column-title">{{ trans('transactions.paid') }} </th>
                                    <th class="column-title">{{ trans('transactions.notes') }} </th>
                                    <th class="column-title">Actions </th>
                                </tr>
                                </thead>
                                <tbody>

                                    @if($GenerateTransactionsDates)
                                    <?php $index = 1; ?>
                                    @foreach($GenerateTransactionsDates as $date => $TransactionDate)
                                    <tr class="@if(Session::has('error_date') && $date == Session::get('error_date')) danger text-danger @else{{$TransactionDate['is_date']?: 'text-success'}}@endif">
                                        <td>
                                            <label class="btn btn-sm btn-primary"><?php echo $index; ?></label>
                                        </td>
                                        <td>
                                            <label>{{$TransactionDate['is_date'] ? trans('psystemdates.date') : trans('psystemdates.profit') }}</label>
                                        </td>
                                        <td><label>{{$date}}</label></td>
                                        <td>
                                            <input class="form-control expected_paid" type="text" name="expected_paid[{{$date}}]" value="{{isset(old('expected_paid')[$date]) ? old('expected_paid')[$date] : 0}}"/>
                                        </td>
                                        <td>
                                            <input class="form-control actullay_paid" type="text" name="actually_paid[{{$date}}]" value="{{isset(old('actually_paid')[$date])? old('actually_paid')[$date] : 0}}"/>
                                        </td>
                                        <td>
                                            <input class="icheckbox_flat-green" type="checkbox" name="has_paid[{{$date}}]" value="1" @if(isset(old('has_paid')[$date])){{old('has_paid')[$date] ? 'checked="checked"' : ''}}@endif />
                                        </td>
                                        <td>
                                            <textarea name="note[{{$date}}]" id="" cols="10" rows="1"></textarea>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" class="removeRow btn btn-sm btn-danger" >{{ trans('global.remove') }}</a>
                                        </td>
                                    </tr>
                                    <?php $index++; ?>
                                    @endforeach
                                    @else
                                        <?php $paid_transaction = $unpaid_transaction = 0 ?>
                                        @foreach($Transactions as $k => $transaction)
                                        <?php $transaction->Done ? $paid_transaction++ : $unpaid_transaction++ ?>
                                        <tr class="even pointer">
                                            <td class="a-center text-info" >
                                                <label class="btn btn-primary btn-sm">{{$k + 1}}</label>
                                            </td>
                                            <td class="a-center text-info" >{{ $transaction->ISDate ? trans('psystemdates.date') : trans('psystemdates.profit') }}</td>
                                            <td class="a-center text-info" ><label>{{ $transaction->PaymentDate }}</label></td>
                                            <td class="a-center text-info" >
                                                <input type="text" class="form-control expected_transaction_paid" disabled="disabled" value="{{$transaction->ExpectedAmount}}">
                                            </td>
                                            <td class="a-center text-info" >
                                                <input type="text" class="form-control" disabled="disabled" value="{{$transaction->AmountPaid}}">
                                            </td>
                                            <td class="a-center text-info" >
                                                <i class="{{$transaction->Done ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                            </td>
                                            <td class="a-center text-info" >
                                                @if($transaction->Notes)
                                                    <label>
                                                        <a class="source" onclick="show_notification('{{$transaction->Notes}}' ,'Notes about ({{$transaction->PaymentDate}})')">
                                                            {{strlen($transaction->Notes) > 27 ? mb_strimwidth($transaction->Notes ,0 ,27 ) . trans('global.read_more').'...' : $transaction->Notes }}
                                                        </a>
                                                    </label>
                                                @else
                                                   <label class="source">N/A</label>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('/transactions/'. $transaction->TransactionID)}}" class="btn btn-sm btn-primary">{{ trans('global.edit') }}</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </form>
                                </tbody>
                                <tfoot>
                                @if($GenerateTransactionsDates)
                                <tr class="">
                                    <th id="lbl_count_payments" colspan="3"></th>
                                    <th>
                                        <label class="btn btn-sm btn-default" id="total_EXValue"></label>
                                    </th>
                                    <th>
                                        <label class="btn btn-sm btn-primary" id="total_ACValue"></label>
                                    </th>
                                    <th></th>
                                    <th></th>
                                    <th>
                                        <label onclick="$('#generateTransactions').submit()"  class="btn btn-sm btn-primary" >{{ trans('global.submit') }}</label>
                                    </th>
                                </tr>
                                @else
                                    <tr class="even pointer">
                                        @if(!$POS->Finished)
                                            <form action="{{url('transactions')}}" method="post">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <input type="hidden" name="_pos" value="{{Input::get('pos')}}"/>
                                            <input type="hidden" name="one_row" value="one_row_{{csrf_token()}}"/>
                                            <td colspan="3">
                                                <?php
                                                $Transactions_dates_array = array_column($Transactions->toArray(), 'PaymentDate');
                                                ?>
                                                <select class="form-control" id="new_transaction_date" >
                                                    <option value="0">{{ trans('global.not_selected') }}</option>
                                                    @foreach($GenerateDates as $date => $payment_system_date)
                                                        @if(array_search( $date , $Transactions_dates_array ) === false)
                                                            <option value="{{$date}}">{{$date . ($payment_system_date['is_date'] ? ' (' . trans('psystemdates.date') . ')' : ' ('.trans('psystemdates.profit').')' )}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="expected_paid[]" value="0" id="new_expected_paid">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="actually_paid[]" value="0" id="new_actually_paid">
                                            </td>
                                            <td>
                                                <input class="icheckbox_flat-green" type="checkbox" name="has_paid[]" value="1" id="new_has_paid" />
                                            </td>
                                            <td>
                                                <textarea name="note[]" id="" cols="10" rows="1" id="new_note" ></textarea>
                                            </td>
                                            <td colspan="1">
                                                <input type="submit" class="btn btn-sm btn-success" value="Add"/>
                                            </td>
                                            </form>

                                        </tr>
                                        <tr style="background-color: #e3e3ff">
                                            <td colspan="3"></td>
                                            <td colspan="1">
                                                <label class="btn btn-sm btn-default" id="total_expected_transactions">Total Expected</label>
                                            </td>
                                            <td></td>
                                            <td colspan="3">
                                                <label class="{{$unpaid_transaction ? 'btn-danger' : 'btn-success' }} btn-sm">
                                                    {{ 'Total Paid ' . $paid_transaction . '/' . ($paid_transaction + $unpaid_transaction) }}
                                                </label>
                                            </td>
                                        @else
                                            <td colspan="5">
                                                <label class="alert alert-info">
                                                    {{ trans('transactions.if_pos_finished') }}
                                                </label>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @include('layout.partial.footer')
    </div>
    <!-- /page content -->
    </div>
    <!-- END page content -->

@endsection

@section('customscript')
    <script src="{{asset('dashboard/js/custom/create_transactions.js')}}"></script>
    <!-- PNotify -->
    <script type="text/javascript" src="{{asset('dashboard/js/notify/pnotify.core.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/notify/pnotify.buttons.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/notify/pnotify.nonblock.js')}}"></script>

    <script type="text/javascript">
        function show_notification(text ,title ){
            new PNotify
            ({
                title: title,
                text: text,
                type: 'dark',
                hide: true
            });
        }

        if ( $('#notification-box') ){
            // check if total expected is over debit
            if ( Ctransaction.total_transactions_expected() > parseInt($('#debit_value').val() ) ){
                $('#notification-box').attr('class' ,'alert alert-dark' ).html(' * مجموع المتوع دفعه اكبر من المتبقي');
            // check if total expected is less than debit
            }else if ( Ctransaction.total_transactions_expected() < parseInt($('#debit_value').val() ) ){
                $('#notification-box').attr('class' ,'alert alert-dark' ).html(' * مجموع المتوقع دفعه اقل من المتبقي');
            }
        }



    </script>

@endsection
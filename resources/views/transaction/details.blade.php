@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/transactions/create?pos=' . Crypt::encrypt($POS->POSID) )}}"><h3>{{ trans('transactions.goto_all_transactions') }}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('transactions.transaction_more_details') }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            @if(!$POS->Finished)
                                <div id="notification-form" class="alert alert-info row invisiblediv" >
                                <div class="col-md-12">
                                    <strong>اضغط موافق اذا كنت تريد الغاء هذه المعاملة !</strong><br /> مع العلم انه لا يمكنك الغاء المعاملة في حالة اغلاق عملية البيع. <strong>هل تريد الالغاء</strong>
                                <br/><br/>
                                </div>
                                <div class="col-md-12">
                                    <form method="post" action="{{url('/transactions/'.Crypt::encrypt($Transaction->TransactionID))}}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <input type="submit" class="btn btn-round btn-warning" value="{{ trans('global.confirm') }}"/>
                                        <input type="button" id="cancel-notification" class="btn btn-round btn-default text-primary " value="{{ trans('global.cancel') }}"/>
                                    </form>
                                </div>
                            </div>
                            @endif
                            @include('layout.partial.success')
                            @include('layout.partial.errors')
                            <div class="form-horizontal form-label-left">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('psystemdates.date') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$Transaction->PaymentDate}}" disabled>
                                        <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('psystemdates.type_date_or_prfoit') }}</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$Transaction->ISDate ? trans('psystemdates.date')  : trans('psystemdates.profit')}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.expected_paid') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$Transaction->ExpectedAmount}}" disabled>
                                        <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.actually_paid') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$Transaction->AmountPaid}}" disabled>
                                        <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.date_of_paid') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$Transaction->DateOfPaid ? $Transaction->DateOfPaid : 'N/A'}}" disabled>
                                        <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.paid') }}</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <label class=" btn source col-md-12 col-xs-12" >
                                            <i class="{{$Transaction->Done ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.notes') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                            <label>{{$Transaction->Notes ? $Transaction->Notes : '' }}</label>
                                    </div>
                                </div>
                                    <div class=" actionBar form-group">
                                    @if(!$POS->Finished)
                                        <a href="{{url('/transactions/'.$Transaction->TransactionID.'/edit')}}" class="btn btn-primary source" >{{ trans('global.edit') }}</a>
                                        <a id="deletetransaction" href="javascript:void(0)" class="btn btn-danger source" >{{ trans('global.delete') }}</a>
                                    @else
                                        <label class="alert-info alert">
                                            {{ trans('transactions.if_pos_finished') }}
                                        </label>
                                    @endif
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('transactions.pos_details') }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div class="form-horizontal form-label-left">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('customer.customer_name') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$POS->Customer()->first()->CustName}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('psystem.psystem') }}</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$POS->PaymentSystem()->first()->PaymentAlias }}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('item.item_name') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$POS->Item()->first()->ItemName}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.purchasing_price') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$POS->PurchasingPrice}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.selling_price') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->SellingPrice}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.deposit') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->Deposit}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.interest') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->Interest}}%" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.discount') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->Discount}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.start_date') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->StartDate}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.end_date') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->EndDate}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.debit') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->Debit}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.margin_profit') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{ $POS->Deposit + $POS->Debit - $POS->PurchasingPrice}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.total_paid') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{\App\Http\Models\Transaction::GetTotalOfAmountPaid($POS->POSID)}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.finished') }}</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <label class=" btn source col-md-12 col-xs-12" >
                                            <i class="{{$POS->Finished ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.notes') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <label>{{$POS->Notes ? $POS->Notes : '' }}</label>
                                    </div>
                                </div>
                                <div class=" actionBar form-group">
                                        <a href="{{url('/pos/'.$POS->POSID.'/edit')}}" class="btn btn-primary source" >{{ trans('global.edit') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection
@section('customscript')
    <script type="text/javascript">
        (function(){

            $('#deletetransaction').on('click' ,function(){
                $('#notification-form').slideDown()
            });
            $('#cancel-notification').on('click' ,function(){
                $('#notification-form').slideUp()
            })

        })();
    </script>
@endsection


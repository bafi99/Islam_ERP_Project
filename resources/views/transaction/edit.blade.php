@extends('dashboard.header')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <a href="{{url('/transactions/create?pos=' . Crypt::encrypt($POS->POSID) )}}"><h3>{{ trans('transactions.goto_all_transactions') }}</h3></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('transactions.transaction_more_details') }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div id="notification-form" class="alert alert-info row invisiblediv" >
                                <div class="col-md-12">
                                    <strong>اضغط موافق اذا كنت تريد تعديل المعاملة !</strong>
                                    <br/><br/>
                                </div>
                                <div class="col-md-12">
                                    <input type="button" onclick="$('#update_transaction').submit()" class="btn btn-round btn-warning" value="{{ trans('global.confirm') }}"/>
                                    <input type="button" id="cancel-notification" class="btn btn-round btn-default text-primary " value="{{ trans('global.cancel') }}"/>
                                </div>
                            </div>
                            @include('layout.partial.success')
                            @include('layout.partial.errors')
                            <form id="update_transaction" action="{{url('transactions/' . Crypt::encrypt($Transaction->TransactionID) )}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="_method" value="PUT" />
                                <div class="form-horizontal form-label-left">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('psystemdates.date') }} </label>
                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                            <select class="form-control text-center select2_single" name="payment_date">
                                                <option value="0"><-- Select Date --></option>
                                                @foreach($GeneratedPaymentDate as $Date => $Object)
                                                    <option value="{{$Date}}" @if($Transaction->PaymentDate == $Date) selected="selected" @endif >{{$Date}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.expected_paid') }} </label>
                                        <div class="col-md-9 col-sm-9 col-xs-9">

                                            <div class="col-md-11 col-sm-11 col-xs-11">
                                                <input type="text" class="text-center form-control" name="expected_paid" value="{{$Transaction->ExpectedAmount}}" disabled >
                                            </div>
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                                <input id="expected_paid" type="checkbox" value="1" name="expected_paid_status" class="icheckbox_flat-green" />
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.actually_paid') }} </label>
                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                            <input type="text" class="text-center form-control" name="amount_paid" value="{{$Transaction->AmountPaid}}">
                                            <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.date_of_paid') }} </label>
                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                            <input type="text" class="form-control text-center date-picker" name="date_of_paid" value="{{$Transaction->DateOfPaid}}" >
                                            <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.paid') }}</label>
                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                            <label class=" btn source col-md-12 col-xs-12" >
                                                <input type="checkbox" class="icheckbox_flat-green" name="is_paid" value="1" {{$Transaction->Done ? 'checked' : ''}} />
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.notes') }} </label>
                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                        <textarea class="form-control" name="notes" >{{trim($Transaction->Notes)}}</textarea>
                                        </div>
                                    </div>
                                    <div class=" actionBar form-group">
                                        <a href="{{url('/transactions/'.$Transaction->TransactionID)}}" class="btn btn-default source" >{{ trans('global.cancel') }}</a>
                                        <a id="updatetransaction" href="javascript:void(0)" class="btn btn-success source" >{{ trans('global.update') }}</a>
                                    </div>
                                </div>
                            </form>
                            <div class=" actionBar form-group" style="background-color: #e3e3e3">
                                <p>
                                    <strong>{{ trans('transactions.postpone_quest') }}</strong><br/>
                                </p>
                                <form action="{{url('/transactions/postpone/' . Crypt::encrypt($Transaction->TransactionID) )}}" method="post" >
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="_method" value="patch"/>
                                    <div class="col-xs-9">
                                        <select class="text-center form-control select2_single" name="postpone">
                                            <option value="0" selected="selected" >{{ trans('global.not_selected') }}</option>
                                            @foreach($AvaiableTransactionPaymentDates as  $TransactionDate)
                                                <option value="{{$TransactionDate->PaymentDate}}">{{$TransactionDate->PaymentDate}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="submit" class="btn btn-primary" value="{{ trans('transactions.postpone') }}"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{ trans('transactions.pos_details') }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div class="form-horizontal form-label-left">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('customer.customer_name') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$POS->Customer()->first()->CustName}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('psystem.psystem') }}</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$POS->PaymentSystem()->first()->PaymentAlias }}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('item.item_name') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$POS->Item()->first()->ItemName}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.purchasing_price') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="text-center form-control" value="{{$POS->PurchasingPrice}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.selling_price') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->SellingPrice}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.deposit') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->Deposit}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.interest') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->Interest}}%" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.discount') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->Discount}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.start_date') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->StartDate}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.end_date') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->EndDate}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.debit') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{$POS->Debit}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.margin_profit') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{ $POS->Deposit + $POS->Debit - $POS->PurchasingPrice}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.total_paid') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" class="form-control text-center" value="{{\App\Http\Models\Transaction::GetTotalOfAmountPaid($POS->POSID)}}" disabled>
                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('pos.finished') }}</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <label class=" btn source col-md-12 col-xs-12" >
                                            <i class="{{$POS->Finished ? 'fa-check-circle text-success' : 'fa-times-circle text-danger'}}  fa-2x fa"></i>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">{{ trans('transactions.notes') }} </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <label>{{$POS->Notes ? $POS->Notes : '' }}</label>
                                    </div>
                                </div>
                                <div class=" actionBar form-group">
                                    <a href="{{url('/pos/'.$POS->POSID.'/edit')}}" class="btn btn-primary source" >{{ trans('global.edit') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        @include('layout.partial.footer')
    </div>
    <!-- END page content -->
@endsection
@section('customscript')
    {{--select2--}}
    <script src="{{asset('dashboard/js/select/select2.full.js')}}"></script>

    <script type="text/javascript" src="{{asset('dashboard/js/moment.min2.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/datepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript">
        (function(){

            $(".select2_single").select2();

            $('#updatetransaction').on('click' ,function(){
                $('#notification-form').slideDown()
            });
            $('#cancel-notification').on('click' ,function(){
                $('#notification-form').slideUp()
            })
            $('.date-picker').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                format: "YYYY-MM-DD"
            });

            // check if expeceted price check box is enable
            $('#expected_paid').on('change' ,function(){
                // expected paid status
                if ( $(this).prop('checked') ) {
                    $('[name=expected_paid]').attr('disabled' ,false )
                }else{
                    $('[name=expected_paid]').attr('disabled' ,true )
                }

            });

        })();
    </script>
@endsection